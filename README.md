# Croatian Run Restaurants in Los Angeles. 1875–1930. #

[Background](https://knobby.ws/blog/2015/09/23/croatian-restaurants-in-los-angeles-project/ "Knobby blog item when kicking off this effort") on this project.

See [working site](https://secure-shore-68966.herokuapp.com "Site hosted at heroku"). Heroku is being used because of its integration with Rails and Postgres.
[Acknowledgements](https://secure-shore-68966.herokuapp.com/acknowledgements) gives some clues about how this works. Contact info also.

This is a work in progress. Having to learn a lot. And very much beta. Several important features left to add. And then lots of clean up and UI improvements.

Greg

If I did this over again, Years would be named Connections, but the date was the join and I was afraid that date might be some kind of reserved word and years was what came to mind. Originally Locations was named Address but got to confusing with the street address, but after the hassle of that name changing decided to leave Years as is. And I found I could use routes to change the interface to connections.

Webpacker and Yarn

Uninstalled all the npm and then used yarn add … 

Trials page for development 

Confusingly I have 'Documents' for ActiveRecord and 'Docs' for the individual document pages that I use as references. Partly because I started without ActiveRecord, then transitioned to it, then I finally understood how to organize it all. Basically each  'doc' is put in the table docs (and stored via ActiveRecord in a field named document (images would have been better)), then for the various entries in the years, locations, and people tables the documents are referenced via docs.
