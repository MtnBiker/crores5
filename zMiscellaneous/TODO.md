#### Data research needed in in Evernote: _Library/Ancestry to do
### Story ideas
- Earliest current reference to Croat in LA is Jerry and John Illich, marietich, ads
- Eastern section in a separate entry goes
- popup photo or stories
- look at some storie

#### TODO List
- Search for ActionText https://thoughtbot.com/blog/full-text-search-with-postgres-and-action-text, but I think can create the extra field and then search for that instead. e.g., content_plain_text and use normal search tools. It seems his field only works with a single table since :plain_text_body will be searched on any table. So need to create something like :plain_text_field-name for each model.EXCEPT, can't create a migration at present
- locations/show map is not centered correctly
- Sizing of snippet_draw is usually too large
- Move CSS back to assets so can use Hotwire and` importmap-rails` more easily.  Note: In order to use JavaScript from Rails frameworks like Action Cable, Action Text, and Active Storage, you must be running Rails 7.0+. This was the first version that shipped with ESM compatible builds of these libraries. MAYBE LEAVE WELL ENOUGH ALONE FOR NOW
- Nav bar doesn't collapse
- https://stackoverflow.com/questions/57277351/rails-6-zeitwerknameerror-doesnt-load-class-from-module and `config.autoloader = :classic`. Remove that option. Doesn't work at Heroku withot for now
- location/edit when click in coords toggle coords locked
- List of restaurants. Use in years form.
- Sort out San Fernando addresses
- people>show. (co-)proprietors only, i.e., not others?
- 5 sec. to get edit page (nor sure for which table)
- year/show- list doc original file
- snippet display on return to show.
- If source.effective_date is nil allow doc.effective date to be defined (for things like draft and Naturalization). First change is to doc form and db, but then how is effective date retrieved for eg years??
- People > show is a styling mess. Also implementation of the two lists as far as jS is crude. And the buttons aren't quite logical. They need to show state or better toggle and change the text in a single button.
- both locations>show and people>show had to "manually" size the map. Need to sort out where the styles are coming from
- In docs, > new, put in the doc and save a snippet (sometimes would be more than one)
- Put a date on the docs, then pull into locations, people, years that use that doc
- snippet larger on show, up to 2X- some such as /1194 are too small
- How to present info. Maybe need to work on stories and maybe something will work. /map isn't going anywhere. Hard to imagine even with enhancements (color dates) it will tell much
- Add https://github.com/zombocom/syntax_search
- https://developers.google.com/streetview/web or another service https://alternativeto.net/software/google-street-view/ openstreetcam.org loaded slowly. Added url and notes for. Implement later
- Zoom level is outside map.  Can zoom be optimized for whichever map is selected?
- Search on sorted tables on people. When do the search the sort is lost. https://expo.stimulusreflex.com/demos/tabular works better, but FIRST try and fix what I have
- Delete images not wanted
- people>show attributions. But try to create a master file for the map links
- people>show if person worked and lived at same place sometime have to move the cursor around to find that out. Can the area be expanded?
- people>show rollover on line should show addresses and postions too? Will try this in olMap
- Arrow on line connecting places to show later years. people>show. Or color scheme to show later years. Brighter later or more up pu the spectrum
- Try map selection options as a sidebar rather than a dropdown. ALA Nishanbaev. Takes space but can see what is displayed
- Left side of people>show is a too spread out vertically
- Fork and knife resto icon. From noun…
- I have `bootstrap_form`, `simple_form` and maybe some other Bootstrap options. Not using `bootstrap_form`, but the gem is installed
- Add back leaflet.timeline in application.js:33
- When click on items in people >show should activate in sidebar or on map like Nishanbaev. Not very needed but as an exercise.
- docs>show add all the links to the doc
- GPS coords good on edit address but not on show https://github.com/alexreisner/geocoder working on it 2020.09.19
- Multiple doc references to years, people, locations
- DoB must have month-date to be added. Should just be able to put a year. Not validated in model so must be a Postgres issue. Seems to be, need y m d. Must be a work around but may involve storing an approx. date in another kind of field and then in show and if-else, i.e., if y d d, the show that otherwise show the year only text field. How do the genealogy sites do it.
- Change `doc.original_url` to something more descriptive (`source_url` ?)
- Slow down doc destruction.
- Doc image showing  on location and people, but need all images, i.e., multiple
- Improvement to Person>Show>connections. Separate columns for resid and resto, but lined up. Maybe a rollover for source?
- Clear x in search field
- Can't be blank notice on years/edit needs to be red
- Map display isn't really what I want it to be
- Default sort on /people to First and Last Name. Now in ID order. Probably could do with a change to Ransack. Still sortable although years and docs have been changed to Ransack, but people and locations are sortable (years needs the relationships, so is more complicated. Also wanted search in years)
- <%= stylesheet_pack_tag 'application', media: 'all', 'data-turbolinks-track': 'reload' %> doing nothing since webpacker.yml extract_css option to true. and application.scss must be application.css. Played with this and cascading errors. Need to sort out Webpacker for css/stylesheets
- Massive cleanup of creation of old resto_resid_lines needed after do the above. Should be able to remove resto_resid_lines table since it's not being used. Need a migration. But it's empty so not using many resources. Also remove lines table.
- checkbox to delete document. see https://stackoverflow.com/questions/53411099/removing-activestorage-attachments-in-rails-5
- Add ActiveStorage thumbnails to years/index. Works on years/show
- Location > Show: Nice to show count of resto and resid in subheading
- Purge the duplicate documents, e.g. ActiveStorage::Attachment.find(30).purge
- A page for a year with a resto and resid column with Person/address. Would look like the bottom of address>show except showing person address
- Add map to years/xx, i.e., so can see where that person was and show the connection if it exists for that date or?
- In people, place_of_birth is blank, but not NULL. Why? Can I fix this. The blank drop down at least when update doesn't create a null.
- map tooltip would be better if wrapped to multi-line. L.popup does this, but less control over tooltip. See Nov 2019 notes and changes 31-map-popup
- Should given_name be first_name? When I came back after a long time that's what I assumed it was
- Nav bar > Image/Document Uploader update for ActiveStorage documents and images?
- years>ref_url should be renamed source_url, In database and in pages that use it.
- delete config>intializers>aws.rb
- Sorting on People page. Sort on Address page works. No errors in dev mode. Can't see any differences between People or Location (address) setups
- Sort not working for years. 2020.01.28 Now working for locations and people. Still using GoRails 2010
- Show was formatted for while adding data, but is a bit messy for an observer, maybe need a detailed (default when logged in) version and an observer version with a toggle between the two
- Users page is blank
- Need to put in some testing. Tried RSpec and couldn't figure out where test values were coming from.
- Integration tests
- Should resto list be a separate table and somehow linked to locations?
- Sort out activerecord-postgis-adapter and rgeo installation and dependencies. Having activerecord-postgis-adapter causes Heroku to (often) not push. Have both with historicstreets.la without any problems
- Location>Edit, click to set location. Click does come up the coordinates, although they don't seem to be quite in the right place--it shows the center of the map, not the place clicked
- Mapbox map (Hamlin) not showing.
- For connecting lines, check on either side of date and if person hasn't moved during that time, use that home address
- For connecting lines, if a person has three addresses for a period (two work or two resto—this is common), the matches might fail. If, e.g., the search try to match a home address and a resto is found, the second resto address will never be found. 2016-10-30
- Map should have different color for different years. And should be an option to have all the lines showing.
- Map PopUp not working

- See Implementation Notes on where in the process I'm at or at the bottom of this page

- 'data-turbolinks-track' => true is on some pages and not other where javascript and css includes are. Sort this out. Maybe don't need at all since it messed up LA Historical sites (probably because my jS isn't that well written)

- Add delete/destroy to Years (People  too, but may want to keep them with notes about why deleted. But Years' entries go away when consolidate people, that is, when two people become one and some of  duplicate Years' entries go away. This may be rare and maybe I should just do it in PGAdmin)

- Create 50x50 thumbnail for images and save in database for use on index.html. Every time index is reloaded all the full size images are downloaded. Not good. 2016.11.25 Doing this in image-uploading branch using PaperClip. Will keep old version for now. Follow https://devcenter.heroku.com/articles/paperclip-s3 Needs work. Problems with AWS access. Backed this out and am now implementing CarrierWave. And Rails 5.2 implements uploading. So using ActiveStorage new
 elsif document.previewable? commented out in years show and edit. Undo?

- Rewrite _leafletmap.html.erb in Mapbox (and rename) which is used in locations>show. Probably go to OL

- Resid vs. resto shading works on Connections>Summary but not index.

- Check if something exists for that person and location within a year. Show the differences and allow save if desired. See "Creating a Record if It Doesn’t Already Exist" in Gilmore

- Added * to address to show geocoded to current location. This should be more robust. EG, note in entry to put in asterisk (or another field to indicate address shown doesn't match current coords) and then the asterisk shows up automatically



- Images page. Formatting needed. Maybe put them all in a folder and have them served up automatically. Look at Bootstrap, but it may take more than that.

- Year markers on Summary page

- Add search to top of Connections>index page

- people/show.html.erb Links are not obvious. Can't figure out how to make them darken, etc in Bootstrap. years/summary is fine

- Person consolidation notes. New table with link to people, so can log each change. Same link in old and new person—have to work  this out, not sure how it should be

- _ year.html.erb shows duplicates for a year. Also the listing should have a link back to the connection. In other words wrap the line in a link back the the year_id or similar

- In about 1889, addresses on N Main where changed. ~100 was added to the addresses. Need to find out when exactly and adjust all old addresses. May need to mark pre-1888 addresses (maybe just add " pre-1888" to address)
- On Connection page, foreign fields don't sort correctly. Something happens on first click, but it's not a sort

- How handle possible duplicates, e.g., N Lessovich and Nicholas Lessovich. Assume same unless know otherwise. Use more complete, but note that done so
- How merge people when determine they are the same. Problems with connection. See above
- Decide if Lessovich is Lessevich 1888
- Two Jack Marietichs Change last_name to surname. Wait a week or so until Address to Location change is settled
- Two Marietich in 1893. Guessed-fix?. Two Andrews

- Add link/search to connection from People>list and >show and visa versa for address.
- Should map and resto_resid_lines be one and the same?

- Confirm 1890 inputs when Years>List page is working.
- Sort only works first time click on a column, i.e. asc; but second click won't desc sort. Years>list updated_at for one
- Get rid of try on Years>Index.
- Is Thomas Marinovitch the same as Thomas Marinovich? (120 and 121)?


### Update years_controller to     @year = Year.new({:year_date => "1887-09-01"})
 to give default date in new form depending on the year working in . 09-01 is default for unknown date for City Directories

##### Markdown Help
<https://github.com/adam-p/markdown-here/wiki/Markdown-Cheatsheet>

- + * for bulleted lists (see non-rendered)

Emphasis, aka italics, with single *asterisks* or _underscores_.

Strong emphasis, aka bold, with two surrounding **asterisks** or _underscores__. Everything is bold below here how tor  turn off Underscores causing the problem

Combined emphasis with two surrounding **asterisks and _underscores_**.

Strikethrough uses two surrounding tildes. ~~Scratch this.~~ Except it doen't work mixed into a bulleted list

[I'm an inline-style link with title](https://www.google.com "Google's Homepage")

--- Thought this was supposed to be a line. Not so in Ruby/Rails


##### Images
Inline-style:
![alt text](https://github.com/adam-p/markdown-here/raw/master/src/common/images/icon48.png "Logo Title Text 1")

[logo]: https://github.com/adam-p/markdown-here/raw/master/src/common/images/icon48.png "Logo Title Text 2"

## Completed

- How handle multiple proprietors? See above item too. I now have Co-proprietor which will handle it for now~~

- Rename address field to avoid confusion. numstreet? or leave well enough alone. Renamed Addresses table instead ~~

- It's still address_id in Years. Might break the Years data if fix. So I'm creating a pdf of the existing index page output. Try again, doesn't show all the info. BEST is probably .tsv I saved. migrated address_id column to location_id column in Years

- New Connection. Try to get drop down list for Title/Position working. Simpler than other since don't need key
2016.02.04

- Need Restaurant Names. Should this be part of Connection?? Yes done 2016.02.08
- Add Users—needed so move to public. Follow Hartl past I think it's Chap 5 or 6.
- Stack Overflow about putting db on Heroku.
- Person>Show. List of connections needs to be sorted by year. Note _year.html.erb creates the item. The list is people> show. But how would sorting be done? Can't remember how I did this. Look at the code

2016-09-28 - The following two fixed I think by straightening out bower and  few conflicting gems: need to check heroku though
Dropdown working on laptop, but not heroku, maybe the following will fix, but proceed slowly
Be careful with this as I've had SASS problems, but maybe this will fix the problems
Did you know that Twitter Bootstrap 4 will be in Sass and not LESS anymore? That means we can directly use it inside Rails projects with official bootstrap gem.

No need to use bootstrap-sass or even LESS version twitter-bootstrap-rails.
https://github.com/twbs/bootstrap-rubygem
I don't have sprockets installed and http://masa331.github.io/2016/04/23/integrate-bootstrap-4-into-rails.html talks about not using sprockets. OK what is sprockets? Need to sort out sprockets, sass, bootstrap

2019.01.07. After hiatus to work on Historical Streets coming back to work on this. First now on iMac with Mojave. 
Trying without Ruby version manager, i.e.,rs with `bundle install` so went back to 5.0.5. 
Ruby Sass is deprecated and will be unmaintained as of 26 March 2019.

* If you use Sass as a plug-in for a Ruby web framework, we recommend using the
  sassc gem: https://github.com/sass/sassc-ruby#readme. [I added the gem sassc]

* For more details, please refer to the Sass blog:
  http://sass.logdown.com/posts/7081811

bundle errors gone. 

~~Now deal with pages not loading. Locations seems to be missing from database—no locations table in PGAdmin. Most recent dump has it, but need to check if changes since then. Most likely but how do I tell. Most recent dump is 2016.10.05—this is a dot sql file and is easier to read. NOT SURE WHICH IS THE LATEST. MOD DATES DON'T MATCH THE DATE I PUT IN THE FILE NAME. But the 2016.10.15 with a mod date of Mar 17, 2017 is the largest, so maybe it's the lates. All the other files are dot dump files. How was the dot sql file created? and why? Maybe it's from PGAdmin which can create a plaintext dump. I'll do a new dump, and then see if I can figure out how to see changes since then. Argh!
Can I just import the locations part or should I just bite the bullet and restore 
Latest upload to Heroku via Transmit and knobby.ws/toShare was 3/16/17 and is 60KB which is in db/dumps also
crores5_development.2016.10.05.sql is dated one day later, so isn't likely to be much different. Why is says 2016.10.05 is likely a typo
Still doesn't answer question of where location went.~~ Didn't get it ported over correctly from MBP
pg_restore --verbose --clean --no-acl --no-owner -h localhost -d crores5_development crores5.fromMBP.dump (after dumping )
2019.01.17 location now here and everything seems to work on iMac. This is a week or so after the above comments. Lost track of what was going on, but seems to be OK now. Will try to upload to Heroku.
Need to upgrade to Cedar stack 16, heroku stack:set heroku-18 -a secure-shore-68966, git push heroku master. Site still works, but has formatting problems that it did before and map doesn't load. Database should be OK, but will push it up again. No change at Heroku, but maybe wait a bit. Still shows as cedar-14. Didn't get built yet though `heroku apps:info -a secure-shore-689661` next build will be heroku-18
2019.01.18 Pushing to Heroku go the following error, the next line was the fix. found at https://github.com/bundler/bundler/issues/6784
remote:  !     You must use Bundler 2 or greater with this lockfile.
heroku buildpacks:set https://github.com/bundler/heroku-buildpack-bundler2
git push heroku master
Formatting still not working at Heroku, nor does map work

Now working. Got rid of Uglifier. Wasn't using on la-hist-streets. May have to put some arrow assets back in
Sort not working. See action in Rails log (whatever see in Rails s window in iTerm) , but sorting doesn't happen Seems to be OK at least on locations page
=======
2019.05.06 - Shouldn't be able to delete any address with a connection. Needs to be some kind of warning or note made in any connection. Was able to do this on localhost. Couldn't delete on heroku but who knows why
- A test is needed to confirm that repopulateResidResto in controllers/application_controller.rb is run correctly. Runs but does a ROLLBACK which leaves it blank in Rails 5.1. I may have a workaround for this in that I check if the resto_resid_lines pages has any rows of data.. Have to see what happens when get to 5.1
- 2019/06/15 ActiveStorage implemented. - Image uploading not working. Set up as CarrierWave. But maybe should consider what Rails now provides (ActiveStorage https://guides.rubyonrails.org/active_storage_overview.html ) rather than fix.zSetting up Active Storage isn't trivial. Still have connection to AWS S3 as previously uploaded images are showing; just not working anymore.  Switch from CarrierWave. Look at images tests
- secrets.yml needs to go away. Credentials https://evilmartians.com/chronicles/rails-5-2-active-storage-and-beyond. https://guides.rubyonrails.org/security.html#custom-credentials 2019.06.20 done

- See if table Images is needed. Removed, not needed

- cancel button on edit pages (probably true for connections, addresses and people) goes back to the show for that item, but if you came from list or somewhere else it should return you to the page you came from. 2019.06.22. path is :back
 - Clean up aws (2019.09.23 fixed a few days ago):
    Some interaction between aws pieces        [
    gem 'aws-sdk-s3', '~> 1' cannot be gem "aws-sdk-s3", require: false
    S3_BUCKET must be set in aws.rb (probably can do this somewhere else)        [
    years_controller.rb >  before_action :set_s3_direct_post, only... can't be commented out. What is it for?
    Some combos, server won't launch, others give errors on years/xx/edit


- Delete unused field in People: location_id. 2019.10.01
- Can resto_resid_lines be created directly in a page from years with an ActiveRecord or SQL search Find people who lived and worked in the same year, map to new variables to display ?? Need to see if I learn enough SQL to do this. If I can't figure this out would like to be able to add links to locations in resto_resid_lines/index.html. Since two locations can't use them like person.id 2019.11.22 - RestoResidLines uses the person.id which means it displays on the page, but location.id occurs twice so I remapped it and it doesn't show up as a link on the page. RestoResidLines (renamed to RestoPersonResid or something), should be made smarter. Is JOIN on the date the way to go?
- 2019.11.25 Now need to update it automatically (trigger) and access it.
- git push heroku master (didn't work when didn't use master, but something about update to Rails 6 said us master). Not sure about this, but 2019.11.26 pushing to Heroku now working.t
- Add MapBox key to EDITOR="mate --wait" bin/rails credentials:edit ? Don't think necessary. Is available 

- Webpacker is more tedious and I think breaks convention over configuration a bit. But I'm in now and probably not worth going back. I left images under control of Sprockets (?). The issue for me is having all of the node_modules.
- If you don’t have much JS (just a bit of jQuery, retina.js, etc). Probably not worth changing over to Webpack. webpack really shines when you’re using a lot of modules and have a significant amount of your own JS code to work with too. If your total use of JS amounts to a few <script src='...'> tags you won’t see much benefit. 2015 article
  I have one script src that is active: <script src="https://api.mapbox.com/mapbox.js/v3.1.1/mapbox.js"></script>, but I have a few javascipt/*.js files. Bootstrap 4 might be better off with Webpack, but I don't know that I need it
- Deal with non-Croatians. Victorin Dol, Montegrins? non_croations: boolean, default false_ Toppans is a partner and he's not the only non-Croatian partner. Default: false Field true for non-Croatians. Set all to false to start
- Map on each person page with locations. Coded by resto or resid and maybe year with color. Use ol modeled on location layout. Beta implementation
- Add Sources to Location/Address. 2020.03.20 an two items below
- Should Source be a table and if so, just year with pg number being different? Address>Source needs to be "shared". Maybe should name field in Address table "Source_address" Maybe should be called "Reference" table. Need several listing possible for the Source: Title_Source, Local (my copy), Ancestry/Library, other URL. Do I have to go to Sources to create a new source. I want to save directly form connections>new or edit.
-2020.05.06 - Cannot put in new locations because new not working. Probably because now have latest leaflet via webpacker. Either sort this out or make work with OpenLayers. Yeah right. I think I meant that find coordinates for an address not working. Manual for now and maybe that's easiest
-2020.05.10 - Sorting on Docs>index. Did with Ransack
- ActionText is messing up how some things are displayed. Sort this out. :content sorted out
- person/show layout needs reworking with the map taking space. Seems OK, fixed along the way?
- Multiple source images. Need a good scheme for snippet names. Year-Ref-Who-What. 1893-LACD_Resto_Marietich-Restovich [resto being resto section], eg 1893-01-02LAH__, 1894USC__ (census),
- Styling on people/show is a mess. Somehow date is not on same line and doesn't retain color. Details and Delete 'button' aren't well placed
- May need do more to get images to upload from Heroku: https://devcenter.heroku.com/articles/direct-to-s3-image-uploads-in-rails#s3-sdk. I think I'm beyond this
- Edit of connection/years is creating a duplicate (old and revised are saved). Some evidence of this going back to 2019. Getting two records without editing. Double created when originally created. 2020.09.09 fixed
- Baist maps need to be one step higher resolution. Can't read addresses. Now 10–18
- Markers need changing on people> show. See olLocationShowMap.js for ideas? 2020.10.17 and earlier
- Look at ol.source.Cluster for olPersonMap. Maybe not, need to show more popup  info since my problem is multiple occurrences at the identical location. Therefore abandoned
- Popover people>show only shows one event at an address when there may be many https://getbootstrap.com/docs/4.5/components/popovers/ and hover. Not using Bootstrap for popovers, but OL, maybe a cluster like display. Or maybe click for more info. Fixed using jBuilder to create another key with needed elements and then gathered them all in olPersonMap.js
- Make sure page loads correctly the first time. people/show. FIXME. If use show next on a people page, doesn't display the page without a refresh. Changed application.js to add channels, but now an error
- Message bar shows in people>show all the time in yellow 2020.10.28
- years: if resid then should be Resident or Householder and if resto should be one of several positions 2020.11.13  
- years: resto or resid should be determined based on title instead of having to select 2020.11.14
- Directory date changed from Sept to Jan of the year of the directory 2021.03.01 in progress
- Classify address as pre or post 1890. How to flag and how to display on various pages?
 

