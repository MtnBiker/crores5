-- fn_create_view_view_resto_resid_lines. This created the View

CREATE OR REPLACE FUNCTION fn_create_view_resto_resid_lines() RETURNS trigger AS
  $$
  BEGIN
  CREATE OR REPLACE VIEW public.view_resto_resid_lines AS
  SELECT 
     resto.person_id,
     p.given_name AS first_name,
     p.last_name,
     (rtrim(p.given_name::text) || ' '::text) || rtrim(p.last_name::text) AS full_name,
     resto.year_date AS resto_date,
     resid.year_date AS resid_date,
     resid.year_date + ((resto.year_date - resid.year_date)/2) AS mid_date,  -- can't add dates, but can substract and end up with days (interval) which can be added to a date
     resto.resto_name,
     resto_loc.address AS resto_address,
     resto.title AS title_resto,
     resid_loc.address AS resid_address,
     resid.title AS title_resid,
     resto_loc.longitude AS long_resto,
     resto_loc.latitude AS lat_resto,
     resid_loc.longitude AS long_resid,
     resid_loc.latitude AS lat_resid,
     rtrim(resto.notes) AS resto_notes,
     rtrim(resid.notes) AS resid_notes,
     p.date_of_birth,
     resto.id AS resto_connection_id,
     resid.id AS resid_connection_id,
     resto.location_id AS resto_loc_id,
     resid.location_id AS resid_loc_id
    FROM years resto
      JOIN years resid ON resto.year_date >= (resid.year_date - 364) AND resto.year_date <= (resid.year_date + 364)
      JOIN people p ON resto.person_id = p.id
      JOIN locations resto_loc ON resto.location_id = resto_loc.id
      JOIN locations resid_loc ON resid.location_id = resid_loc.id
   WHERE resto.person_id = resid.person_id AND resto.resto = true AND resto.resto <> resid.resto AND resto.resid <> resid.resid AND resto.resto = true AND resid.resto = false
   ORDER BY resto.year_date, p.last_name, p.given_name;
  RETURN NEW; -- I don't need anything returned, but there needs to be a RETURNS in the first line. Error without though
  END;
  $$
  LANGUAGE plpgsql;  -- the only language I have for triggers, not sure about trigger functions, but play it safe


Triggers (not sure if it's the following three or the fourth that made it work):
CREATE TRIGGER trg_view_rrl 
AFTER UPDATE OR DELETE ON people 
EXECUTE PROCEDURE fn_create_view_resto_resid_lines()

CREATE TRIGGER trg_loc_view_rrl 
AFTER UPDATE OR DELETE ON locations 
EXECUTE PROCEDURE fn_create_view_resto_resid_lines()
  
CREATE TRIGGER trg_years_view_rrl 
AFTER INSERT OR UPDATE OR DELETE ON years 
EXECUTE PROCEDURE fn_create_view_resto_resid_lines()

  --Didn't try this one (because got the above earlier). Did run this later, and not sure if it's the one controlling everything
  CREATE TRIGGER trg_view_rrl 
  AFTER UPDATE OR DELETE ON people 
  AFTER UPDATE OR DELETE ON locations
  AFTER INSERT OR UPDATE OR DELETE ON years 
  BEGIN
  EXECUTE PROCEDURE fn_create_view_resto_resid_lines()
    RETURN NEW; -- whatever this means but trying to get rid of ERROR:  control reached end of trigger procedure without RETURN
  END



Obe: 
Example 8-11. Trigger function to timestamp new and changed records 
CREATE OR REPLACE FUNCTION trig_time_stamper() RETURNS trigger AS -- 1
$$
BEGIN
    NEW.upd_ts := CURRENT_TIMESTAMP;
    RETURN NEW;
END;
$$
LANGUAGE plpgsql VOLATILE;

CREATE TRIGGER trig_1
BEFORE INSERT OR UPDATE OF session_state, session_id -- 2
ON web_sessions
FOR EACH ROW EXECUTE PROCEDURE trig_time_stamper(); 
