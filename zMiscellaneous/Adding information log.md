## Tracking information added
Mostly ad hoc to this point
See _Library/Ancestry to do in Evernote

### Adding information
1. Get documents labeled similarly
  YEAR Name of document. pxx. some description
2. Add the documents to docs table
3. Then add years, locations, people as needed. Reference all relevant docs.
4. Easiest to do a year/Directory at a time, so can keep track of where I'm at
5. 

When gathering documents from say MyHeritage, get Title Page and any pub info first, then look at restaurants to be clued into new names. Go through the Copied or downloaded documents log.numbers database and log any found

Note earlier (pre Mar 2020) I added the document again and again to the database, but now only one copy is added. Something to edit