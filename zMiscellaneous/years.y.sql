This works but may be superceded by create_connections_table.sql 2019.11.22

-- 68 rows in current database which may be in the ballpark, but what is the logic? Seems too complicated, but giving good results in 43 msec
-- Old database on MBP. Script results in 42 rows, but if change to 364 days plus minus comes in the same as Rails app at 49 items-- seems like the app is wrong.
-- Summary on MBP with same criteria, same count. Havent' compared the results for accuracy
-- Need to get both resto and resid address showing

Select 
 -- y1.id as year1_id,
 -- y2.id as year2_id,
 y1.id || ' ' ||  y2.id as y1_y2_id, 
 y1.person_id,
 p.given_name,
 p.last_name,
 -- p.given_name || ' ' || p.last_name as name, -- Doesn't create wide enough space so don't bother
 l.address, 
 -- l.address where l.id =  y1.location_id as y1_address, -- how do I do this
-- y1.location_id as y1_location_id, 
 -- y2.location_id as y2_location_id,
 y1.location_id || '  ' || y2.location_id as y1_y2_location_id,
 y1.resto as y1_resto,
 y1.resid as y1_resid,
 y2.resto as y2_resto,
 y2.resid as y2_resid,
 y1.year_date as y1_year_date, 
 y2.year_date as y2_year_date,
 y1.year_date -  y2.year_date as date_diff
From years y1,  years y2, people p, locations l -- which person and location. Need location for each
Where  y1.year_date between (y2.year_date-180) and (y2.year_date+180) -- Rails app has 364 days, Why?
     and y1.person_id = y2.person_id
     and p.id = y1.person_id   -- if both commented out, 316k results. One commented out 25k results
     and l.id = y1.person_id   -- if both commented out, 316k results. One commented out 25k results
     and y1.resto !=  y2.resto
     and y1.resid !=  y2.resid -- 2 rows fewer with this added
     and y1.resto = true
     and y2.resto = false     
Order by y1.year_date, p.last_name

-- All years data
Select * from years Order by year_date, person_id
Select * from locations Order by address
Select * from people Order by last_name, given_name

-- Declaration not working
-- days_plus_minus integer DEFAULT 180;
DO $$ 
  DECLARE days_plus_minus integer DEFAULT 180;
END $$;

Select 
 -- y1.id as year1_id,
 -- y2.id as year2_id,
 y1.id || ' ' ||  y2.id as year1_2_id, 
 y1.person_id,
 p.given_name,
 p.last_name, 
 -- p.given_name || ' ' || p.last_name as name, -- Doesn't create wide enough space so don't bother
 locations.address,
y1.resto_name as y1_resto_name, -- resto_name is in years
-- y1.address as y1_address, does not exist, duh, it's in locations
-- y2.resto_name as y2_resto_name, -- alway blank, so y2 is resid
-- y1.location_id as y1_location_id, 
 -- y2.location_id as y2_location_id,
-- locations.address where locations.id =  y1.location_id as y1_address,
 y1.location_id || '  ' || y2.location_id as y1_y2_location_id,
 y1.resto as y1_resto, 
 y1.resid as y1_resid, 
 y2.resto as y2_resto, 
 y2.resid as y2_resid, 
 y1.year_date as y1_year_date, 
 y2.year_date as y2_year_date,
 y1.year_date -  y2.year_date as date_diff
From years y1,  years y2, people p, locations -- which person and location. Need location for each
Where  y1.year_date between (y2.year_date-180) and (y2.year_date+180) -- Rails app has 364 days, Why?
     and y1.person_id = y2.person_id
     and p.id = y1.person_id   -- if both commented out, 316k results. One commented out 25k results
     and locations.id = y1.person_id   -- if both commented out, 316k results. One commented out 25k results
     and y1.resto !=  y2.resto
     and y1.resid !=  y2.resid -- 2 rows fewer with this added
     and y1.resto = true
     and y2.resto = false     
Order by y1.year_date, p.last_name, p.given_name

-- Go
Select * from years Order by year_date, person_id
-- Select * from locations Order by address

-- Info to kill pgAdmin 
SELECT
  pid,
  now() - pg_stat_activity.query_start AS duration,
  query,
  state
FROM pg_stat_activity
WHERE (now() - pg_stat_activity.query_start) > interval '5 minutes';


-- Results in 49 rows, but addresses don't match anything rational. The numbers are right but l.address,is meaningless
Select y1.id as year1_id, y2.id as year2_id, y1.person_id, p.given_name || ' ' || p.last_name as name, l.address, y1.location_id as y1_location_id, y2.location_id as y2_location_id, y1.resto as y1_resto, y1.resid, y2.resto as y2_resto, y2.resid, y1.year_date, y2.year_date
From years y1,  years y2, people p, locations l -- which  location. Need location for each
Where  y1.year_date between (y2.year_date-364) and (y2.year_date+364)
     and y1.person_id = y2.person_id
     and p.id = y1.person_id   -- if both commented out, 316k results. One commented out 25k results
     and l.id = y1.person_id   -- if both commented out, 316k results. One commented out 25k results
     and y1.resto !=  y2.resto
     and y1.resid !=  y2.resid -- 2 rows fewer with this added
     and y1.resto = true
     and y2.resto = false     
Order by y1.year_date, p.last_name
=========
--8673 results
Select y1.id as year1_id, y2.id as year2_id, y1.person_id, p.given_name || ' ' || p.last_name as name, l.address, l2.address, y1.location_id as y1_location_id, y2.location_id as y2_location_id, y1.resto as y1_resto, y1.resid, y2.resto as y2_resto, y2.resid, y1.year_date, y2.year_date
From years y1,  years y2, people p, locations l, locations l2 -- which person and location. Need location for each
Where  y1.year_date between (y2.year_date-364) and (y2.year_date+364)
     and y1.person_id = y2.person_id
     and p.id = y1.person_id   -- if both commented out, 316k results. One commented out 25k results
     and l.id = y1.person_id   -- if both commented out, 316k results. One commented out 25k results
     and y1.resto !=  y2.resto
     and y1.resid !=  y2.resid -- 2 rows fewer with this added
     and y1.resto = true
     and y2.resto = false     
Order by y1.year_date, p.last_name
===========
-- l.id is same as y1.person_id
Select y1.address as y1_address, y1.id as year1_id, y2.id as year2_id, y1.person_id as y1_person_id, p.given_name || ' ' || p.last_name as name, l.address, l.id as l_id, p.id as p_id, y1.location_id as y1_location_id, y2.location_id as y2_location_id, y1.resto as y1_resto, y1.resid, y2.resto as y2_resto, y2.resid, y1.year_date, y2.year_date
From years y1,  years y2, people p, locations l -- which  location. Need location for each
Where  y1.year_date between (y2.year_date-364) and (y2.year_date+364)
     and y1.person_id = y2.person_id
     and p.id = y1.person_id   -- if both commented out, 316k results. One commented out 25k results
     and l.id = y1.person_id   -- why ..l.id = but seem to work
     and y1.resto !=  y2.resto
     and y1.resid !=  y2.resid -- 2 rows fewer with this added
     and y1.resto = true
     and y2.resto = false     
Order by y1.year_date, p.last_name
==========
-- 
Select l1.address as l1_address, l2.address as l2_address, y1.id as year1_id, y2.id as year2_id, y1.person_id as y1_person_id, p.given_name || ' ' || p.last_name as name,  l1.id as l1_id, p.id as p_id, y1.location_id as y1_location_id, y2.location_id as y2_location_id, y1.resto as y1_resto, y1.resid, y2.resto as y2_resto, y2.resid, y1.year_date as y1_year_date, y2.year_date as y2_year_date
From years y1,  years y2, people p, locations l1, locations l2 -- which  location. Need location for each
Where  y1.year_date between (y2.year_date-364) and (y2.year_date+364)
     and y1.person_id = y2.person_id
     and p.id = y1.person_id   -- if both commented out, 316k results. One commented out 25k results
     and l1.id = y1.location_id   -- why ..l.id = but seem to work
     and l2.id = y2.location_id
--      and y1.location_id = l1.id    -- can't execute and empty query
-- 	 and y2.location_id = l2.id 
     and y1.resto !=  y2.resto
     and y1.resid !=  y2.resid -- 2 rows fewer with this added
     and y1.resto = true
     and y2.resto = false     
Order by y1.year_date, p.last_name
==========

-- Looks good, but confirm against baseline
Select l1.address as l1_address, l2.address as l2_address, y1.id as year1_id, y2.id as year2_id, p.id as p_id, p.given_name || ' ' || p.last_name as name,  l1.id as l1_id, l2.id as l2_id, y1.resto as y1_resto, y1.resid as y1_resid, y2.resto as y2_resto, y2.resid as y2_resid, y1.year_date as y1_year_date, y2.year_date as y2_year_date
From years y1,  years y2, people p, locations l1, locations l2 -- which  location. Need location for each
Where  y1.year_date between (y2.year_date-364) and (y2.year_date+364)
     and y1.person_id = y2.person_id
     and p.id = y1.person_id   -- if both commented out, 316k results. One commented out 25k results
     and l1.id = y1.location_id   -- why ..l.id = but seem to work
     and l2.id = y2.location_id
--      and y1.location_id = l1.id    -- can't execute and empty query
-- 	 and y2.location_id = l2.id 
     and y1.resto !=  y2.resto
     and y1.resid !=  y2.resid -- 2 rows fewer with this added
     and y1.resto = true  
     and y2.resto = false    
Order by y1.year_date, p.last_name
=======
-- Revise to  replace resto_resid lines table. Do in Rails and have it go once in a while. Or maybe just do in the database with a timer. Works, but needs to be verified
-- If (as I think) loc1 is resid_address change loc1 to loc_resid, and same for loc2 ==> loc_resto
-- Maybe true for y1 and y2. y1 ==> con_resid or visa versa
Select resto_loc.address as resto_loc_address, resid_loc.address as resid_loc_address, p.id as p_id, p.given_name || ' ' || p.last_name as name,  resto_loc.id as resto_loc_id, resid_loc.id as resid_loc_id, y1.year_date as y1_year_date, y2.year_date as y2_year_date, y1.id as year1_id, y2.id as year2_id
From years y1,  years y2, people p, locations resto_loc, locations resid_loc -- which  location. Need location for each
Where  y1.year_date between (y2.year_date-364) and (y2.year_date+364)
     and y1.person_id = y2.person_id
     and p.id = y1.person_id 
     and resto_loc.id = y1.location_id 
     and resid_loc.id = y2.location_id
     and y1.resto !=  y2.resto
     and y1.resid !=  y2.resid
     and y1.resto = true  
     and y2.resto = false    
Order by y1.year_date, p.last_name, p.given_name
======
-- Revise to  replace resto_resid lines table. Do in Rails and have it go once in a while. Or maybe just do in the database with a timer--use the application_controller repopulateResidResto and put the SQL in there. Works, but needs to be verified
-- y=years is the database with information for that person-location-date-resto or resid
THIS WORKS TO PRODUCE A VIEW OF THE DATA. SHOULD I REWRITE AS JOIN? AND THEN TO CREATE A TABLE AS A PROCEDURE?
LOOKS LIKE I'M TRYING A TABLE DOWN BELOW'
Select resto_loc.address as resto_address, resid_loc.address as resid_address, p.id as p_id, p.given_name || ' ' || p.last_name as name,  resto_loc.id as resto_loc_id, resid_loc.id as resid_loc_id, y_resto.year_date as resto_date, y_resid.year_date as resid_date, y_resto.id as y_resto_id, y_resid.id as y_resid_id
From years y_resto,  years y_resid, people p, locations resto_loc, locations resid_loc
Where  y_resto.year_date between (y_resid.year_date-364) and (y_resid.year_date+364)
     and y_resto.person_id = y_resid.person_id
     and p.id = y_resto.person_id 
     and resto_loc.id = y_resto.location_id 
     and resid_loc.id = y_resid.location_id
     and y_resto.resto !=  y_resid.resto
     and y_resto.resid !=  y_resid.resid
     and y_resto.resto = true  
     and y_resid.resto = false    
Order by y_resto.year_date, p.last_name, p.given_name
-- 71 rows. Some semi duplicates, meaning different uploads that need to be consolidated. But not seeing all the duplicates of entries in years
-- Not seeing early events
======
resto_resid_lines fields of interest
person_id | resto_date | resid_date |  title_resto  | title_resid |      resto_name       |         created_at         |         updated_at         | resto_connection_id | resid_connection_id | resto_loc_id | resid_loc_id | lat_resid | long_resid  | lat_resto | long_resto  |         resto_address         | resid_address  |  mid_date
======
-- field names from 
CREATE TABLE resto_resid_lines
p.id AS person_id, y_resto.year_date AS resto_date, y_resid.year_date AS resid_date, y_resto.title AS title_resto, y_resid.title AS title_resid, y_resto.resto_name AS resto_name, y_resto.id AS resto_connection_id, y_resid.id AS resid_connection_id, resto_loc.id AS resto_loc_id, resid_loc.id AS resid_loc_id, y_resid.lat AS lat_resid, y_resid.long AS long_resid, y_resto.lat AS lat_resto, y_resto.long AS long_resto, y_resto.address AS resto_address, y_resid.address AS resid_address, mid_date
========
mid_date = (y_resto.year_date + y_resid.year_date)/2
Select p.id AS person_id, y_resto.year_date AS resto_date, y_resid.year_date AS resid_date, y_resto.title AS title_resto, y_resid.title AS title_resid, y_resto.resto_name AS resto_name, y_resto.id AS resto_connection_id, y_resid.id AS resid_connection_id, resto_loc.id AS resto_loc_id, resid_loc.id AS resid_loc_id, y_resid.lat AS lat_resid, y_resid.long AS long_resid, y_resto.lat AS lat_resto, y_resto.long AS long_resto, y_resto.address AS resto_address, y_resid.address AS resid_address, mid_date
From years y_resto,  years y_resid, people p, locations resto_loc, locations resid_loc
Where  y_resto.year_date between (y_resid.year_date-364) and (y_resid.year_date+364)
     and y_resto.person_id = y_resid.person_id
     and p.id = y_resto.person_id 
     and resto_loc.id = y_resto.location_id 
     and resid_loc.id = y_resid.location_id
     and y_resto.resto !=  y_resid.resto
     and y_resto.resid !=  y_resid.resid
     and y_resto.resto = true  
     and y_resid.resto = false    
Order by y_resto.year_date, p.last_name, p.given_name


