#!/bin/zsh -f
  osascript << END
    tell application "iTerm"
      make new terminal
      tell the first terminal
          activate current session
          launch session "Default Session"
          tell the last session
            write text "cd \"$TM_DIRECTORY\""
            write text "clear"
            write text "pwd"
          end tell
      end tell
    end tell
  END

# cd to the directory containing the file displayed in TextMate
#
# The first example is a command to open a new iTerm tab and cd to the directory that contains the file being edited.