-- so far haven't finished this TODO. But may not need
CREATE OR REPLACE VIEW public.view_resto_or_resid AS
SELECT 
   resto.person_id,
   p.given_name AS first_name,
   p.last_name,
   (rtrim(p.given_name::text) || ' '::text) || rtrim(p.last_name::text) AS full_name,
   resto.year_date AS resto_date,
   resid.year_date AS resid_date,
   resid.year_date + ((resto.year_date - resid.year_date)/2) AS mid_date,  -- can't add dates, but can substract and end up with days (interval) which can be added to a date
   resto.resto_name,
   resto_loc.address AS resto_address,
   resto.title AS title_resto,
   resid_loc.address AS resid_address,
   resid.title AS title_resid,
   resto_loc.longitude AS long_resto,
   resto_loc.latitude AS lat_resto,
   resid_loc.longitude AS long_resid,
   resid_loc.latitude AS lat_resid,
   rtrim(resto.notes) AS resto_notes,
   rtrim(resid.notes) AS resid_notes,
   p.date_of_birth,
   resto.id AS resto_connection_id,
   resid.id AS resid_connection_id,
   resto.location_id AS resto_loc_id,
   resid.location_id AS resid_loc_id
  FROM years
    JOIN people p ON resto.person_id = p.id
    JOIN locations resto_loc ON resto.location_id = resto_loc.id
    JOIN locations resid_loc ON resid.location_id = resid_loc.id
 WHERE resto.person_id = resid.person_id AND resto.resto = true AND resto.resto <> resid.resto AND resto.resid <> resid.resid AND resto.resto = true AND resid.resto = false
 ORDER BY resto.year_date, p.last_name, p.given_name;
 --copied back from database 2019.11.25
 -- reran with full_name 2019.11.27 since thought maybe a reserve word problem with name. Not sure this did it. Need to run fn_view_resto_resid_lines