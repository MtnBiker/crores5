The flow to get the locations showing on People > Show (place where a person worked or lived)

<!-- https://stackoverflow.com/questions/59548291/rails-passing-variable-to-jbuilder -->
<!-- https://stackoverflow.com/questions/60610788/rails-connecting-to-jbuilder -->

<!--  views/people/show.html.erb-->
      <div id="map" class="map" data-url="<%= person_locations_path(@person) %>" data-line="<%= person_lines_path(@person) %>" ></div> <!-- data-url isn't just any old variable, the data- is key, maybe can pass another word after because other defs don't pass i.e.,  data-line="<%= person_locations_path(@person) %>"-->
      <%= javascript_pack_tag 'olPersonMap' %>
      
      # passes `/people/124/locations.` to olPersonMap
      
      # // javascript/packs/olPeopleMap.js
35     var urlJSON = $('#map').data('url') + '.json' // /people/124/locations.json and when called below builds the json I think
      // below is here:
267   new VectorLayer({
        title: 'Where lived and worked',
        imageRatio: 2,
        source: new VectorSource({
          url: urlJSON,
          format: new GeoJSON()
      })
      
## app/views/people/locations/index.json.jbuilder is called which builds the geojson to put on the show page
## another piece is the # app/controllers/people/locations_controller.rb

which makes the link to index.json.jbuilder and sets and may send the right variable to 

      @person = Person.eager_load(:locations)
                      .find(params[:person_id])
      @locations = @person.locations
      
      # and routes.rb
      resources :people do
        resources :locations, only: [:index], module: :people
      end
      
## app/views/people/locations/index.json.jbuilder
json.type "FeatureCollection"
## http://localhost:3000/people/52/locations.json is where this file ends up
json.features @person.years do |year| 
  json.type "Feature"
  json.year_id year.id # not using but is handy for debugging and maybe I'll use it
  json.name year.person.full_name
  
  if year.resto ## maybe use different way to get later but keep it separate 
    json.properties do
      json.date year.year_date
      json.popup year.location.address
      json.resto_name year.resto_name
      json.icon 'restoIcon'
      json.set! "marker-color", "#C978F3"
    end # json.properties resto
  else # resid
    json.properties do
      json.date year.year_date
      json.popup year.location.address
      json.set! "marker-color", "#7E0FB7"
      json.icon 'residIcon'
    end # json.properties resid
  end # if year.resto

  json.geometry do
    json.type "Point"   
    json.coordinates [year.location.longitude.to_f, year.location.latitude.to_f]
  end # json.geometry
end # json.features

and returns, one line of http://localhost:3000/people/5/locations.`json`

{"type":"FeatureCollection","features":[{"type":"Feature","year_id":588,"name":"Anthony Milovich","properties":{"date":"1886-09-01","popup":"50 Wilmington","marker-color":"#7E0FB7","icon":"residIcon"},"geometry":{"type":"Point","coordinates":[-118.240165,34.051331]}},
