-- Avoiding image (:document) duplicates development
-- A test to see what shows up
    Select *
    from active_storage_blobs
    where checksum = '0M4nc4nuUaVuqo3+sJw+Lg=='
-- works, now translate to RoR

-- Gives an idea of duplicates
SELECT * FROM public.active_storage_attachments
ORDER BY record_id ASC LIMIT 100

item = ActiveStorage::Blob(checksum)
item = ActiveStorage::Blob.find_by(checksum) == '0M4nc4nuUaVuqo3+sJw+Lg=='
---------
Select given_name, last_name, created_at
From People  Order by last_name Limit 200

SELECT * FROM action_text_rich_texts

SELECT * FROM view_resto_resid_lines Order by mid_date

-- List of all people with associated dates /years table has this informtion
-- But also need a list of people with year_date and whether resto or resid sorted by person to know which people to look for when doing research and will also show "active" dates for that person. Active being either in LA and in resto business

Select full_name, list of year_date's'
From Years  Order by last_name, given_name Limit 200
type if resto 'resto' else 'resid'

Select rtrim(given_name) || ' ' || rtrim(last_name) , year_date
From Years  Order by last_name, given_name Limit 200
type if resto 'resto' else 'resid'


-- Works, maybe better if last and first separate
Select y.year_date , p.given_name || ' ' || p.last_name as name
From Years AS y, People as p
Where y.person_id = p.id
Order by p.last_name
  Limit 200 -- only needed with TM because defaults to 5 or 10

  -- Works, maybe better if last and first separate
  Select y.year_date , y.title, p.given_name, p.last_name, y.source
  From Years AS y, People as p
  Where y.person_id = p.id
  Order by p.last_name, p.given_name,  y.year_date
    Limit 500 -- only needed with TM because defaults to 5 or 10


    -- Works and is good, but can I have a list of directory years for person (of course no 'title')
Select y.year_date , y.title, p.given_name, p.last_name, y.source, date_part('year', y.year_date + 365)  as Est_Directory_Year
From Years AS y, People as p
Where y.person_id = p.id
Order by p.last_name, p.given_name,  y.year_date
Limit 500 -- only needed with TM because defaults to 5 or 10

    -- as above, but sorted by year first
Select y.year_date , y.title, p.given_name, p.last_name, y.source, date_part('year', y.year_date + 365)  as Est_Directory_Year
From Years AS y, People as p
Where y.person_id = p.id
Order by y.year_date, p.last_name, p.given_name
Group by Est_Directory_Year
Limit 500 -- only needed with TM because defaults to 5 or 10

--Maybe better organized than previous
Select y.year_date, date_part('year', y.year_date + 365)  as Est_Directory_Year , y.title, p.given_name, p.last_name, y.source
From Years AS y, People as p
Where y.person_id = p.id
Order by y.year_date, p.last_name, p.given_name
Limit 500 -- only needed with TM because defaults to 5 or 10

-- For one person and one type (resto or resid)
Select year_date as date,  p.given_name || ' ' || p.last_name as full_name, y.resto, y.resid, l.longitude, l.latitude, title, l.address
From years y
inner join people p on y.person_id = p.id
inner join locations l on y.location_id = l.id
where  person_id = 5  and resid = true and l.city = 'Los Angeles'-- these will vary
order by year_date

   