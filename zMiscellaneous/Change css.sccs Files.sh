#!/bin/sh
# Removes the .css portion which is not supported in Sprockets 4
for file in $(find ./app/assets/stylesheets/ -name "*.css.scss")
do
    git mv $file `echo $file | sed s/\.css//`
done