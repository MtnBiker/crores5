require 'test_helper'

class UsersEditTest < ActionDispatch::IntegrationTest
  def setup
    @user = users(:michael)
  end

  test 'unsuccessful edit' do
    log_in_as(@user)
    get edit_user_path(@user)
    assert_template 'users/edit'
    patch user_path(@user), params: { user: { name: '',
                                              email: 'foo@invalid',
                                              password: 'foo',
                                              password_confirmation: 'bar' } }
    assert_template 'users/edit'
  end

  test 'successful edit with friendly forwarding' do
    get edit_user_path(@user)
    # puts "+++++ #{edit_user_path(@user)} ++++++++++" # >> /users/762146111/edit
    log_in_as(@user)
    assert_redirected_to edit_user_path(@user)
    name  = 'Foo Bar'
    email = 'foo@bar.com'
    patch user_path(@user), params: { user: { name: name,
                                              email: email,
                                              password: '',
                                              password_confirmation: '' } }
    assert_not flash.empty?
    assert_redirected_to @user
    @user.reload
    assert_equal name,  @user.name
    assert_equal email, @user.email
  end
end

# UsersEditTest#test_successful_edit_with_friendly_forwarding [/Users/gscar/Documents/Croatian Restaurants Project-CroRes/crores5/test/integration/users_edit_test.rb:23]:
# Expected response to be a redirect to <http://www.example.com/users/762146111/edit> but was a redirect to <http://www.example.com/>.
#  From users.yml: email: michael@example.com
