require 'test_helper'

#  https://api.rubyonrails.org/classes/ActiveRecord/FixtureSet.html Using Fixtures in TEst Cases
class LocationsTest < ActiveSupport::TestCase
  test 'locations_count' do
    assert_equal 3, Location.count # Should be 3 because fixture has three, but currently not failing on 2. Fixed
  end

  test 'find one in locations' do
    assert_equal 'Los Angeles', locations(:one).city # this does not generate an error, so not happening?. FIXED
    # puts "Did this happen from locations_controller_test?"
  end
end
