require 'test_helper'

class SiteLayoutTest < ActionDispatch::IntegrationTest
  test 'layout links' do
    get root_path
    assert_template 'static_pages/home'
    assert_select 'a[href=?]', root_path, count: 2
    assert_select 'a[href=?]', about_path
    assert_select 'a[href=?]', contact_path
    assert_select 'a[href=?]', addresses_path
    assert_select 'a[href=?]', people_path
    assert_select 'a[href=?]', connections_path
    get signup_path
    # assert_select "title", full_title("Sign up") # This was giving a rake test error, so am trying the following
    # same error: undefined method `full_title' for #
    # assert_equal full_title("Sign up"), "Sign up | Croatian Restaurants in Los Angeles 1880–1930"
  end

  test 'Connections-resto_resid_lines' do
    get resto_resid_lines_path
    assert_template 'resto_resid_lines/index'
    # I don't think this will be on page  if nothing to show, i.e., the database not being updated if a change to people, locations, or connections
    assert_select '.table-body'
    # These are in both the header and body so will be there with or without data (missed that always there and originally had as the sole test untll I created .table-body)
    assert_select 'div.col-md-3'
    assert_select 'div.col-md-4'
    assert_select 'div.col-md-5'
  end
end
