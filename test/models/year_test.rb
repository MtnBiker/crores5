require 'test_helper'
# require 'active_record/fixtures' # didn't help with problem
# Should this be a controller test?

class YearModelTest < ActiveSupport::TestCase # < ActionDispatch::IntegrationTest #
  # following Hartl p 551. We want connections to show in :earliestlogical order.
  # Note that the model defines: default_scope -> { order(:year_date) }
  # But at the moment it's not year_date in the following test, although the syntax follows Hartl example
  # But Hartl is using created_at

  # Here years corresponds to the fixture filename years.yml, while the symbol :earliest references year with the key shown in Listing 8.22. Section 8.2.4 online

  def setup
    @year = years(:earliest)
    puts "1. @year: #{@year}"
  end

  test 'order should be earliest first, i.e., chronological' do
    # assert_equal Year.first, years(:earliest) # :earliest is in the /test/fixtures/year.yml and year_date for :earliest is the earliest.
    # Since default_scope -> { order(:year_date) }, Year.first should be the earliest
    # firstYear = Year.first
    # puts "1. firstYear.year_date: #{firstYear.year_date}" #
    puts "2. Year.first.year_date: #{Year.first.year_date}. Pulling from .yml not from database. 1886 is the earliest in database and \n   @year.year_date (years(:earliest)): #{@year.year_date}" # . .yml one is 1889
    assert_equal Year.first.year_date, @year.year_date # How do I get earliest?
    puts 'Hey we just tested the order of the listing in years (Connections) from models/year_test.rb'
  end

  test 'reject large images' do # TODO
    # @refImage.size = 6.megabytes
    # assert_not @refImage.valid?
  end
end
