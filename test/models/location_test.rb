require 'test_helper'

class LocationTest < ActiveSupport::TestCase
  def setup
    @location = Location.new(address: '123 1st St')
  end

  test 'should be valid' do
    assert @location.valid?
  end
end
