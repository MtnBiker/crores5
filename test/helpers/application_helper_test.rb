require 'test_helper'

class ApplicationHelperTest < ActionView::TestCase
  test 'full title helper' do
    assert_equal full_title,         'Croatian Operated Restaurants in Early Los Angeles'
    assert_equal full_title('Help'), 'Help | Croatian Operated Restaurants in Early Los Angeles'
  end
end
