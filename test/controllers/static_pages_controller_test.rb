#  rails test test/controllers/static_pages_controller_test.rb
require 'test_helper'

class StaticPagesControllerTest < ActionDispatch::IntegrationTest
  def setup
    @base_title = 'Croatian Operated Restaurants in Early Los Angeles'
  end

  test 'should get home' do
    get root_path
    assert_response :success
    assert_select 'title', @base_title.to_s
  end

  test 'should get help' do
    get help_path
    assert_response :success
    assert_select 'title', "Help | #{@base_title}"
  end

  test 'should get about' do
    get about_path
    assert_response :success
    assert_select 'title', "About | #{@base_title}"
  end

  test 'should get contact' do
    get contact_path
    assert_response :success
    assert_select 'title', "Contact | #{@base_title}"
  end

  test 'should get acknowledgements' do
    get acknowledgements_path
    assert_response :success
    assert_select 'title', "Acknowledgements and Resources | #{@base_title}"
  end

  test 'should get illich' do
    get illich_path
    assert_response :success
    assert_select 'title', "Intro. and Illich | #{@base_title}"
  end

  test 'should get marietich' do
    get marietich_path
    assert_response :success
    assert_select 'title', "Jack Marietich | #{@base_title}"
  end
end
