require 'test_helper'

class PeopleControllerTest < ActionController::TestCase
  setup do
    @person = people(:one)
  end

  test 'should get index' do
    # get :index # https://www.sitepoint.com/onwards-to-rails-5-additions-changes-and-deprecations/
    process :index, method: :get
    assert_response :success
    assert_not_nil assigns(:people)
  end

  test 'should get new' do
    process :new, method: :get
    assert_response :success
  end

  test 'should create person' do
    puts "from the people_controller_test > should create person. Person.count: #{Person.count}"
    assert_difference('Person.count') do
      # person :create, :person => { :given_name => "Samson"}
      # The above is from
      process :create, params: { person: {
        date_of_birth: @person.date_of_birth,
        place_of_birth: @person.place_of_birth,
        date_of_death: @person.date_of_death,
        date_of_citizenship: @person.date_of_citizenship,
        date_of_entry: @person.date_of_entry,
        doe_source: @person.doe_source,
        given_name: @person.given_name,
        last_name: @person.last_name,
        # name: @person.name, #deprecated
        source: @person.source,
        notes: @person.notes
      } }
    end
    assert_redirected_to person_path(assigns(:person))
  end

  # https://guides.rubyonrails.org/v5.1/testing.html Section 6.2.1

  # ActionController::UrlGenerationError: No route matches {:action=>"/people/new", :controller=>"people"}
  #     in the line below, people_controller has a new action.
  test 'can create a new person' do
    # get "/people/new"
    process :new, method: :get
    assert_response :success

    post '/people',
         params: { person: { given_name: 'can create', last_name: 'person successfully.' } }
    assert_redirected_to person_path(assigns(:person))
  end

  test 'should show person' do
    process :show, method: :get, params: { id: @person }
    assert_response :success
  end

  test 'should get edit' do
    process :edit, method: :get, params: { id: @person }
    assert_response :success
  end

  test 'should update person' do
    patch :update, params: { id: @person, person: { date_of_birth: @person.date_of_birth, date_of_citizenship: @person.date_of_citizenship, date_of_entry: @person.date_of_entry, doe_source: @person.doe_source, given_name: @person.given_name, last_name: @person.last_name, notes: @person.notes } } #  name: @person.name, # deprecated
    assert_redirected_to person_path(assigns(:person))
  end

  test 'should destroy person' do
    assert_difference('Person.count', -1) do
      delete :destroy, params: { id: @person }
    end

    assert_redirected_to people_path
  end
end
