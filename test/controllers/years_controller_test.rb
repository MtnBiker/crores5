require 'test_helper'

class YearsControllerTest < ActionController::TestCase
  setup do
    @year = years(:one)
  end

  test 'should get index' do
    # get :index # https://www.sitepoint.com/onwards-to-rails-5-additions-changes-and-deprecations/
    process :index, method: :get
    assert_response :success
    assert_not_nil assigns(:years)
  end

  test 'should get new' do
    process :new, method: :get # Was: get :new
    assert_response :success
  end

  test 'should create year' do
    assert_difference('Year.count') do
      # post :create, params: { year: { year_date: "1893-09-01",
      process :create, method: :post, params: { year: { year_date: '1893-09-01',
                                                        created_at: nil,
                                                        updated_at: nil,
                                                        resto: false,
                                                        resid: true,
                                                        source: 'TEST',
                                                        person_id: 99,
                                                        location_id: 27,
                                                        title: 'Proprietor',
                                                        notes: '',
                                                        resto_name: '',
                                                        aws_url: '',
                                                        ref_url: '',
                                                        snippet_file_name: nil,
                                                        snippet_content_type: nil,
                                                        snippet_file_size: nil,
                                                        snippet_updated_at: nil,
                                                        ref_image_file_name: nil,
                                                        ref_image_content_type: nil,
                                                        ref_image_file_size: nil,
                                                        ref_image_updated_at: nil,
                                                        refImage: nil } }
      # Note that the person and location id's are needed in their respective yml
    end
    assert_redirected_to year_path(assigns(:year))
  end

  test 'should show year' do
    process :show, method: :get, params: { id: @year }
    assert_response :success
  end

  test 'should get edit' do
    process :edit, method: :get, params: { id: @year } # get :edit, params: { id: @year }
    assert_response :success
  end

  test 'should update year' do
    patch :update, params: { id: @year, year: { year_date: @year.year_date } }
    assert_redirected_to year_path(assigns(:year))
  end

  # Now failing because can't find image which was a field removed from years long ago
  test 'should destroy year' do
    assert_difference('Year.count', -1) do
      puts '{params: { id: @year }: '
      puts params: { id: @year }
      puts "+++++++++++ @year: #{@year}. @year.year_date: #{@year.year_date}. params: { id: @year } ++++++" # . @year.year_image: #{@year.year_image} gave an error as expected
      delete :destroy, params: { id: @year }
    end

    assert_redirected_to years_path
  end
end
