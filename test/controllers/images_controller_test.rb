require 'test_helper'

#  NameError - uninitialized constant ImagesController: So commented out this 2019.11.01
#  but it made no difference so turned it back on. I think the problem is elsewhere
class ImagesControllerTest < ActionDispatch::IntegrationTest
  setup do
    @image = images(:one)
  end

  test 'should get index' do
    get images_url
    assert_response :success
  end

  test 'should get new' do
    get new_image_url
    assert_response :success
  end

  test 'should create image' do
    assert_difference('Image.count') do
      post images_url,
           params: { image: { description: @image.description, file: @image.file, notes: @image.notes,
                              ref: @image.ref, source_url: @image.source_url, transcript: @image.transcript, url: @image.url } }
    end

    assert_redirected_to image_url(Image.last)
  end

  test 'should show image' do
    get image_url(@image)
    assert_response :success
  end

  test 'should get edit' do
    get edit_image_url(@image)
    assert_response :success
  end

  test 'should update image' do
    patch image_url(@image),
          params: { image: { description: @image.description, file: @image.file, notes: @image.notes,
                             ref: @image.ref, source_url: @image.source_url, transcript: @image.transcript, url: @image.url } }
    assert_redirected_to image_url(@image)
  end

  # TODO: reactivate this if I still need images after 5.1. Never got beyond putting in two data points.
  # test "should destroy image" do
  #   assert_difference('Image.count', -1) do
  #     delete image_url(@image) # What I had. Below suggests how this is different, ie, url
  #     # delete :destroy, params: { id: @image } # from people Error: URI::InvalidURIError: bad URI(is not URI?): "http://www.example.com:80destroy"
  #   end
  #   assert_redirected_to images_url
  # end
end
