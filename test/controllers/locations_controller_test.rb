require 'test_helper'

class LocationsControllerTest < ActionController::TestCase
  setup do
    @location = locations(:one)
  end

  test 'should get index' do
    # get :index # Changed with later Rails
    process :index, method: :get # https://www.sitepoint.com/onwards-to-rails-5-additions-changes-and-deprecations/
    assert_response :success
    assert_not_nil assigns(:locations)
  end

  test 'should get new' do
    process :new, method: :get
    assert_response :success
  end

  # Giving an error on count. But the app can create a location, so the error is in the test
  test 'should create location' do
    assert_difference('Location.count') do
      # post :create, params: { location: { address: "1 New Address", # change per https://www.sitepoint.com/onwards-to-rails-5-additions-changes-and-deprecations/
      process :create, method: :post, params: { location: { address: '1 New Address',
                                                            city: @location.city,
                                                            state: @location.state,
                                                            longitude: @location.longitude,
                                                            latitude: @location.latitude,
                                                            # geom: @location.geom, # Going in circles TODO
                                                            coords_not_locked: @location.coords_not_locked,
                                                            geocoded_with: @location.geocoded_with,
                                                            extant: @location.extant,
                                                            current_description: @location.current_description,
                                                            source: @location.source,
                                                            source_url: @location.source_url,
                                                            notes: @location.notes } }
    end

    assert_redirected_to location_path(assigns(:location))
  end

  test 'should show location' do
    process :show, method: :get, params: { id: @location } # get :show, params: { id: @location }
    assert_response :success
  end

  test 'should get edit' do
    process :edit, method: :get, params: { id: @location }
    assert_response :success
  end

  test 'should update location' do
    patch :update, params: { id: @location, location: { address: @location.address,
                                                        city: @location.city,
                                                        state: @location.state,
                                                        longitude: @location.longitude,
                                                        latitude: @location.latitude,
                                                        # geom: @location.geom, #TODO
                                                        coords_not_locked: @location.coords_not_locked,
                                                        geocoded_with: @location.geocoded_with,
                                                        extant: @location.extant,
                                                        current_description: @location.current_description,
                                                        source: @location.source,
                                                        source_url: @location.source_url,
                                                        notes: @location.notes } }
    assert_redirected_to location_path(assigns(:location))
  end

  test 'should destroy location' do
    assert_difference('Location.count', -1) do
      delete :destroy, params: { id: @location }
    end

    assert_redirected_to locations_path
  end
end
