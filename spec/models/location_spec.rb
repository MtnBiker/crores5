require 'rails_helper'

RSpec.describe Location, type: :model do
  # Ignoring this one for the moment that used FactoryBot
  # it "is valid with address, coordinates " do
  FactoryBot.create(:location) # Just started experimenting with this but commented out since gone for a month
  #   expect(location).to be_valid
  #   location = Location.new(
  #     address: "123 S Main St",
  #     city:    "Los Gatos",
  #     longitude: -118.24,
  #     latitude:  34.02,
  #   )
  #   expect(location).to be_valid
  # end

  it 'is invalid without an address' do # should fail because default is longer than 50 characters
    location = Location.new(address: nil)
    location.valid?
    expect(location.errors[:address]).to include("can't be blank")
  end

  it 'is invalid if address not at least 5 characters' do # The validation works at Heroku and the message is as follows
    location = Location.new(
      address: '123',
      city: 'Los Angeles'
    )
    location.valid?
    # expect(location.errors[:address]).to include("is too short (minimum is 5 characters)")
    expect(location.errors[:address]).to include(a_string_starting_with('is too short'))
  end

  it 'is invalid if address.length >51 characters' do
    location = Location.new(
      address: 'Follow the steps below to determine which FOX Transfer Seatpost will work best for you and your bike frame. This guide can be a helpful tool for selecting the appropriate model of FOX Transfer Seatpost before purchase. You will need your frame and original rigid seatpost to make this determination.',
      city: 'Los Angeles'
    )
    location.valid?
    # expect(location.errors[:address]).to include("is too long (maximum is 50 characters)")
    expect(location.errors[:address]).to include(a_string_starting_with('is too long'))
  end

  it 'is invalid if "street" address is not unique' do
    Location.create(
      address: '123 S Main St',
      city: 'Los Gatos'
    )
    location = Location.new(
      address: '123 S Main St',
      city: 'Los Gatos'
    )
    location.valid?
    expect(location.errors[:address]).to include('has already been taken')
  end
end # RSpec.describe Location…
