require 'rails_helper'

RSpec.describe Person, type: :model do
  before do
    @person = Person.new(
      given_name: 'Sam',
      last_name: 'Theman',
      id: '25'
    )
  end
  it 'is valid with a first and last name' do
    expect(@person).to be_valid
  end

  it 'returns full_name as a string' do # Removing def from model breaks
    expect(@person.full_name).to eq 'Sam Theman' # Also breaks if make this just Sam
  end

  it 'returns full_name (id) (what it this for?)' do
    expect(@person.full_name_id).to eq 'Sam Theman (25)'
  end

  # creating a new block here for long names. Probably should have a block above too
  describe 'long names invalid' do # Confirmed by removing check from model
    before(:each) do
      @person = Person.new(
        given_name: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris',
        last_name: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco '
      )
    end
    it 'given_name invalid with more than 50 characters' do
      @person.valid?
      expect(@person.errors[:last_name]).to include(a_string_starting_with('is too long'))
    end
    it 'last_name invalid if more than 50 characters' do
      @person.valid?
      expect(@person.errors[:given_name]).to include(a_string_starting_with('is too long'))
    end
  end # long names invalid

  it 'do the searches'
end

# Before DRYing
# RSpec.describe Person, type: :model do
#   it 'is valid with first and last name' do
#     person = Person.new(
#     given_name: "Sam",
#     last_name:  "Theman",
#    )
#    expect(person).to be_valid
#   end
#
#   it 'returns full name as a string' do
#     person = Person.new(
#     given_name: "Sam",
#     last_name:  "Theman",
#    )
#    expect(person.full_name).to eq "Sam Theman"
#   end
#
#   it 'returns full_name (id) (what it this for?)' do
#     person = Person.new(
#     given_name: "Sam",
#     last_name:  "Theman",
#     id:         "25",
#    )
#    expect(person.full_name_id).to eq "Sam Theman (25)"
#   end
#
#   it 'invalid if more than 50 characters ' do
#
#   end
#
# end
