# Modeled on https://www.codewithjason.com/rails-integration-tests-rspec-capybara/
require 'rails_helper'

RSpec.describe 'Creating a new location', type: :feature do
  scenario 'valid inputs' do
    visit new_location_path
    fill_in 'Address', with: '1 W 1st St'
    fill_in 'City', with: 'Los Angeles'
    click_on 'Create/Update Location'
    visit locations_path
    expect(page).to have_content '1 W 1st St'
    expect(page).to have_content 'Los Angeles'
  end

  scenario 'invalid inputs' do
    visit new_location_path
    fill_in 'Address', with: ''
    click_on 'Create/Update Location'
    expect(page).to have_content("can't be blank")
  end
end
