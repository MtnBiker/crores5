# Modeled on https://www.codewithjason.com/rails-integration-tests-rspec-capybara/
require 'rails_helper'

RSpec.describe 'Creating a new connection', type: :feature do
  scenario 'valid inputs' do
    visit new_year_path
    expect(page).to have_select 'Martin Bozina (85)'
    expect(page).to have_select '117 Main St'
    expect(page).to have_select '1 Sep 1874'
    click_on 'Create/Update Connection'
    visit year_path
    expect(page).to have_content 'Martin Bozina'
    expect(page).to have_content '117 Main St'
    expect(page).to have_content '85'
    expect(page).to have_content '1 Sep 1874'
  end

  # scenario 'invalid inputs' do
  #   visit new_year_path
  #   fill_in 'Last name', with: ''
  #   click_on 'Create/Update Person'
  #   expect(page).to have_content("can't be blank")
  # end
end
