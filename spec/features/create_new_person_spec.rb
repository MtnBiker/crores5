# Modeled on https://www.codewithjason.com/rails-integration-tests-rspec-capybara/
require 'rails_helper'

RSpec.describe 'Creating a new person', type: :feature do
  scenario 'valid inputs' do
    visit new_person_path
    fill_in 'Given name', with: 'Ralph'
    fill_in 'Last name', with: 'Smith'
    click_on 'Create/Update Person'
    visit people_path
    expect(page).to have_content 'Ralph'
    expect(page).to have_content 'Smith'
  end

  scenario 'invalid inputs' do
    visit new_person_path
    fill_in 'Last name', with: ''
    click_on 'Create/Update Person'
    expect(page).to have_content("can't be blank")
  end
end
