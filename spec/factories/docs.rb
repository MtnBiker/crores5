FactoryBot.define do
  factory :doc do
    source { 1 }
    page_no { 'MyString' }
    image { 'MyString' }
    original_url { 'MyString' }
    basename { 'MyString' }
  end
end
