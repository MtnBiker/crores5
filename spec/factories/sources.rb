FactoryBot.define do
  factory :source do
    year { 1 }
    name { 'MyString' }
    publisher { 'MyString' }
    source { 'MyString' }
    from_where { 'MyString' }
    from_where_url { 'MyString' }
    caption { 'MyString' }
    pub_date { '2020-03-19' }
    notes { 'MyText' }
  end
end
