FactoryBot.define do
  factory :location do
    address    { '123 S Main St' }
    city       { 'Los Gatos' }
    longitude  { -118.24 }
    latitude   { 34.02 }
  end
end

# This gives a similar error:  undefined method 'name' in 'user' factory, but then we are in locations
# FactoryBot.define do
#  factory :user do
#    name "Aaron"
#    last_name  "Sumner"
#    email "tester@example.com"
#    password "dottle-nouveau-pavilion-tights-furze"
#  end
# end
