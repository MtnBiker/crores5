require 'spec_helper'

# describe 'resto_resid_line/index.html.erb' do
#   # it 'renders a list of residences and restaurants for a person on a date' do
#   #
#   #
#   #   assign(:article, double(Article).as_null_object)
#   #   render
#   #   expect(rendered).to have_selector('form',
#   #     method: 'post',
#   #     action: articles_path
#   #   ) do |form|
#   #     expect(form).to have_selector('input', type: 'submit')
#   #   end
#   # end
# end

# I tried to copy from https://github.com/rubocop-hq/rspec-style-guide#views, but it doesn't look like what Marston and Dees show
# RSpec.describe Year, type: :view do

# end

#  From https://www.rubydoc.info/github/teamcapybara/capybara#using-capybara-with-rspec
#  Same errors as for other examples: NameError: uninitialized constant Location
# RSpec.describe "locations/show.html.erb", type: :view do
#   it "displays the location title" do
#     assign :location, Location.new(title: "Address (Restaurant and Residental)")
#
#     render
#
#     expect(rendered).to have_css("header h1", text: "Address (Restaurant and Residental)")
#   end
# end

# spec/views/resto_resid_line/index.html.erb_spec.rb
# based on http://ruby-journal.com/how-to-write-rails-view-test-with-rspec/ from 2013

# describe 'resto_resid_line/index.html.erb' do
#   it 'displays people and their associated restidences and restaurants  correctly' do
#     assign(:resto_resid_line, RestoResidLines.create(person: 'Victorin Dol', title_resto: "Proprietor"))
#
#     render
#
#     rendered.should contain('Victorin Dol')
#     rendered.should contain('Proprietor')
#   end
# end

# spec/views/products/show.html.erb_spec.rb

# describe 'products/show.html.erb' do
#   it 'displays product details correctly' do
#     assign(:product, Product.create(name: 'Shirt', price: 50.0))
#
#     render
#
#     rendered.should contain('Shirt')
#     rendered.should contain('50.0')
#   end
# end

# describe 'views/locations/index.html.erb' do
#   it 'displays location details correctly' do
#     assign(:location, Location.create(address: '10 Main St', city: 'Los Angeles')) # uninitialized constant Location
#     # <td><%= location.address %></td>
#   #     <td><%= location.city %></td>
#   #     <td><%= location.state %></td>
#
#
#     render
#     rendered.should have_selector("table#address_#{@address.id} tbody tr:nth-of-type(1) td:nth-of-type(1)", text: '10 Main St')
#     rendered.should have_selector("table#address_#{@address.id} tbody tr:nth-of-type(1) td:nth-of-type(2)", text: 'Los Angeles')
#
#     # rendered.should contain('10 Main St')
#    #  rendered.should contain('Los Angeles')
#   end
# end
