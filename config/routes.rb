Rails.application.routes.draw do
  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html
  root 'static_pages#home'

  # https://stackoverflow.com/questions/4479233/static-pages-in-ruby-on-rails
  controller :static_pages do
    get :admin
    get :about
    get :contact
    get :help
    get :acknowledgements
    get :streets
    get :sanborn
    get :history
    get :story_intro
    get :illich # Jerry Illich
    get :marietich # Jack Marietich
    get :postira # Postira, Brač Picnics
    get :images
    get :tojson
    get :trials # for development and testing at Heroku. Can delete if no longer used. Delete link in header also
  end

  # get 'otherimages', to: 'static_pages#images' # was images until created. Removed this link

  # get    'stories/index' # why
  get 'stories', to: 'stories#index'
  get 'signup', to: 'users#new'
  get 'login', to: 'sessions#new'
  post 'login', to: 'sessions#create'
  delete 'logout', to: 'sessions#destroy'

  # get   "one_line", to: 'resto_resid_lines#one_line' #, controller: 'resto_resid_lines'
  resources :docs

  resources :all_locations, only: %i[index]

  resources :locations do
    resources :docs # added with polymorphic
    # resources :years # just to see if this would help on allLocations.html.erb. Seems to work without it 2021.07.02
    # https://boringrails.com/articles/hovercards-stimulus/ next three lines. At present, not working
    # member do
    #   get :hovercard
    # end
  end
  get 'addresses', to: 'locations#index'

  # For map in people/show.
  # https://stackoverflow.com/questions/60610788/rails-connecting-to-jbuilder
  resources :people do
    # all default routes are included with this
    resources :locations, only: %i[index], module: :people
    resources :lines, only: %i[index], module: :people
  end

  # https://gorails.com/forum/how-do-i-create-a-delete-button-for-images-uploaded-with-active-storage
  # resources :years # Tried this without all the below commented out and still double GET
  resources :years do
    member do
      delete :delete_image_attachment # I think this is for images added before I started using docs for images
    end

    # Ransack
    collection do
      match 'search', to: 'years#search', via: %i[get post], as: :search
    end
  end
  get 'connections', to: 'years#index'
  get 'summary', to: 'years#summary'
  get 'line', to: 'years#line'
  get 'documents', to: 'years#documents' # Still needed for legacy AWS files

  # get 'search', to: 'years#search' # FIXME Doesn't seem like a good idea to use a general search term for just years. Does it work without this? I think it's in Ransack now

  resources :sources do
    member { delete :delete_image_attachment }
  end
  get 'sources_brief', to: 'sources#index_brief'

  # All links for Historical Streets Los Angeles. FIXME. Can't figure out how to use these with link_to
  direct :historicalla do
    'https://stark-cove-20051.herokuapp.com/'
  end
  direct :historicalla_addresses do
    'https://stark-cove-20051.herokuapp.com/street_list/' # street_list implemented in historicLA since streets goes to crores/streets which is different
  end

  resources :users
  resources :sessions # for logging in. p337
  resources :account_activations, only: %i[edit]
  resources :password_resets, only: %i[new create edit update]
  resources :resto_resid_lines
  resources :lines, controller: 'resto_resid_lines' # alternate /url
  resources :images # do I need this anymore?

  # From Clark, but too crude. See below for what I need to do
  # resources :map
  get 'map', to: 'map#index' # leaftlet version of map

  # The following are for OpenLayers. Some are development
  get 'olMap', to: 'map#olMap' # ol version of map
  get 'openlayers', to: 'map#ol'
  get 'ol', to: 'map#ol'
  get 'olls', to: 'map#ol_layer_switcher'
  get 'layerswitcher', to: 'map#ol_layer_switcher'
  get 'geojson_features', to: 'map#geojson_features'
  get 'geojsonFromFile', to: 'map#geojsonFromFile'
  get 'leafletSelfContained', to: 'map#leaflet_self_contained'
  get 'leafletSelfContainedMapDiv2', to: 'map#leafletSelfContainedMapDiv2'
  get 'allLocations', to: 'locations#allLocations' # had the html.erb in /map but /location seems more logical

  # jBuilder needs the following. FIXME need to see if all of these are still being used
  get 'map/map_data', defaults: { format: 'json' } # using point_data and line_data now. NO, still needed for something.
  get 'map/dot_data', defaults: { format: 'json' }
  get 'map/line_data', defaults: { format: 'json' }
  get 'map/line_point_data', defaults: { format: 'json' } # combining lines and points
  get 'map/point_data', defaults: { format: 'json' }
  get 'locations/allLocations', defaults: { format: 'json' }
end
