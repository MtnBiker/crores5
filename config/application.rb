require_relative 'boot'
ENV['RANSACK_FORM_BUILDER'] = '::SimpleForm::FormBuilder' # Use both SimpleForm and Ransack form builders. Does this have to be above the following line?

# require 'rails/all' # Seems heavy handed.
require "rails"
# Pick the frameworks you want:
require "active_model/railtie"
# require "active_job/railtie"
require "active_record/railtie"
require "active_storage/engine"
require "action_controller/railtie"
require "action_mailer/railtie"
# require "action_mailbox/engine"
require "action_text/engine"
require "action_view/railtie"
# require "action_cable/engine"
require "sprockets/railtie"
require "rails/test_unit/railtie"

# Require the gems listed in Gemfile, including any gems
# you've limited to :test, :development, or :production.
Bundler.require(*Rails.groups)

module Crores5
  class Application < Rails::Application
    # Initialize configuration defaults for originally generated Rails version.
    config.load_defaults 6.0

    config.active_record.schema_format = :sql # per https://edgeguides.rubyonrails.org/active_record_migrations.html#types-of-schema-dumps to solve some problems
    config.active_record.schema_format = :sql # per https://edgeguides.rubyonrails.org/active_record_migrations.html#types-of-schema-dumps to solve some problems

    # Settings in config/environments/* take precedence over those specified here.
    # Application configuration can go into files in config/initializers
    # -- all .rb files in that directory are automatically loaded after loading
    # the framework and any gems in your application.
    
    config.time_zone = 'Pacific Time (US & Canada)' # from Rails 5.0 version

    # Sumner p52. Causes some adding some files with scaffold generator.
    # May not matter much now since much of the app is build
    config.generators do |g|
      g.test_framework :rspec,
                       fixtures: false,
                       view_specs: false,
                       helper_specs: false,
                       request_specs: false # ,
      # routing_specs: false
    end

    # remove Turbo from Asset Pipeline precompilation for legacy apps.
    # Then have to add manually as needed https://dev.to/nejremeslnici/using-hotwire-turbo-in-rails-with-legacy-javascript-17g1
     config.after_initialize do
       config.assets.precompile.delete("turbo")
     end
  end
end
