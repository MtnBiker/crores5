#  http://stackoverflow.com/questions/22900904/setting-up-leaflet-with-ruby-on-rails

# Got this from gem leaflet-rails which I have deinstalled figuring I need to control leaflet version by declaring on the individual pages.

# Leaflet.tile_layer = 'https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png'

#  The two lines below used to be available. May need to turn back on
# Leaflet.attribution = '&copy; <a href="https://openstreetmap.org">OpenStreetMap</a> contributors, <a href="https://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>'
# Leaflet.max_zoom = 18

# Leaflet.tile_layer = "http://{s}.tile.osm.org/{z}/{x}/{y}.png"
# Leaflet.attribution = '&copy; <a href="https://openstreetmap.org/copyright">OpenStreetMap</a> contributors'
# Leaflet.max_zoom = 18
