# 2019.09.19 Added per https://devcenter.heroku.com/articles/direct-to-s3-image-uploads-in-rails
Aws.config.update({
                    region: 'us-west-1',
                    credentials: Aws::Credentials.new(ENV['AWS_ACCESS_KEY_ID'],
                                                      ENV['AWS_SECRET_ACCESS_KEY'])
                  })

#  This is still needed.
S3_BUCKET = Aws::S3::Resource.new.bucket(ENV['S3_BUCKET'])
