# CarrierWave.configure do |config|
#   require 'carrierwave/orm/activerecord' # https://code.tutsplus.com/tutorials/rails-image-upload-using-carrierwave-and-devise--cms-25681
#   # From https://github.com/carrierwaveuploader/carrierwave/wiki/How-to%3A-Make-Carrierwave-work-on-Heroku
#   # But I also want this to work on development
#   config.root = Rails.root.join('tmp') # adding these...
#   config.cache_dir = 'carrierwave' # ...two lines for use on Heroku
#   # config.cache_dir = "#{Rails.root}/tmp/uploads"   # To let CarrierWave work on heroku, but has the adverse side effect of making uploads not work across form redisplays.
#
#   config.fog_provider = 'fog/aws'                                # required
#   config.fog_credentials = {
#         provider:              'AWS',                            # required
#         aws_access_key_id:     (ENV['AWS_ACCESS_KEY_ID']),       # required
#         aws_secret_access_key: (ENV['AWS_SECRET_ACCESS_KEY']),   # required
#         region:                (ENV['AWS_REGION']),              # optional, defaults to 'us-east-1'
#         # host:                  's3.example.com',               # optional, defaults to nil
#         endpoint:              (ENV['AWS_ENDPOINT']) # optional, defaults to nil. Adding this got rid  of a warning about aws
#   }
#
#   # NOTE that the variables are stored in .env in development
#
#   config.fog_directory    = ENV['S3_BUCKET_NAME']                        # another source
#
#  # For testing, upload files to local `tmp` folder.
#   if Rails.env.test? || Rails.env.cucumber?
#     config.storage = :file
#     config.enable_processing = false
#     config.root = "#{Rails.root}/tmp"
#   else
#     config.storage = :fog
#   end
#
#   # config.fog_public     = false                                        # optional, defaults to true
#   config.fog_attributes = { 'Cache-Control' => "max-age=#{365.day.to_i}" } # optional, defaults to {}
#
# end
