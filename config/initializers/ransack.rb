# For arrows in sort
Ransack.configure do |c|
  c.custom_arrows = {
    up_arrow: '<i class="custom-up-arrow-icon"></i>',
    down_arrow: '&#8595;', # U+02193 didn't work,just showed as U+02193, but &#8595; shows up as a down arrow
    default_arrow: '<i class="default-arrow-icon"></i>'
  }
end
