# e.g. in config/initializers/better_errors.rb
# Other preset values are [:mvim, :macvim, :textmate, :txmt, :tm, :sublime, :subl, :st]
# Causing problems at Heroku. This should be development only
# https://github.com/charliesome/better_errors
# BetterErrors.editor = :textmate
