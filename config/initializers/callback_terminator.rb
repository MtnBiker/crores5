# TODO: delete this file. It's something before rails 5
# ActiveSupport.halt_callback_chains_on_return_false = true

# Got error starting server in Rails 5.2 and in Rails 6, but this is about callbacks which is where I seem to be having a problem
# undefined method `halt_callback_chains_on_return_false=' for ActiveSupport:Module
# https://blog.bigbinary.com/2016/02/13/rails-5-does-not-halt-callback-chain-when-false-is-returned.html
# Hartl Rails 6 app does not have this file nor this command

# Starting from Rails 5.0 if any before_ callback returns false then callback chain is not halted.
#  https://stackoverflow.com/questions/49744200/nomethoderror-undefined-method-halt-callback-chains-on-return-false-for-acti
# So you need to check all before_callbacks in the app for proper behaviour, change them if needed and remove this line from initializer after it. I don't have any explicit before_callbacks
# jQuery has some callbacks, not sure if that's anything to do with the problem
