// config/webpack/environment.js. Only first and last lines in default
const { environment } = require('@rails/webpacker')
const webpack = require('webpack')
const erb = require('./loaders/erb')

// GoRails and but I had to put most of this in application.js to define $ and jQuery.  
// Needed here to not get web page loading errors; similar statements  in application.js don't work
// The following adds jQuery **globally** for crores http://blog.blackninjadojo.com/ruby/rails/2019/03/01/webpack-webpacker-and-modules-oh-my-how-to-add-javascript-to-ruby-on-rails.html

// Add an additional plugin of your choosing : ProvidePlugin
// The process for adding or modifying webpack plugins
// https://github.com/rails/webpacker/blob/master/docs/webpack.md#plugins

// Commented out because trying with jQuery and ol via CDN so try to get map3 working.
// The ProvidePlugin makes a package available as a variable in every module compiled through webpack. If webpack sees that variable used, it will include the given package in the final bundle. But still not global. That's done in application.js. https://rubyyagi.com/how-to-use-bootstrap-and-jquery-in-rails-6-with-webpacker/
environment.plugins.append('Provide',
  new webpack.ProvidePlugin({
    $: 'jquery',
    jQuery: 'jquery',
    jquery: 'jquery',
    // 'window.Jquery': 'jquery', // https://www.lugolabs.com/articles/using-webpacker-in-ruby-on-rails-applications
    // Popper: ['popper.js' ,'default'], // bootstrap 4
    Popper: ['@popperjs/core', 'default'] // bootstrap 5
    // L: 'leaflet' // this is somewhat equivalent to import 'leaflet' but generated conflicts with leaflet.timeline.
    // ol: 'ol' // Trying again in application.js where it belongs. Old: I think this works better than import 'ol' in application.js. Still page doesn't load. cdn is an option. But layer_switcher_demo only works with this commented out and cdn version. Trying to fix ol-ext.js:29 Uncaught TypeError: Cannot set property 'inherits' of undefined  at Object.<anonymous> (ol-ext.js:29). Then get another error. 2021.02.21 `Uncaught ReferenceError: ol is not defined` with commented out. Still got error with it in application.js
  }))

environment.loaders.prepend('erb', erb) // goes with line 3 no doubt
module.exports = environment
