# Earlier
# %w[
#   .ruby-version
#   .rbenv-vars
#   tmp/restart.txt
#   tmp/caching-dev.txt
# ].each { |path| Spring.watch(path) }

# Rails 6.1 instead
Spring.watch(
  ".ruby-version",
  ".rbenv-vars",
  "tmp/restart.txt",
  "tmp/caching-dev.txt"
)
