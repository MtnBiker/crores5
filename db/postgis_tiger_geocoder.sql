﻿CREATE EXTENSION IF NOT EXISTS postgis;
CREATE EXTENSION fuzzystrmatch;
CREATE EXTENSION postgis_tiger_geocoder;