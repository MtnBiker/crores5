SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: tiger; Type: SCHEMA; Schema: -; Owner: -
--

CREATE SCHEMA tiger;


--
-- Name: tiger_data; Type: SCHEMA; Schema: -; Owner: -
--

CREATE SCHEMA tiger_data;


--
-- Name: fuzzystrmatch; Type: EXTENSION; Schema: -; Owner: -
--

CREATE EXTENSION IF NOT EXISTS fuzzystrmatch WITH SCHEMA public;


--
-- Name: EXTENSION fuzzystrmatch; Type: COMMENT; Schema: -; Owner: -
--

COMMENT ON EXTENSION fuzzystrmatch IS 'determine similarities and distance between strings';


--
-- Name: postgis; Type: EXTENSION; Schema: -; Owner: -
--

CREATE EXTENSION IF NOT EXISTS postgis WITH SCHEMA public;


--
-- Name: EXTENSION postgis; Type: COMMENT; Schema: -; Owner: -
--

COMMENT ON EXTENSION postgis IS 'PostGIS geometry, geography, and raster spatial types and functions';


--
-- Name: postgis_tiger_geocoder; Type: EXTENSION; Schema: -; Owner: -
--

CREATE EXTENSION IF NOT EXISTS postgis_tiger_geocoder WITH SCHEMA tiger;


--
-- Name: EXTENSION postgis_tiger_geocoder; Type: COMMENT; Schema: -; Owner: -
--

COMMENT ON EXTENSION postgis_tiger_geocoder IS 'PostGIS tiger geocoder and reverse geocoder';


--
-- Name: fn_create_view_resto_resid_lines(); Type: FUNCTION; Schema: public; Owner: -
--

CREATE FUNCTION public.fn_create_view_resto_resid_lines() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
  BEGIN
  CREATE OR REPLACE VIEW public.view_resto_resid_lines AS
  SELECT 
     resto.person_id,
     p.given_name AS first_name,
     p.last_name,
     (rtrim(p.given_name::text) || ' '::text) || rtrim(p.last_name::text) AS full_name,
     resto.year_date AS resto_date,
     resid.year_date AS resid_date,
     resid.year_date + ((resto.year_date - resid.year_date)/2) AS mid_date,  -- can't add dates, but can substract and end up with days (interval) which can be added to a date
     resto.resto_name,
     resto_loc.address AS resto_address,
     resto.title AS title_resto,
     resid_loc.address AS resid_address,
     resid.title AS title_resid,
     resto_loc.longitude AS long_resto,
     resto_loc.latitude AS lat_resto,
     resid_loc.longitude AS long_resid,
     resid_loc.latitude AS lat_resid,
     rtrim(resto.notes) AS resto_notes,
     rtrim(resid.notes) AS resid_notes,
     p.date_of_birth,
     resto.id AS resto_connection_id,
     resid.id AS resid_connection_id,
     resto.location_id AS resto_loc_id,
     resid.location_id AS resid_loc_id
    FROM years resto
      JOIN years resid ON resto.year_date >= (resid.year_date - 364) AND resto.year_date <= (resid.year_date + 364)
      JOIN people p ON resto.person_id = p.id
      JOIN locations resto_loc ON resto.location_id = resto_loc.id
      JOIN locations resid_loc ON resid.location_id = resid_loc.id
   WHERE resto.person_id = resid.person_id AND resto.resto = true AND resto.resto <> resid.resto AND resto.resid <> resid.resid AND resto.resto = true AND resid.resto = false
   ORDER BY resto.year_date, p.last_name, p.given_name;
  RETURN NEW; -- I don't need anything returned, but there needs to be a RETURNS in the first line. Error without though
  END;
  $$;


SET default_tablespace = '';

SET default_table_access_method = heap;

--
-- Name: action_text_rich_texts; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.action_text_rich_texts (
    id bigint NOT NULL,
    name character varying NOT NULL,
    body text,
    record_type character varying NOT NULL,
    record_id bigint NOT NULL,
    created_at timestamp(6) without time zone NOT NULL,
    updated_at timestamp(6) without time zone NOT NULL
);


--
-- Name: action_text_rich_texts_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.action_text_rich_texts_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: action_text_rich_texts_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.action_text_rich_texts_id_seq OWNED BY public.action_text_rich_texts.id;


--
-- Name: active_storage_attachments; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.active_storage_attachments (
    id bigint NOT NULL,
    name character varying NOT NULL,
    record_type character varying NOT NULL,
    record_id bigint NOT NULL,
    blob_id bigint NOT NULL,
    created_at timestamp without time zone NOT NULL
);


--
-- Name: active_storage_attachments_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.active_storage_attachments_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: active_storage_attachments_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.active_storage_attachments_id_seq OWNED BY public.active_storage_attachments.id;


--
-- Name: active_storage_blobs; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.active_storage_blobs (
    id bigint NOT NULL,
    key character varying NOT NULL,
    filename character varying NOT NULL,
    content_type character varying,
    metadata text,
    byte_size bigint NOT NULL,
    checksum character varying NOT NULL,
    created_at timestamp without time zone NOT NULL
);


--
-- Name: active_storage_blobs_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.active_storage_blobs_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: active_storage_blobs_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.active_storage_blobs_id_seq OWNED BY public.active_storage_blobs.id;


--
-- Name: ar_internal_metadata; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.ar_internal_metadata (
    key character varying NOT NULL,
    value character varying,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL
);


--
-- Name: docs; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.docs (
    id bigint NOT NULL,
    source_id integer,
    page_no character varying,
    original_url character varying,
    basename character varying,
    created_at timestamp(6) without time zone NOT NULL,
    updated_at timestamp(6) without time zone NOT NULL,
    notes character varying,
    snippet_coords json,
    content_plain_text text
);


--
-- Name: docs_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.docs_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: docs_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.docs_id_seq OWNED BY public.docs.id;


--
-- Name: docs_locations; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.docs_locations (
    doc_id bigint NOT NULL,
    location_id bigint NOT NULL
);


--
-- Name: docs_people; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.docs_people (
    doc_id bigint NOT NULL,
    person_id bigint NOT NULL
);


--
-- Name: docs_years; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.docs_years (
    doc_id bigint NOT NULL,
    year_id bigint NOT NULL
);


--
-- Name: lines; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.lines (
    id integer NOT NULL,
    person_id integer,
    resid_loc_id integer,
    resto_loc_id integer,
    resto_date date,
    resid_date date,
    title_resto character varying,
    title_resid character varying,
    resto_name character varying,
    resto_connection_id integer,
    resid_connection_id integer,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL
);


--
-- Name: lines_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.lines_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: lines_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.lines_id_seq OWNED BY public.lines.id;


--
-- Name: locations; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.locations (
    id integer NOT NULL,
    address character varying,
    city character varying DEFAULT 'Los Angeles'::character varying,
    state character varying DEFAULT 'CA'::character varying,
    longitude numeric(9,6),
    latitude numeric(9,6),
    extant boolean,
    current_description character varying,
    notes character varying,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone,
    geom public.geometry(Point,4326),
    file_basename text,
    geocoded_with text,
    coords_not_locked boolean DEFAULT true,
    ref_link character varying,
    caption character varying,
    street_view_note character varying,
    street_view_url character varying,
    doc_id integer,
    snippet_coords json,
    pre1890 boolean DEFAULT false,
    content_plain_text text
);


--
-- Name: locations_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.locations_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: locations_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.locations_id_seq OWNED BY public.locations.id;


--
-- Name: people; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.people (
    id integer NOT NULL,
    date_of_birth date,
    date_of_entry date,
    doe_source text,
    date_of_citizenship date,
    notes text,
    last_name character varying,
    given_name character varying,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL,
    file_basename text,
    date_of_death date,
    place_of_birth character varying,
    caption character varying,
    croatian boolean,
    from_where character varying,
    from_where_url character varying,
    doc_id integer,
    snippet_coords json,
    resto boolean DEFAULT true,
    content_plain_text text
);


--
-- Name: people_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.people_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: people_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.people_id_seq OWNED BY public.people.id;


--
-- Name: positions; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.positions (
    id integer NOT NULL,
    title character varying
);


--
-- Name: positions_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.positions_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: positions_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.positions_id_seq OWNED BY public.positions.id;


--
-- Name: resto_resid_lines; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.resto_resid_lines (
    id bigint NOT NULL,
    mid_date date,
    person_id integer,
    resto_connection_id integer,
    resid_connection_id integer,
    resto_loc_id integer,
    resto_name character varying,
    resto_address character varying,
    title_resto character varying,
    lat_resto numeric(9,6),
    long_resto numeric(9,6),
    resto_date date,
    resid_loc_id integer,
    resid_address character varying,
    title_resid character varying,
    lat_resid numeric(9,6),
    long_resid numeric(9,6),
    resid_date date,
    created_at timestamp(6) without time zone NOT NULL,
    updated_at timestamp(6) without time zone NOT NULL
);


--
-- Name: resto_resid_lines_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.resto_resid_lines_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: resto_resid_lines_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.resto_resid_lines_id_seq OWNED BY public.resto_resid_lines.id;


--
-- Name: schema_migrations; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.schema_migrations (
    version character varying NOT NULL
);


--
-- Name: sources; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.sources (
    id bigint NOT NULL,
    cover_year character varying,
    name character varying,
    publisher character varying,
    file_basename character varying,
    from_where character varying,
    from_where_url character varying,
    effective_date date,
    notes text,
    created_at timestamp(6) without time zone NOT NULL,
    updated_at timestamp(6) without time zone NOT NULL,
    caption text
);


--
-- Name: sources_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.sources_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: sources_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.sources_id_seq OWNED BY public.sources.id;


--
-- Name: users; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.users (
    id integer NOT NULL,
    name character varying,
    email character varying,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL,
    password_digest character varying,
    remember_digest character varying,
    admin boolean DEFAULT false,
    activation_digest character varying,
    activated boolean DEFAULT false,
    activated_at timestamp without time zone,
    reset_digest character varying,
    reset_sent_at timestamp without time zone
);


--
-- Name: users_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.users_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: years; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.years (
    id integer NOT NULL,
    year_date date,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL,
    resto boolean,
    resid boolean,
    file_basename text,
    person_id integer,
    location_id integer,
    title character varying,
    notes text,
    resto_name character varying,
    aws_url character varying,
    from_where_url character varying,
    caption character varying,
    resto_event boolean DEFAULT true,
    from_where character varying,
    doc_id integer,
    snippet_coords json,
    name_shown character varying,
    content_plain_text text
);


--
-- Name: view_resto_resid_lines; Type: VIEW; Schema: public; Owner: -
--

CREATE VIEW public.view_resto_resid_lines AS
 SELECT resto.person_id,
    p.given_name AS first_name,
    p.last_name,
    ((rtrim((p.given_name)::text) || ' '::text) || rtrim((p.last_name)::text)) AS full_name,
    resto.year_date AS resto_date,
    resid.year_date AS resid_date,
    (resid.year_date + ((resto.year_date - resid.year_date) / 2)) AS mid_date,
    resto.resto_name,
    resto_loc.address AS resto_address,
    resto.title AS title_resto,
    resid_loc.address AS resid_address,
    resid.title AS title_resid,
    resto_loc.longitude AS long_resto,
    resto_loc.latitude AS lat_resto,
    resid_loc.longitude AS long_resid,
    resid_loc.latitude AS lat_resid,
    rtrim(resto.notes) AS resto_notes,
    rtrim(resid.notes) AS resid_notes,
    p.date_of_birth,
    resto.id AS resto_connection_id,
    resid.id AS resid_connection_id,
    resto.location_id AS resto_loc_id,
    resid.location_id AS resid_loc_id
   FROM ((((public.years resto
     JOIN public.years resid ON (((resto.year_date >= (resid.year_date - 364)) AND (resto.year_date <= (resid.year_date + 364)))))
     JOIN public.people p ON ((resto.person_id = p.id)))
     JOIN public.locations resto_loc ON ((resto.location_id = resto_loc.id)))
     JOIN public.locations resid_loc ON ((resid.location_id = resid_loc.id)))
  WHERE ((resto.person_id = resid.person_id) AND (resto.resto = true) AND (resto.resto <> resid.resto) AND (resto.resid <> resid.resid) AND (resto.resto = true) AND (resid.resto = false))
  ORDER BY resto.year_date, p.last_name, p.given_name;


--
-- Name: years_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.years_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: years_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.years_id_seq OWNED BY public.years.id;


--
-- Name: action_text_rich_texts id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.action_text_rich_texts ALTER COLUMN id SET DEFAULT nextval('public.action_text_rich_texts_id_seq'::regclass);


--
-- Name: active_storage_attachments id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.active_storage_attachments ALTER COLUMN id SET DEFAULT nextval('public.active_storage_attachments_id_seq'::regclass);


--
-- Name: active_storage_blobs id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.active_storage_blobs ALTER COLUMN id SET DEFAULT nextval('public.active_storage_blobs_id_seq'::regclass);


--
-- Name: docs id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.docs ALTER COLUMN id SET DEFAULT nextval('public.docs_id_seq'::regclass);


--
-- Name: lines id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.lines ALTER COLUMN id SET DEFAULT nextval('public.lines_id_seq'::regclass);


--
-- Name: locations id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.locations ALTER COLUMN id SET DEFAULT nextval('public.locations_id_seq'::regclass);


--
-- Name: people id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.people ALTER COLUMN id SET DEFAULT nextval('public.people_id_seq'::regclass);


--
-- Name: positions id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.positions ALTER COLUMN id SET DEFAULT nextval('public.positions_id_seq'::regclass);


--
-- Name: resto_resid_lines id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.resto_resid_lines ALTER COLUMN id SET DEFAULT nextval('public.resto_resid_lines_id_seq'::regclass);


--
-- Name: sources id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.sources ALTER COLUMN id SET DEFAULT nextval('public.sources_id_seq'::regclass);


--
-- Name: years id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.years ALTER COLUMN id SET DEFAULT nextval('public.years_id_seq'::regclass);


--
-- Name: action_text_rich_texts action_text_rich_texts_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.action_text_rich_texts
    ADD CONSTRAINT action_text_rich_texts_pkey PRIMARY KEY (id);


--
-- Name: active_storage_attachments active_storage_attachments_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.active_storage_attachments
    ADD CONSTRAINT active_storage_attachments_pkey PRIMARY KEY (id);


--
-- Name: active_storage_blobs active_storage_blobs_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.active_storage_blobs
    ADD CONSTRAINT active_storage_blobs_pkey PRIMARY KEY (id);


--
-- Name: ar_internal_metadata ar_internal_metadata_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.ar_internal_metadata
    ADD CONSTRAINT ar_internal_metadata_pkey PRIMARY KEY (key);


--
-- Name: docs docs_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.docs
    ADD CONSTRAINT docs_pkey PRIMARY KEY (id);


--
-- Name: lines lines_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.lines
    ADD CONSTRAINT lines_pkey PRIMARY KEY (id);


--
-- Name: locations locations_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.locations
    ADD CONSTRAINT locations_pkey PRIMARY KEY (id);


--
-- Name: people people_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.people
    ADD CONSTRAINT people_pkey PRIMARY KEY (id);


--
-- Name: positions positions_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.positions
    ADD CONSTRAINT positions_pkey PRIMARY KEY (id);


--
-- Name: resto_resid_lines resto_resid_lines_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.resto_resid_lines
    ADD CONSTRAINT resto_resid_lines_pkey PRIMARY KEY (id);


--
-- Name: sources sources_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.sources
    ADD CONSTRAINT sources_pkey PRIMARY KEY (id);


--
-- Name: users users_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.users
    ADD CONSTRAINT users_pkey PRIMARY KEY (id);


--
-- Name: years years_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.years
    ADD CONSTRAINT years_pkey PRIMARY KEY (id);


--
-- Name: index_action_text_rich_texts_uniqueness; Type: INDEX; Schema: public; Owner: -
--

CREATE UNIQUE INDEX index_action_text_rich_texts_uniqueness ON public.action_text_rich_texts USING btree (record_type, record_id, name);


--
-- Name: index_active_storage_attachments_on_blob_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_active_storage_attachments_on_blob_id ON public.active_storage_attachments USING btree (blob_id);


--
-- Name: index_active_storage_attachments_uniqueness; Type: INDEX; Schema: public; Owner: -
--

CREATE UNIQUE INDEX index_active_storage_attachments_uniqueness ON public.active_storage_attachments USING btree (record_type, record_id, name, blob_id);


--
-- Name: index_active_storage_blobs_on_key; Type: INDEX; Schema: public; Owner: -
--

CREATE UNIQUE INDEX index_active_storage_blobs_on_key ON public.active_storage_blobs USING btree (key);


--
-- Name: index_docs_locations_on_doc_id_and_location_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_docs_locations_on_doc_id_and_location_id ON public.docs_locations USING btree (doc_id, location_id);


--
-- Name: index_docs_locations_on_location_id_and_doc_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_docs_locations_on_location_id_and_doc_id ON public.docs_locations USING btree (location_id, doc_id);


--
-- Name: index_docs_people_on_doc_id_and_person_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_docs_people_on_doc_id_and_person_id ON public.docs_people USING btree (doc_id, person_id);


--
-- Name: index_docs_people_on_person_id_and_doc_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_docs_people_on_person_id_and_doc_id ON public.docs_people USING btree (person_id, doc_id);


--
-- Name: index_docs_years_on_doc_id_and_year_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_docs_years_on_doc_id_and_year_id ON public.docs_years USING btree (doc_id, year_id);


--
-- Name: index_docs_years_on_year_id_and_doc_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_docs_years_on_year_id_and_doc_id ON public.docs_years USING btree (year_id, doc_id);


--
-- Name: index_locations_on_address; Type: INDEX; Schema: public; Owner: -
--

CREATE UNIQUE INDEX index_locations_on_address ON public.locations USING btree (address);


--
-- Name: index_years_on_location_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_years_on_location_id ON public.years USING btree (location_id);


--
-- Name: index_years_on_person_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_years_on_person_id ON public.years USING btree (person_id);


--
-- Name: unique_schema_migrations; Type: INDEX; Schema: public; Owner: -
--

CREATE UNIQUE INDEX unique_schema_migrations ON public.schema_migrations USING btree (version);


--
-- Name: locations trg_loc_view_rrl; Type: TRIGGER; Schema: public; Owner: -
--

CREATE TRIGGER trg_loc_view_rrl AFTER DELETE OR UPDATE ON public.locations FOR EACH STATEMENT EXECUTE FUNCTION public.fn_create_view_resto_resid_lines();


--
-- Name: people trg_view_rrl; Type: TRIGGER; Schema: public; Owner: -
--

CREATE TRIGGER trg_view_rrl AFTER DELETE OR UPDATE ON public.people FOR EACH STATEMENT EXECUTE FUNCTION public.fn_create_view_resto_resid_lines();


--
-- Name: years trg_years_view_rrl; Type: TRIGGER; Schema: public; Owner: -
--

CREATE TRIGGER trg_years_view_rrl AFTER INSERT OR DELETE OR UPDATE ON public.years FOR EACH STATEMENT EXECUTE FUNCTION public.fn_create_view_resto_resid_lines();


--
-- Name: years fk_rails_8fc1813509; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.years
    ADD CONSTRAINT fk_rails_8fc1813509 FOREIGN KEY (person_id) REFERENCES public.people(id);


--
-- Name: active_storage_attachments fk_rails_c3b3935057; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.active_storage_attachments
    ADD CONSTRAINT fk_rails_c3b3935057 FOREIGN KEY (blob_id) REFERENCES public.active_storage_blobs(id);


--
-- Name: years fk_rails_e1624dbb3f; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.years
    ADD CONSTRAINT fk_rails_e1624dbb3f FOREIGN KEY (location_id) REFERENCES public.locations(id);


--
-- PostgreSQL database dump complete
--

SET search_path TO "$user", public, tiger;

INSERT INTO "schema_migrations" (version) VALUES
('20160121043338'),
('20160121052941'),
('20160121060101'),
('20160122000931'),
('20160122003702'),
('20160122211232'),
('20160122211729'),
('20160123025717'),
('20160123042452'),
('20160124050750'),
('20160125020155'),
('20160126023341'),
('20160202170523'),
('20160202225025'),
('20160202235835'),
('20160203004059'),
('20160208163855'),
('20160208193352'),
('20160209002940'),
('20160209003628'),
('20160211231206'),
('20160212021200'),
('20160212163532'),
('20160213012440'),
('20160214011601'),
('20160214014442'),
('20160220214819'),
('20160220215937'),
('20160222020237'),
('20160222160014'),
('20160312155716'),
('20160313010142'),
('20160316040429'),
('20161004200000'),
('20161007010425'),
('20161013201609'),
('20161013205335'),
('20161014005219'),
('20161014152419'),
('20161022213941'),
('20161029201219'),
('20161031003319'),
('20161102225811'),
('20161107171911'),
('20161126003953'),
('20161127000032'),
('20161127014004'),
('20161213051947'),
('20170315045923'),
('20170315052205'),
('20170325170952'),
('20190515194941'),
('20190515215800'),
('20190516200725'),
('20190617214357'),
('20190617215446'),
('20190620035032'),
('20190620041647'),
('20190915024943'),
('20190917172340'),
('20190921181517'),
('20190928212704'),
('20190928215306'),
('20190928230940'),
('20190928231715'),
('20191001185219'),
('20191205060709'),
('20191205181226'),
('20200302200532'),
('20200302201356'),
('20200302215143'),
('20200302221833'),
('20200302225343'),
('20200319172703'),
('20200319183614'),
('20200319192708'),
('20200320171711'),
('20200320174746'),
('20200320211643'),
('20200320212414'),
('20200320212647'),
('20200320214540'),
('20200320222828'),
('20200320230112'),
('20200320230616'),
('20200321000146'),
('20200321020340'),
('20200322044332'),
('20200322164504'),
('20200322165602'),
('20200322230523'),
('20200323135728'),
('20200323140200'),
('20200323143123'),
('20200324013630'),
('20200423182114'),
('20200423183354'),
('20200425234448'),
('20200428191811'),
('20200428192442'),
('20200428192632'),
('20200428192704'),
('20200428192859'),
('20200428193129'),
('20200501041356'),
('20201030205617'),
('20201211005633'),
('20201217200332'),
('20201217201411'),
('20201218173138'),
('20201221202758'),
('20201221203002'),
('20210219183612'),
('20210520014246'),
('20210607163203'),
('20210607170332'),
('20210607174723'),
('20210607175910'),
('20210826042431'),
('20210826044248'),
('20210826050220'),
('20210826051613'),
('20210827010634'),
('20210827010709'),
('20210827010807');


