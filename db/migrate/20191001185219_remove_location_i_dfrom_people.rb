class RemoveLocationIDfromPeople < ActiveRecord::Migration[6.0]
  def change
    remove_column :people, :location_id
  end
end
