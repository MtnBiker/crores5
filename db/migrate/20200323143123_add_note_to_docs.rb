class AddNoteToDocs < ActiveRecord::Migration[6.0]
  def change
    add_column :docs, :notes, :string
    change_table :docs do |t| # only needed for multiple items, but no harm, no foul (see below for single item
      t.rename :source, :source_id
    end
  end
end
