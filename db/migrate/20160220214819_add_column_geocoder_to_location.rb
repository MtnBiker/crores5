class AddColumnGeocoderToLocation < ActiveRecord::Migration[5.0]
  def change
    add_column :locations, :geocode, :text
  end
end
