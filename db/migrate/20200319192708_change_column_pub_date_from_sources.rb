class ChangeColumnPubDateFromSources < ActiveRecord::Migration[6.0]
  change_table :sources do |t|
    t.rename :pub_date, :effective_date
  end
end
