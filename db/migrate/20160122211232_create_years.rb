class CreateYears < ActiveRecord::Migration[5.0]
  def change
    create_table :years do |t|
      t.date :year_date

      t.timestamps null: false
    end

    add_index :years, :year_date, unique: true
  end
end
