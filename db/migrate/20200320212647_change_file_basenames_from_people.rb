class ChangeFileBasenamesFromPeople < ActiveRecord::Migration[6.0]
  change_table :people do |t|
    t.rename :source, :file_basename
  end
end
