class DropColumnnsRestoResidLines < ActiveRecord::Migration[6.0]
  def change
    remove_column :resto_resid_lines, :i_bug
    remove_column :resto_resid_lines, :j_bug
  end
end
