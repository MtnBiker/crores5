class CreateSources < ActiveRecord::Migration[6.0]
  def change
    create_table :sources do |t|
      t.integer :year
      t.string :name
      t.string :publisher
      t.string :source
      t.string :from_where
      t.string :from_where_url
      t.string :caption
      t.date :pub_date
      t.text :notes

      t.timestamps
    end
  end
end
