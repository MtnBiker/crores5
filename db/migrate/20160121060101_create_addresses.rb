class CreateAddresses < ActiveRecord::Migration[5.0]
  def change
    create_table :addresses do |t|
      t.string :address
      t.string :city, default: 'Los Angeles'
      t.string :state, default: 'CA'
      t.decimal :longitude, precision: 9, scale: 6
      t.decimal :latitude, precision: 9, scale: 6
      t.boolean :extant
      t.string :current_description
      t.string :notes

      t.timestamps null: false
    end

    add_index :addresses, :address, unique: true
  end
end

# Couldn't add geometry
