class AddColumnNameToYears < ActiveRecord::Migration[5.0]
  def change
    add_column :years, :source_url, :string
    add_column :years, :ref_url, :string
  end
end
