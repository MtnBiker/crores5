class ChangeColumnNameInYears < ActiveRecord::Migration[5.0]
  def change
    rename_column :years, :address_id, :location_id
  end
end
