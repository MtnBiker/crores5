class ChangeColumnCoverYearFromSources < ActiveRecord::Migration[6.0]
  def change
    change_column :sources, :cover_year, :string
  end
end
