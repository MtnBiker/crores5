class UniqueFullName < ActiveRecord::Migration[5.0]
  def change
    add_index :people, [:given_name, :last_name], unique: true
  end
end
