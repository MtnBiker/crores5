class CreateImages < ActiveRecord::Migration[5.0]
  def change
    create_table :images do |t|
      t.text :description
      t.string :ref
      t.string :source_url
      t.string :url
      t.text :notes
      t.text :transcript
      t.string :file

      t.timestamps
    end
  end
end
