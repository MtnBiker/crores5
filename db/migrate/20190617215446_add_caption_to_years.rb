class AddCaptionToYears < ActiveRecord::Migration[5.2]
  def change
    add_column :years, :caption, :string
  end
end
