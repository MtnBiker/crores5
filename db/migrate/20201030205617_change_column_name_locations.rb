class ChangeColumnNameLocations < ActiveRecord::Migration[6.0]
  def change
    rename_column :locations, :from_where, :street_view_note
    rename_column :locations, :from_where_url, :street_view_url
  end
end
