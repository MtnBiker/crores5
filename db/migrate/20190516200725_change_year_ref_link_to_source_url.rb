class ChangeYearRefLinkToSourceUrl < ActiveRecord::Migration[5.2]
  def change
    # rename_column :years, :ref_link, :source_url # Sam Ruby p417, change_column is reversible
  end
end
# Commented out because got `rails db:migrate RAILS_ENV=test` error:

# UndefinedColumn: ERROR:  column "ref_link" does not exist

# Uncommented since didn't fix the problem yet
