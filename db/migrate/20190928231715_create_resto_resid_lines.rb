class CreateRestoResidLines < ActiveRecord::Migration[6.0]
  # rails g migration CreateRestoResidLines mid_date:date person_id:integer resto_connection_id:integer resid_connection_id:integer  resto_loc_id:integer resto_name:string resto_address:string title_resto:string 'lat_resto:decimal{9,6}' 'long_resto:decimal{9,6}' resto_date:date  resid_loc_id:integer resid_address:string title_resid:string 'lat_resid:decimal{9,6}' 'long_resid:decimal{9,6}' resid_date:date
  # had to add t.timestamps. NULL is not allowed by default apparently with Postgres
  def change
    create_table :resto_resid_lines do |t|
      t.date :mid_date
      t.integer :person_id
      t.integer :resto_connection_id
      t.integer :resid_connection_id
      t.integer :resto_loc_id
      t.string :resto_name
      t.string :resto_address
      t.string :title_resto
      t.decimal :lat_resto, precision: 9, scale: 6
      t.decimal :long_resto, precision: 9, scale: 6
      t.date :resto_date
      t.integer :resid_loc_id
      t.string :resid_address
      t.string :title_resid
      t.decimal :lat_resid, precision: 9, scale: 6
      t.decimal :long_resid, precision: 9, scale: 6
      t.date :resid_date

      t.timestamps
    end
  end
end
