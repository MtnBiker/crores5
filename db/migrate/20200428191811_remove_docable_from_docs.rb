class RemoveDocableFromDocs < ActiveRecord::Migration[6.0]
  def change
    remove_column :docs, :docable_type, :string
    remove_column :docs, :docable_id, :integer
  end
end
