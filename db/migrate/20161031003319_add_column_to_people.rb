class AddColumnToPeople < ActiveRecord::Migration[5.0]
  def change
    add_column :people, :place_of_birth, :string
  end
end
