class CreateTablePositions < ActiveRecord::Migration[5.0]
  def change
    create_table :positions do |t|
      t.string :title
    end
  end
end
