class AddColumnsToRestoresidlines < ActiveRecord::Migration[5.0]
  def change
    add_column :resto_resid_lines, :lat_resid,  :decimal, precision: 9, scale: 6
    add_column :resto_resid_lines, :long_resid, :decimal, precision: 9, scale: 6
    add_column :resto_resid_lines, :lat_resto,  :decimal, precision: 9, scale: 6
    add_column :resto_resid_lines, :long_resto, :decimal, precision: 9, scale: 6
  end
end
