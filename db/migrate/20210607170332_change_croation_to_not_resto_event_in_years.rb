class ChangeCroationToNotRestoEventInYears < ActiveRecord::Migration[6.0]
  change_table :years do |t|
    t.rename :croatian, :not_resto_event
  end
end

# See next migration where this got changed
