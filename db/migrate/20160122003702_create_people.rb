class CreatePeople < ActiveRecord::Migration[5.0]
  def change
    create_table :people do |t|
      t.string :name # trying to squelch an error and I've removed :name in lots of places TODO
      t.date :date_of_birth
      t.date :date_of_entry
      t.text :doe_source
      t.date :date_of_citizenship
      t.text :notes
      t.string :last_name
      t.string :given_name

      t.timestamps null: false
    end

    add_index :people, :name # TODO, what is needed here?
  end
end
