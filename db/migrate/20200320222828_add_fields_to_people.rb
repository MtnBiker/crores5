class AddFieldsToPeople < ActiveRecord::Migration[6.0]
  def change
    add_column :people, :from_where, :string
    add_column :people, :from_where_url, :string
  end
end
