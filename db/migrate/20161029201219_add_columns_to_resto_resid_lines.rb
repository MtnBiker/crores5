class AddColumnsToRestoResidLines < ActiveRecord::Migration[5.0]
  def change
    add_column :resto_resid_lines, :resto_address, :string
    add_column :resto_resid_lines, :resid_address, :string
  end
end
