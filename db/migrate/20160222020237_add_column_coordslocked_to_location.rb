class AddColumnCoordslockedToLocation < ActiveRecord::Migration[5.0]
  def change
    add_column :locations, :coords_locked, :boolean, default: true
  end
end
