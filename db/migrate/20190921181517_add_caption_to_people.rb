class AddCaptionToPeople < ActiveRecord::Migration[6.0]
  def change
    add_column :people,    :caption, :string
    add_column :locations, :caption, :string
  end
end
