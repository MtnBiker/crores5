class RemoveCroatianFromLocations < ActiveRecord::Migration[6.0]
  def change
    remove_column :locations, :croatian, :boolean
  end
end
