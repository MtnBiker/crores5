class AddDefaultValueToShowAttribute < ActiveRecord::Migration[6.0]
  def up
    change_column :years, :resto_event, :boolean, default: true
  end

  def down
    change_column :years, :resto_event, :boolean, default: nil
  end
end

# This is picked up for years/new
