class ChangeColumnNameCoordslockedInLocation < ActiveRecord::Migration[5.0]
  def change
    rename_column :locations, :coords_locked, :coords_not_locked
  end
end
