# May not be needed. I think the problem was elsewhere TODO
class Lines < ActiveRecord::Migration[5.0]
  def change
    create_table :lines do |t|
      t.integer :person_id
      t.integer :resid_loc_id
      t.integer :resto_loc_id
      t.date    :resto_date
      t.date    :resid_date
      t.string  :title_resto
      t.string  :title_resid
      t.string  :resto_name
      t.integer :resto_connection_id
      t.integer :resid_connection_id

      t.timestamps null: false
    end
  end
end
