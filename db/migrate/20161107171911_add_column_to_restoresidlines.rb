class AddColumnToRestoresidlines < ActiveRecord::Migration[5.0]
  def change
    add_column :resto_resid_lines, :mid_date, :date # Can't be :RestoResidLines
  end
end
