class AddSnippetExtentToYears < ActiveRecord::Migration[6.0]
  def change
    add_column :years, :snippet_extent, :text, array: true
  end
end
