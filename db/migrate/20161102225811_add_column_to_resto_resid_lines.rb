class AddColumnToRestoResidLines < ActiveRecord::Migration[5.0]
  def change
    add_column :resto_resid_lines, :i_bug, :integer
    add_column :resto_resid_lines, :j_bug, :integer
  end
end
