class AddPre1890toLocation < ActiveRecord::Migration[6.0]
  def change
    add_column :locations, :pre1890, :boolean, default: false
  end
end
