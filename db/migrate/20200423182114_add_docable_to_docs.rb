class AddDocableToDocs < ActiveRecord::Migration[6.0]
  def change
    add_column :docs, :docable_id, :integer
    add_column :docs, :docable_type, :string
  end

  # add_index :docs, [:docable_type, :docable_id] # I think I commented this out because it didn't think these columns existed. Then ran in next migration
end
