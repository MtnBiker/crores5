class AddContentPlainTextToDocs3 < ActiveRecord::Migration[6.0]
  def change
    add_column :docs, :content_plain_text, :text
  end
end

# Trying with postgis off