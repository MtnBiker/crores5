class ChangeRefUrlFromYears < ActiveRecord::Migration[6.0]
  change_table :years do |t|
    t.rename :ref_url, :from_where_url
  end
end
