class AddPersonRefToYear < ActiveRecord::Migration[5.0]
  def change
    add_reference :years,  :person,  index: true, foreign_key: true
    add_reference :years,  :address, index: true, foreign_key: true
  end
end

#  I changed this by accident later, but think I changed it back correctly. Note address later changed to lcoation
