class AddColumnsToRestoResidLine < ActiveRecord::Migration[5.0]
  def change
    add_column :resto_resid_lines, :resto_connection_id, :integer
    add_column :resto_resid_lines, :resid_connection_id, :integer
  end
end
