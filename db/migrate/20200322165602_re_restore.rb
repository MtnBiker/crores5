class ReRestore < ActiveRecord::Migration[6.0]
  def change
    # 20200321020340_change_source_url_from_years
    change_table :years do |t|
      t.rename :source_url, :aws_url
      t.rename :ref_url, :from_where_url
      t.rename :source, :file_basename
    end

    # 20200321000146_add_fields_to_locations
    # add_column :locations, :from_where, :string
    # add_column :locations, :from_where_url, :string

    # 20200320230616_add_fields_to_years
    # add_column :years, :from_where, :string

    # # 20200320230112_change_ref_url_from_years
    #  change_table :years do |t|
    #
    # end

    # 20200320222828_add_fields_to_people
    # add_column :people, :from_where, :string
    # add_column :people, :from_where_url, :string

    # # 20200320214540_change_file_basenames_from_years
    # change_table :years do |t|
    # end

    # 20200320212647_change_file_basenames_from_people
    change_table :people do |t|
      t.rename :source, :file_basename
    end

    # 20200320212414_change_file_basenames_from_locations
    change_table :locations do |t|
      t.rename :source, :file_basename
    end
  end
end
