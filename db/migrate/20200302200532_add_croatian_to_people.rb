class AddCroatianToPeople < ActiveRecord::Migration[6.0]
  def change
    add_column :people, :croatian, :boolean, default: true
  end
end
