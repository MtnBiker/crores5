class ChangeFieldnameToYears < ActiveRecord::Migration[5.0]
  def change
    rename_column :years, :position, :title
  end
end
