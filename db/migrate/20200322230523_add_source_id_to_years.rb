class AddSourceIdToYears < ActiveRecord::Migration[6.0]
  def change
    add_column :years, :source_id, :integer
    add_column :locations, :source_id, :integer
    add_column :people, :source_id, :integer
  end
end
