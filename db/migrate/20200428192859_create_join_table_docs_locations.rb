class CreateJoinTableDocsLocations < ActiveRecord::Migration[6.0]
  def change
    create_join_table :docs, :locations do |t|
      t.index [:doc_id, :location_id]
      t.index [:location_id, :doc_id]
    end
  end
end
