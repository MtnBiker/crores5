class AddCroatianToYears < ActiveRecord::Migration[6.0]
  def change
    add_column :years, :croatian, :boolean, default: true
    add_column :locations, :croatian, :boolean, default: true
  end
end
