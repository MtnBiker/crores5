class CreateJoinTableDocsYears < ActiveRecord::Migration[6.0]
  def change
    create_join_table :docs, :years do |t|
      t.index [:doc_id, :year_id]
      t.index [:year_id, :doc_id]
    end
  end
end
