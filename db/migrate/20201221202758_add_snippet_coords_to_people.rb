class AddSnippetCoordsToPeople < ActiveRecord::Migration[6.0]
  def change
    add_column :people,    :snippet_coords, :json
    add_column :docs,      :snippet_coords, :json
    add_column :locations, :snippet_coords, :json
  end
end
