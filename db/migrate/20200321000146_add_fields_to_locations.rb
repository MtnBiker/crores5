class AddFieldsToLocations < ActiveRecord::Migration[6.0]
  def change
    add_column :locations, :from_where, :string
    add_column :locations, :from_where_url, :string
  end
end
