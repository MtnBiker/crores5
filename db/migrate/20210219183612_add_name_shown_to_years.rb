class AddNameShownToYears < ActiveRecord::Migration[6.0]
  def change
    add_column :years, :name_shown, :string
  end
end
