class AddRestoToPeople < ActiveRecord::Migration[6.0]
  def change
    add_column :people, :resto, :boolean, default: true
  end
end
