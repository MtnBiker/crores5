class RemoveSnippetExtentFromYears < ActiveRecord::Migration[6.0]
  def change
    remove_column :years, :snippet_extent
  end
end
