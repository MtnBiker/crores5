class AddColumnNotesToYears < ActiveRecord::Migration[5.0]
  def change
    add_column :years, :notes, :text
  end
end
