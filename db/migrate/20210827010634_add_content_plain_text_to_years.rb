class AddContentPlainTextToYears < ActiveRecord::Migration[6.0]
  def change
    add_column :years, :content_plain_text, :text
  end
end

# rails generate migration AddContentPlainTextToYears content_plain_text:text