class ChangeFileBasenamesFromYears < ActiveRecord::Migration[6.0]
  change_table :years do |t|
    t.rename :source, :file_basename
  end
end
