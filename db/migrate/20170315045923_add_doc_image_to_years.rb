class AddDocImageToYears < ActiveRecord::Migration[5.0]
  def change
    add_column :years, :doc_image, :string
  end
end
