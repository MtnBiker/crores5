class AddColumnToYears < ActiveRecord::Migration[5.0]
  def change
    add_column :years, :position, :string
  end
end
