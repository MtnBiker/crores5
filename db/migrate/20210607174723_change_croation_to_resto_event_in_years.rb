class ChangeCroationToRestoEventInYears < ActiveRecord::Migration[6.0]
  change_table :years do |t|
    t.rename :not_resto_event, :resto_event
  end
end

# Went in circles. Got original true mixed up. went from :croatian to :not_resto_event to :resto_event.
