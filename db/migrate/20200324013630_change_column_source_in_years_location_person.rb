class ChangeColumnSourceInYearsLocationPerson < ActiveRecord::Migration[6.0]
  def change
    rename_column :years,     :source_id, :doc_id
    rename_column :locations, :source_id, :doc_id
    rename_column :people,    :source_id, :doc_id
  end
end
