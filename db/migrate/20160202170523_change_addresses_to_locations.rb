class ChangeAddressesToLocations < ActiveRecord::Migration[5.0]
  def change
    rename_table :addresses, :locations
  end
end

# To avoid confusion with field name address.
