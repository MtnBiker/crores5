class AddSourceOnLaptopToLocation < ActiveRecord::Migration[5.0]
  def change
    add_column :locations, :source_url, :string
  end
end
