class DropTableRestoResidLines < ActiveRecord::Migration[6.0]
  # Going to add back in, but hoping to fix problems by doing it in Rails 6 and Postgres 10
  def change
    drop_table :resto_resid_lines
  end
end
