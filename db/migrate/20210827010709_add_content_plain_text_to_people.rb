class AddContentPlainTextToPeople < ActiveRecord::Migration[6.0]
  def change
    add_column :people, :content_plain_text, :text
  end
end
