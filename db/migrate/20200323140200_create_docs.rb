class CreateDocs < ActiveRecord::Migration[6.0]
  def change
    create_table :docs do |t|
      t.integer :source
      t.string :page_no
      # t.string :image will be attached with Active Record
      t.string :original_url
      t.string :basename

      t.timestamps
    end
  end
end
