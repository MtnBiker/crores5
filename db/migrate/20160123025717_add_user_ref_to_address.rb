class AddUserRefToAddress < ActiveRecord::Migration[5.0]
  def change
    add_reference    :addresses, :person, index: true
    add_foreign_key  :addresses, :people

    add_reference    :people, :address, index: true
    add_foreign_key  :people, :addresses

    add_column :years, :resto, :boolean
    add_column :years, :resid, :boolean
    add_column :years, :source, :text
    add_index  :years, [:people, :addresses], unique: true # not sure what I wanted here. 2019.05.03 commented out because of an error running bin/rails db:migrate RAILS_ENV=test. Stopped the error.
    # 2019.09.25 put back in and bin/rails db:migrate RAILS_ENV=test still has an error
  end
end
