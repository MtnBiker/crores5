class CreateJoinTableDocsPeople < ActiveRecord::Migration[6.0]
  def change
    create_join_table :docs, :people do |t|
      t.index [:doc_id, :person_id]
      t.index [:person_id, :doc_id]
    end
  end
end
