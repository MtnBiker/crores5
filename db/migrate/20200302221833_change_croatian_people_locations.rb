class ChangeCroatianPeopleLocations < ActiveRecord::Migration[6.0]
  def change
    change_column_default(:people, :croatian, nil)
    change_column_default(:locations, :croatian, nil)
  end
end
