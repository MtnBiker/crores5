class ChangeFileBasenamesFromLocations < ActiveRecord::Migration[6.0]
  change_table :locations do |t|
    t.rename :source, :file_basename
  end
end
