class ChangeCroatianYears < ActiveRecord::Migration[6.0]
  def change
    change_column_default(:years, :croatian, nil)
  end
end
