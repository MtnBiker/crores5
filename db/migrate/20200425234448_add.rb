class Add < ActiveRecord::Migration[6.0]
  def change
    # add_column :docs. :docable, polymorphic: true # Already have columns docable_id and docable_type and Rails should take of of the polymorhic TRUE?
    # add_index :docs, :docable # Do I need to do this. Odin Project Advanced Active Record Associations doesn't mention it. Rails Guides Association Basics does have and add index but with different syntax below which may amount to the same thing. But below is more specific, so go with it.
    # add_index :docs, [:docable_type, :docable_id]
    # # Note that Rails Guides Association Basics doesn't reuse the base, that is, it didn't call it the equivalent of docable, but used imageable which apparerently didn't relate to anything but this
    # index_docs_on_docable_type_and_docable_id already exists in pgAdmin, so suspect this is covered
  end
end

# Run this with nothing in it to make it easier for me to keep track of.
