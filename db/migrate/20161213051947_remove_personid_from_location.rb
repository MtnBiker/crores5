class RemovePersonidFromLocation < ActiveRecord::Migration[5.0]
  def change
    remove_column :locations, :person_id, :integer
  end
end
