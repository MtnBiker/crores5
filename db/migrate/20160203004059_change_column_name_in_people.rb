class ChangeColumnNameInPeople < ActiveRecord::Migration[5.0]
  def change
    rename_column :people, :address_id, :location_id
  end
end

# Probably don't need this column, but update now and delete later
