class ChangeSourcesFromLocations < ActiveRecord::Migration[6.0]
  change_table :sources do |t|
    t.rename :source, :file_basename
  end
end
