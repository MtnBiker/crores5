class AddSnippetCoordsToYears < ActiveRecord::Migration[6.0]
  def change
    add_column :years, :snippet_coords, :json
  end
end
