class ChangeColumnCaptionFromSources < ActiveRecord::Migration[6.0]
  def change
    remove_column :sources, :caption, :string
    add_column :sources, :caption, :text
  end
end
