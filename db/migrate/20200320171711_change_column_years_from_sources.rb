class ChangeColumnYearsFromSources < ActiveRecord::Migration[6.0]
  change_table :sources do |t|
    t.rename :year, :cover_year
  end
end
