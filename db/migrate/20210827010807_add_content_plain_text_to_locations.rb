class AddContentPlainTextToLocations < ActiveRecord::Migration[6.0]
  def change
    add_column :locations, :content_plain_text, :text
  end
end
