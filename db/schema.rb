# THIS FILE IS NO LONGER BEING USED: structure.sql takes its place

# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# This file is the source Rails uses to define your schema when running `rails
# db:schema:load`. When creating a new database, `rails db:schema:load` tends to
# be faster and is potentially less error prone than running all of your
# migrations from scratch. Old migrations may fail to apply correctly if those
# migrations use external dependencies or application code.
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2019_09_17_172340) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "fuzzystrmatch"
  enable_extension "plpgsql"
  enable_extension "postgis"

  create_table "active_storage_attachments", id: :bigint, default: -> { "nextval('active_storage_attachments_id_seq'::regclass)" }, force: :cascade do |t|
    t.string "name", null: false
    t.string "record_type", null: false
    t.bigint "record_id", null: false
    t.bigint "blob_id", null: false
    t.datetime "created_at", null: false
    t.index ["blob_id"], name: "index_active_storage_attachments_on_blob_id"
    t.index ["record_type", "record_id", "name", "blob_id"], name: "index_active_storage_attachments_uniqueness", unique: true
  end

  create_table "active_storage_blobs", id: :bigint, default: -> { "nextval('active_storage_blobs_id_seq'::regclass)" }, force: :cascade do |t|
    t.string "key", null: false
    t.string "filename", null: false
    t.string "content_type"
    t.text "metadata"
    t.bigint "byte_size", null: false
    t.string "checksum", null: false
    t.datetime "created_at", null: false
    t.index ["key"], name: "index_active_storage_blobs_on_key", unique: true
  end

  create_table "lines", id: :integer, default: -> { "nextval('lines_id_seq'::regclass)" }, force: :cascade do |t|
    t.integer "person_id"
    t.integer "resid_loc_id"
    t.integer "resto_loc_id"
    t.date "resto_date"
    t.date "resid_date"
    t.string "title_resto"
    t.string "title_resid"
    t.string "resto_name"
    t.integer "resto_connection_id"
    t.integer "resid_connection_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "locations", id: :integer, default: -> { "nextval('locations_id_seq'::regclass)" }, force: :cascade do |t|
    t.string "address"
    t.string "city", default: "Los Angeles"
    t.string "state", default: "CA"
    t.decimal "longitude", precision: 9, scale: 6
    t.decimal "latitude", precision: 9, scale: 6
    t.boolean "extant"
    t.string "current_description"
    t.string "notes"
    t.datetime "created_at", null: false
    t.datetime "updated_at"
    t.geometry "geom", limit: {:srid=>4326, :type=>"st_point"}
    t.text "source"
    t.text "geocoded_with"
    t.boolean "coords_not_locked", default: true
    t.string "ref_link"
    t.index ["address"], name: "index_locations_on_address", unique: true
  end

  create_table "people", id: :integer, default: -> { "nextval('people_id_seq'::regclass)" }, force: :cascade do |t|
    t.date "date_of_birth"
    t.date "date_of_entry"
    t.text "doe_source"
    t.date "date_of_citizenship"
    t.text "notes"
    t.string "last_name"
    t.string "given_name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "location_id"
    t.text "source"
    t.datetime "date_of_death"
    t.string "place_of_birth"
    t.index ["location_id"], name: "index_people_on_location_id"
  end

  create_table "positions", id: :integer, default: -> { "nextval('positions_id_seq'::regclass)" }, force: :cascade do |t|
    t.string "title"
  end

  create_table "resto_resid_lines", id: :integer, default: -> { "nextval('resto_resid_lines_id_seq'::regclass)" }, force: :cascade do |t|
    t.integer "person_id"
    t.date "resto_date"
    t.date "resid_date"
    t.string "title_resto"
    t.string "title_resid"
    t.string "resto_name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "resto_connection_id"
    t.integer "resid_connection_id"
    t.integer "resto_loc_id"
    t.integer "resid_loc_id"
    t.decimal "lat_resid", precision: 9, scale: 6
    t.decimal "long_resid", precision: 9, scale: 6
    t.decimal "lat_resto", precision: 9, scale: 6
    t.decimal "long_resto", precision: 9, scale: 6
    t.string "resto_address"
    t.string "resid_address"
    t.date "mid_date"
  end

  create_table "users", id: :integer, default: nil, force: :cascade do |t|
    t.string "name"
    t.string "email"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "password_digest"
    t.string "remember_digest"
    t.boolean "admin", default: false
    t.string "activation_digest"
    t.boolean "activated", default: false
    t.datetime "activated_at"
    t.string "reset_digest"
    t.datetime "reset_sent_at"
  end

  create_table "years", id: :integer, default: -> { "nextval('years_id_seq'::regclass)" }, force: :cascade do |t|
    t.date "year_date"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.boolean "resto"
    t.boolean "resid"
    t.text "source"
    t.integer "person_id"
    t.integer "location_id"
    t.string "title"
    t.text "notes"
    t.string "resto_name"
    t.string "source_url"
    t.string "ref_url"
    t.string "caption"
    t.index ["location_id"], name: "index_years_on_location_id"
    t.index ["person_id"], name: "index_years_on_person_id"
  end

  add_foreign_key "active_storage_attachments", "active_storage_blobs", column: "blob_id"
  add_foreign_key "people", "locations"
  add_foreign_key "years", "locations"
  add_foreign_key "years", "people"
end
