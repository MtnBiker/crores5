# number is minimum
# >= upgrade even to major version, i.e, for ever
# ~> upgrade but NOT to the next major version, e.g, if ~> 1.14, upgrade but not to 2.0

source 'https://rubygems.org' # Try just git, one source is supposed to be enough, but Heroku doesn't seem to see the following one. So uncommenting this.

git_source(:github) do |repo_name|
  repo_name = "#{repo_name}/#{repo_name}" unless repo_name.include?('/')
  "https://github.com/#{repo_name}.git"
end

# ruby '2.7.2' # 3.0.0 should wait until 2022 or Rails 7. https://devcenter.heroku.com/articles/getting-started-with-rails6 says to. Don't rush to change to .upgrade. ..are probably a good idea for security
ruby '3.0.2'
gem 'rails', '~> 6.0' # changed from 6.0.0 to see if fixed ActionText failure IF GO TO 6.1 SEE config/environment/production.rb ~line 23 and change. Get errors trying for 6.1.0
gem 'importmap-rails' # (make sure it's included before any gems using it!) https://github.com/rails/importmap-rails/
gem 'hotwire-rails' # installs both Turbo and Stimulus as of Aug. 2021 ?? 
# gem 'turbo-rails' # Hotwire https://evilmartians.com/chronicles/hotwire-reactive-rails-with-no-javascript Turbo installed by hotwire-rails
# gem 'turbolinks', '~> 5' # gem not needed with Webpacker, i.e., installed via Yarn. And now using turbo-rails
gem 'webpacker', '~> 5.0' # Now up to v6, but may be less important with Rails 7

gem 'puma', '~> 4' # v.5 is out https://schneems.com/2019/06/26/puma-4-new-io-4-your-server/
gem 'pg' # Use postgresql as the database for Active Record
gem 'active_storage_validations' # https://github.com/igorkasyanchuk/active_storage_validations
# Use SCSS for stylesheets
# gem 'sassc-rails', '>= 2.0.1' # this instead of sassc and sass-rails ? Seems to work. If don't need Sprockets just use gem 'sassc'. I think I need to use Sprockets. sass-rails is probably enough for me. sassc-rails adds some to sass-rails, but requires setting up a couple of things
# gem 'sassc' # Trying to resolve bundle error per https://github.com/sass/sassc-ruby#readme. Error went away. Don't understand. quit using with sassc-rails. Follows what is shown for bootstrap-sass
# Use Uglifier as compressor for JavaScript assets
# gem 'uglifier' #, '>= 3.1.7' # not needed with Webpacker
# gem 'jquery-rails' # Webpacker Installed anyway
# gem 'jquery-ui-rails' # Webpacker?
# gem 'mimemagic', '0.3.5', git: 'https://github.com/mimemagicrb/mimemagic', ref: '01f92d8' # needed Mar 2021 to fix a gem sourcing problem FIXME. Can delete when the need for this is fixed. It's a dependency issue

# Build JSON APIs with ease. Read more: https://github.com/rails/jbuilder. Needed for map
gem 'bcrypt',                  '~> 3.1.7'
gem 'faker',                   '1.4.2'
gem 'fog',                     '1.36.0'
gem 'image_processing' # ActiveStorage uses in place of mini_magick in 6.0. But can't tell which library is used and HEIC don't seem to work. https://github.com/janko/image_processing
gem 'jbuilder', '~> 2.9' # 2019.11.27 v2.9.1 is current
# gem 'sdoc', '~> 0.4.0', group: :doc # 2021.06.30 Why do I have this? Asking for json which has deprecated code
# gem 'will_paginate'
# gem 'bootstrap-will_paginate', '0.0.10'
gem 'simple_form' # https://simple-form-bootstrap.herokuapp.com/documentation
# gem "bootstrap_form", "~> 4.0" # trying this to replace simple_form. Turned off 2021.05.18 to see if helped with checkbox. It did not. If use enable in application.scss. Or should I use Yarn with this
gem 'fuuu'
# geo stuff
gem 'geocoder'
# gem 'activerecord-postgis-adapter' # Failing with Rails 6. Below is a branch until master gets updated. 2021.08.25  rails won't launch
# Removed this 2021.08.25 and see if app works. Was having migration problems with the postgresql adaptor
# gem 'activerecord-postgis-adapter',
#     git: 'https://github.com/corneverbruggen/activerecord-postgis-adapter', branch: 'activerecord-6.0'
gem 'rgeo-activerecord' # 6.2.1
gem 'rgeo-geojson' # rgeo added with bundle, but I don't have
# aws
gem 'fog-aws' # What is this for?
gem 'aws-sdk-s3', '~> 1' # , require: false # rails s fails with require: false. Can change to this as aws-sdk loads other stuff, but I only need s3. https://github.com/aws/aws-sdk-ruby
# gem "aws-sdk-s3", require: false # per https://guides.rubyonrails.org/active_storage_overview.html#setup, but rails s fails. IT WORKS AS IT WAS, SO LEAVE WELL ENOUGH ALONE UNTIL I KNOW I NEED TO CHANGE. Works now with aws.rb out. Not quite. Some interaction between this and
# gem 'pg-eyeballs' # gives you detailed information about how the SQL queries created by the active record # TODO Error on bundle update, maybe not Rails 6 compatible yet
# gem 'leaflet-rails' # Being kept up to date. Should be available across the app. But version may be problem as this will be the latest. Leaflet doesn't seem to break many things with updates. Getting an error 2020.01.08 abbout mapTwo.js:14 Uncaught TypeError: L.timeline is not a function so turning back on. Still getting error, so problem is somewhere else in the pipeline. TODO Try removing this and see if leaflet still works, Added with Yarn

# Use Capistrano for deployment
# gem 'capistrano-rails', group: :development

gem 'bootsnap', require: false
gem 'pg_search' # Using for Stimulus search trial: https://medium.com/swlh/build-a-dynamic-search-with-stimulus-js-and-rails-6-56b537a44579
gem 'ransack' # searching https://github.com/activerecord-hackery/ransack and https://github.com/ylankgz/InvoiceApp  for using with date interval and http://railscasts.com/episodes/370-ransack?view=asciicast
gem 'naturalsorter' # 2021.08.03 to see if it would work on sources/show
gem "naturally", "~> 2.2"
gem 'responders'

group :development, :test do
  # gem 'rails_db' # http://localhost:3000/rails/db to see the database. No editing. pgAdmin covers this, so not sure of its utility for me. Adds jquery and jquery_ujs. Might be helpful at Heroku.
  gem 'factory_bot_rails' # formerly factory_girl
  gem 'rspec-rails'
  # Capybara, the library that allows us to interact with the browser using Ruby
  gem 'capybara' # Maybe should only be in group :test
  # This gem helps Capybara interact with the web browser. From https://www.codewithjason.com/rails-integration-tests-rspec-capybara/
  gem 'webdrivers' # Maybe should only be in group :test
  gem 'awesome_print' # Prefs in ~/.irbrc. See also .pryrc
  gem 'super_awesome_print' # Gilmore just uses awesome_print may be enough-no sap shows up better. In code sap <var> will print to iTerm server log   # https://undefined-reference.org/2016/01/31/super_awesome_print-as-debugger.html
  gem 'database_cleaner'
  gem 'dotenv-rails' # See p198 Clark. Right now I just have Mapbox credential. But I'm using it for AWS I think.

  # Copy db from (and to?) Heroku
  # To see an updated list of tasks and descriptions: rake heroku_db_restore -T heroku_db_restore
  gem 'dead_end' # replaces syntax_search https://github.com/zombocom/dead_end
  gem 'heroku_db_restore'

  # Call 'byebug' anywhere in the code to stop execution and get a debugger console. Used for tests also
  # gem 'byebug', platform: :mri # Put <% byebug %> in code. Takes you to iTerm. quit to exit. Do I need this with pry-byebug
  # gem 'ruby_jard' # complements or replaces pry and byebug. `jard` to stop and dumps to terminal. Ran into a bundle install issue

  # https://gist.github.com/justin808/1fe1dfbecc00a18e7f2a
  gem 'pry' # Console with powerful introspection capabilities
  gem 'pry-byebug' # Integrates pry with byebug. Insert `binding.pry` in code
  # gem 'pry-doc' # Provide MRI Core documentation. Not supported by ruby 3.0.2
  gem 'pry-rails' # Causes rails console to open pry. `DISABLE_PRY_RAILS=1 rails c` can still open with IRB. https://github.com/pry/pry/wiki
  gem 'pry-rescue' # Start a pry session whenever something goes wrong.
  gem 'pry-theme' # An easy way to customize Pry colors via theme files 
end

group :development do
  # Access an IRB console on exception pages or by using <%= console %> in views
  gem 'better_errors'
  gem 'binding_of_caller' # suggested by error message for documents. Didn't help
  gem 'web-console', '>= 3.3.0'

  # Spring speeds up development by keeping your application running in the background. Read more: https://github.com/rails/spring
  # Copeland doesn't like this, so turned it off for now
  # gem 'spring'
  # gem 'spring-watcher-listen', '~> 2.0.0'
  gem 'annotate' # https://github.com/ctran/annotate_models, on command line `annotate`
  gem 'listen', '~> 3.0.5' # Default in Rails 5, but I didn't have it before and not sure I understand how to use it. 2020.12.28 back in because couldn't launch server
  gem 'rails-erd' # Entity-Relationship Diagrams, `rake erd` or `bundle exec rake erd` to get a pdf http://voormedia.github.io/rails-erd/install.html
  gem 'rubocop-rails', require: false # on command line `rubocop --require rubocop-rails --rails`
  gem 'rubocop-rspec'
end

group :production do
end

group :test do
  gem 'minitest-reporters', '1.1.9'
  # gem 'guard',              '2.13.0' # Rails 5 Hartl. Turned off to see if helped with ffi install issue
  # gem 'guard-minitest',     '2.4.4'  # Rails 5 Hartl
  gem 'rails-controller-testing'
  # Creates /coverage/index.html which details MiniTest coverage
  gem 'simplecov', require: false
end

