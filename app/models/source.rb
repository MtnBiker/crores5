# models/source.rb
class Source < ApplicationRecord
  # has_many :locations      # now connected thrugh docs and they don't need to know about source
  #  has_many :people         # now connected thrugh docs and they don't need to know about source
  # has_many :years          # now connected thrugh docs and they don't need to know about source
  has_many :docs
  # # Copied from years
  has_many_attached :documents # ActiveStorage. Deprecated since it gets its documents from docs FIXME
  # has_rich_text :caption # ActionText 2019.12.05 not implemented because form needs major changes to work
  # scope :with_eager_loaded_documents, -> { eager_load(documents_attachments: :blob) } # Multiple Images. I replaced images with documents, but no idea if that matters

  # https://medium.com/swlh/build-a-dynamic-search-with-stimulus-js-and-rails-6-56b537a44579
  include PgSearch::Model
  pg_search_scope :global_search,
      # A list of fields to be searched against.
      against: [
        :id,
        :effective_date,
        :cover_year,
        :name,
        :publisher,
        :file_basename,
        :from_where,
        :from_where_url,
        :caption,
        :notes,
      ],
    using: {
      tsearch: { prefix: true }
  }

  # For use with docs
  def year_name
    [cover_year, name].join(' ') # more Ruby like
  end

  def year_name_id
    "#{cover_year} #{name}  (#{id})" # so this is more Rails like?
  end
end
