# models/person.rb
class Person < ApplicationRecord
  # include MyDocumentValidator

  has_many :years, dependent: :destroy # destroy gets rid of the years (connection) link if person is deleted which is OK, but the converse is not desirable.
  has_many :locations, through: :years
  belongs_to :doc

  # has_many :doc_people
  # has_many :docs, through: :doc_people
  # has_many :docs, through: :years
  has_many :resto_resid_lines

  # belongs_to :source # FIXME commented so update and save would work, but I think I want this. Location doesn't have this and they are similar in their associations
  has_many_attached :documents # ActiveStorage FIXME legacy
  has_rich_text :content # ActionText
  
  before_save :content_body_to_plain_text

  validates :last_name, presence: true, length: { maximum: 50 }
  validates :given_name, presence: true, length: { maximum: 50 } # ,

  # https://medium.com/swlh/build-a-dynamic-search-with-stimulus-js-and-rails-6-56b537a44579
  include PgSearch::Model
  pg_search_scope :global_search,
      # A list of fields to be searched against.
    against: [:id, :last_name, :given_name, :date_of_birth, :place_of_birth, :date_of_death, :date_of_entry,
         :doe_source, :date_of_citizenship, :notes, :content_plain_text],
    using: {
      tsearch: { prefix: true }
  }

  # uniqueness: {:allow_blank => true} # https://stackoverflow.com/questions/39512689/pguniqueviolation-error-duplicate-key-value-violates-unique-constraint Stopped a testing error (below) by adding this line. Why? But got count error, so maybe this "fix" is making things worse
  # Error:
  # PeopleControllerTest#test_should_create_person:
  # ActiveRecord::RecordNotUnique: PG::UniqueViolation: ERROR:  duplicate key value violates unique constraint "index_people_on_given_name_and_last_name"
  # DETAIL:  Key (given_name, last_name)=(GivenName1, LastName1) already exists.

  # FIXME. How get from a concern?
  # include LocationsController
  # validates_with MyDocumentValidator # which is in models/concerns/my_document_validator. Does this work?

  # class Profile
  #   def name_split
  #     @split_name ||= fullname.split # @split_name would cache the result so that no need to split the fullname every time
  #   end
  # end

  def self.search(search_people)
    where('last_name  ILIKE ?', "%#{search_people}%")
    # where("given_name ILIKE ?", "%#{search_people}%") #  Doesn't find all with more than one of these
    # where("notes      ILIKE ?", "%#{search_people}%") #
    # where("date_of_birth ILIKE ?", "%#{search_people}%")
    # where("place_of_birth ILIKE ?", "%#{search_people}%")
    # where("date_of_death ILIKE ?", "%#{search_people}%")
    # where("date_of_entry ILIKE ?", "%#{search_people}%")
    # where("doe_source ILIKE ?", "%#{search_people}%")
    # where("date_of_citizenship ILIKE ?", "%#{search_people}%")    #
    # where("source ILIKE ?", "%#{search_people}%")
  end

  def full_name
    # "#{given_name} #{last_name}"
    [given_name, last_name].join(' ') # more Ruby like
  end

  def full_name_id
    "#{given_name} #{last_name} (#{id})"
  end

  def last_first
    "#{last_name}, #{given_name} "
  end

  def last_first_id
    "#{last_name}, #{given_name} (#{id})"
  end

  def full_name_id
    "#{given_name} #{last_name} (#{id})"
  end

  # For next and previous in show. Does this need testing. NOT CRITICAL
  def next
    Person.where(['id > ?', id]).first
  end

  def previous
    Person.where(['id < ?', id]).last
  end

  # Calculate value of content_plain_text by stripping html markup from content
  def content_body_to_plain_text
    self.content_plain_text = content.body.to_plain_text
  end
end
