class ViewRestoResidLine < ApplicationRecord
  # self.primary_key = 'name', 'resto_date', 'resto_connection_id', 'resid_connection_id'  # apparently don't need a PK, at least for index

  def map_popup # so far not being used. Copied here, but line_popup is what I needed.
    # "#{person.given_name} #{person.last_name} was a #{title} at #{location.address} in #{year_date.to_formatted_s(:month_year)}"  # copied from year.rb
    "#{name} was a #{title} at #{location.address} in #{year_date.to_formatted_s(:month_year)}"
  end # map_popup

  def line_popup # copied from resto_resid_line.rb
    # the 'line_popup = ' appears to be extraneous? Where?
    # Following line to prevent weird blank when resto_name is unknown. Getting an extraneous comma when name not known.
    # the variables need to be in map_controller > line_data > @lines
    resto_name ? resto_name_popup = "at #{resto_name}," : resto_name_popup = 'at unknown restaurant name at'
    if resid_date == resto_date
      line_popup = "Connection: In #{mid_date.to_formatted_s(:month_year)} #{full_name} was a #{title_resto} #{resto_name_popup} #{resto_address}, and lived at #{resid_address}"
    else
      line_popup = "Connection: #{full_name} was a #{title_resto} #{resto_name_popup} #{resto_address} in #{resto_date.to_formatted_s(:month_year)} and lived at #{resid_address} in #{resid_date.to_formatted_s(:month_year)}"
    end
  end # line_popup
end

# #{person.given_name} #{person.last_name}  was in XXX. Get rid of error, but nothing popup is blank
