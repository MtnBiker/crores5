# models/doc.rb
class Doc < ApplicationRecord
  has_one_attached :image # Active Storage
  # puts "XXXX models/doc:4. @doc: #{@doc}." # doc undefined on edit and show . @doc is nil on show and edit # popped up on an error check and not sure I need this, was debugging I assume
  # sap @doc # super_awesome_print out to iTerm. for debugging issues with metadata # development only. Error at heroku
  before_save :content_body_to_plain_text
  after_save_commit :analyze_metadata_now # same as :analyze_metadata_now, [ on: :create, on: :update ] # # see def below for explanation and link # This now makes sure metadata is created correctly ## after_commit may be deprecated
  belongs_to :source

  # Are the following three handled by …able? I think so since that's what Rails Guides Active Records basics has
  # Will this prevent accidentally deleting a doc with references?
  has_many :locations
  has_many :people
  has_many :years # this works for one doc per year and cloned for people and locations, but not needed to add doc??
  # has_and_belongs_to_many :years
  # has_and_belongs_to_many :people
  # has_and_belongs_to_many :locations

  has_rich_text :content # Action Text. This is for notes but there is no content in the table

  # The following may or may not work FIXME

  # Validate attachment in Active Storage using gem 'active_storage_validations'  https://github.com/igorkasyanchuk/active_storage_validations
  validates :image, attached: true, content_type: [:png, :jpg, :jpeg] # , :pdf, :heic , message: "Apparently HEIC not supported." # added last two since I want them, vips supports them, so should work? HEIC causes an error, but I don't think it's this that's checking. Gives generic message
  # validates :image, presence: true # , uniqueness: true # do upload title page twice #TypeError at doc.save with uniqueness?
  validates :page_no, uniqueness: { scope: [:source_id], message: '. Already a doc for this page number for this source' } # SWAG. Doesn't work right if add , presence: true. Says needs page_no even if has one, but OK if separate below.
  validates :page_no, presence: true # Turned off to see if would do doc.image.metadata. Yes on re-edit

  # validates :source_id, uniqueness: { scope: [:page_no], message: '. This page already entered for this directory' } # May not work for items that aren't city directories. Probably simplier syntax for this situation https://stackoverflow.com/questions/3276110/rails-3-validation-on-uniqueness-on-multiple-attributes Screwed up metadata creation FIXME. Readd when fix that problem

  # For Ransacker to sort on page no. correctly. Does it work? No: Need to sort character varying, this is for integer
  # ransacker :page_no do
  #  Arel.sql("to_char(page_no, '9999999')")
  # end

  # https://medium.com/swlh/build-a-dynamic-search-with-stimulus-js-and-rails-6-56b537a44579
  include PgSearch::Model
  pg_search_scope :global_search,
      # A list of fields to be searched against.
      against: [:id, :original_url, :basename, :notes, :content_plain_text],
    using: {
      tsearch: { prefix: true }
  }

  # Calculate value of content_plain_text by stripping html markup from content
  def content_body_to_plain_text
    self.content_plain_text = content.body.to_plain_text
  end

  def doc_source_page
    "#{page} #{basename} [page basename as test]"
  end

  def year_page
    [source.cover_year, page_no].join(' ') # more Ruby like
  end

  def year_name_page # 949ms and needs id
    "#{source.try(:cover_year)} #{source.try(:name)}. p#{page_no}"
  end

  def year_name_page_notes # a bit unwieldly but it helps for selecting the oc
    "#{source.try(:cover_year)} #{source.try(:name)}. p#{page_no}. #{notes}"
  end

  def id_page_year_name__notes_content # takes 3 seconds to create this using in years>new and probably other places
    "#{id}. #{page_no}. #{source.try(:cover_year)} #{source.try(:name)}. #{notes}. #{content}"
  end

  def id_page # fast, but would like more info. Try for a bit to see if it helps with buttons not working
    "#{id}. #{page_no}"
  end

  def id_page_content # Overall  3370ms. Hard to read, because trix.
    "#{id}. #{page_no}. #{content}"
  end

  # def page_int # trying for sorting in sources>show. Couldn't access this variable
  #    page_no.to_i # "#{page_no.to_i}"
  # end

  def page_year_name__notes_content # a bit unwieldly but it helps for selecting the doc. Changed to above when too many duplicate address numbers
    "#{page_no}. #{source.try(:cover_year)} #{source.try(:name)}. #{notes}. #{content}" # Shows the formatting for trix in the drop down menu
  end

  def test_source_id_page_no
    "#{source_id}. #{page_no}"
  end

  # For next and previous in show.
  def next
    Doc.where(['id > ?', id]).first
  end

  def previous
    Doc.where(['id < ?', id]).last
  end

  # The last doc, i.e., highest numerical id. The previous about is just that. Use occasionally when adding docs to find out where I am
  def last
    Doc.last
  end

  # https://blog.eq8.eu/til/image-width-and-height-in-rails-activestorage.html
  # Can't seem to access this, but can access them. See doc>show
  def height
    image.metadata['height']
  end

  def width
    image.metadata['width']
  end

  # private # get an error if make private NO. Get error after using once; now seems to work. Have to restart server
  # Sometimes docs are created with incomplete metadata, this forces it to be done. Dimensions and 'analyzed' are missing
  def analyze_metadata_now
    puts "X X X X  models/doc:106. image.to_yaml: #{image.to_yaml}" # Debugging. not activated on show as expected. object on update # image.class: #{image.class}.  ActiveStorage::Attached::One and is the first field of .to_yaml
    # puts "XXXX models/doc:88. sap image below YYYYY"
    sap image # Debugging(super_awesome_print). #<ActiveStorage::Attached::One:0x00007fea7e0d4178 @name="image", @record=#<Doc id: 631, source_id: 49, page_no: "262", original_url: nil, basename: "1890–1 Los Angeles and Orange County Business Dire...", created_at: "2021-05-28 05:42:10", updated_at: "2021-05-28 05:42:10", notes: nil, snippet_coords: nil>>
    begin
      image.analyze # if image.attached? # errors on image if can't find, so the if is ignored anyway
    rescue
      puts " XX X X  models/doc:112. image.to_yaml. image couldn't be analyzed X X X X. Rescued from image.analyze"
    end
  end
end
