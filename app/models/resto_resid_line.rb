#  No longer being used. Replaced by view_resto_resid_line
class RestoResidLine < ApplicationRecord
  belongs_to :location
  belongs_to :person
  belongs_to :year

  # This may not be being activated and no saves are done with the problem. 2019.09.26. So there may be problems here.. Lat an lng may not always be available.
  validates :person_id, :resto_loc_id, :resto_name, :resto_date, :title_resto, :lat_resto,
            :long_resto, :resto_address, :resid_loc_id, :resid_date, :lat_resid, :long_resid, :resid_address, :title_resid, :mid_date, :resto_connection_id, :resid_connection_id, presence: true
  validates :person_id, :resto_loc_id, :resid_loc_id, :resto_connection_id, :resid_connection_id,
            numericality: { only_integer: true }
  # validates :resto_date, :resid_date, :mid_date, numericality: { only_dates: true } # only_dates doesn't exist

  def line_popup
    # the 'line_popup = ' appears to be extraneous? Where?
    # Following line to prevent weird blank when resto_name is unknown. Getting an extraneous comma when name not known.
    resto_name_popup = resto_name ? "at #{resto_name}," : 'at'
    if resid_date == resto_date
      line_popup = "Connection: In #{mid_date.to_formatted_s(:month_year)} #{person.given_name} #{person.last_name} was a #{title_resto} #{resto_name_popup} #{resto_address}, and lived at #{resid_address}"
    else
      line_popup = "Connection: #{person.given_name} #{person.last_name} was a #{title_resto} #{resto_name_popup} #{resto_address} in #{resto_date.to_formatted_s(:month_year)} and lived at #{resid_address} in #{resid_date.to_formatted_s(:month_year)}"
    end
  end # line_popup
end  # class
