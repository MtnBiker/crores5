# models/year.rb
class Year < ApplicationRecord
  belongs_to :location
  belongs_to :person
  belongs_to :doc # works with single doc selection
  # has_and_belongs_to_many :docs # trying with multiple doc selection
  has_many_attached :documents # ActiveStorage FIXME deprecated
  has_rich_text :content # Action Text. This is for notes but there is no content field in the table
  # has_rich_text :caption # ActionText 2019.12.05 not implemented because form needs major changes to work
  # belongs_to :source, through: :doc # Doc   has_many :years
  has_many :resto_resid_lines
  scope :with_eager_loaded_documents, lambda {
                                        eager_load(documents_attachments: :blob)
                                      } # Multiple Images. I replaced images with documents, but no idea if that matters

  default_scope lambda {
                  order(:year_date)
                } # For connection listing in person. following Hartl p553 (Doesn't affect years>index)
  # scope :within_year, -> {where("last(3)")} # Don't know if this works. Calling from view/years/line'. Calling from line different than calling from index
  # scope :resto_and_resid, -> {where("resto AND resid")} # this is not useful as it only compares one item, not one to another
  # The callback is getting called because puts works, now check if logic is working. It is, resto get set to true per iTerm. Yes resid is set, but not making it to the database
  # A class that defines a belongs_to :person association will use “person_id” as the default :foreign_key <http://api.rubyonrails.org/classes/ActiveRecord/Associations/ClassMethods.html>. I guess the foreign_key does not need to be explicity generated.
  # At this time Years contains FKs for People and Locations
  # validates :resto, !equal_to: :resid # don't allow having both checked
  before_save :content_body_to_plain_text
  validates :person_id, presence: true
  validates :location_id, presence: true # message: "Check if address_id value exists" Need help here
  validates :doc_id, presence: true # if one is not selected get an error on page: `Doc can't be blank`
  validates :name_shown, presence: true
  # The combination of :year_date, :person_id, :location_id is unique. Although could be a case where two different sources validate the fact, but then should probably add a note about the second source
  validates :year_date, uniqueness: { scope: [:person_id, :location_id, :title], message: '. Already an item for this person with this title at this address on this date' } # https://stackoverflow.com/questions/3276110/rails-3-validation-on-uniqueness-on-multiple-attributes

  # https://medium.com/swlh/build-a-dynamic-search-with-stimulus-js-and-rails-6-56b537a44579
  include PgSearch::Model
  pg_search_scope :global_search,
      # A list of fields to be searched against.
      against: [
        :id,
        :year_date,
        # :person_id,
        :name_shown,
        :title,
        # :location_id,
        # :resid,
        # :resto,
        :resto_name,
        :resto_event,
        :notes,
        # :doc_id,
   #      :snippet_extent,
   #      :snippet_coords,
   #      :content,
        :content_plain_text
      ],
    using: {
      tsearch: { prefix: true }
  }

  # FIXME: the following three can go away with changes in controller to set resto or resid
  # validates_presence_of :resto, {:if => :resto_name?, message: 'Click on Resto (above) since a restaurant name has been selected' } # Causes problems with
  # validates :resto, :presence => { :if => :resto_name?, message: 'Select Restaurant since a restaurant name has been specified' } # is this the same as above, but not working?
  # validates_with MyDocumentValidator # which is in models/concerns/my_document_validator. Does this work? FIXME
  # validates :documents, attached: true, content_type: [:png, :jpg, :jpeg] #  gem 'active_storage_validations'  https://github.com/igorkasyanchuk/active_storage_validations Not working, causes year item not to save FIXME loops back to new/update

  # validate :resid_xor_resid
  # validates_absence_of :resto, {:if => :resid?, message: 'Select only one of Residence or Restaurant' } # checks that only one resto or resid is checked. Couldn't make work with validates_presence_of Not needed with setting with controller

  # validates :title, inclusion: { in: %w(Bookkeeper Cashier Chef Clerk Co-proprietor Cook Dishwasher Manager Owner Restaurant Position Unknown Proprietor Waiter), if: :resto?,
  #     message: "%{value} is not acceptable for a restaurant." }
  #
  # validates :title, inclusion: { in: %w(Resident Householder), if: :resid?,
  # message: "%{value} is not acceptable for a residence." }

  def next_date_for_person_and_type
    Year.find(:all, conditions: ['depart_date = ?', 2.weeks.from_now.to_date])
    Year.where('id > ? AND year_date = ?', id, year_date).first
  end

  def map_popup # was an earlier version . Can be used for demo/testing
    "#{year_date.to_formatted_s(:month_year)}: #{person.given_name} #{person.last_name} was a #{title} at #{location.address}"
  end

  def resid_popup # was an earlier version # Maybe should be resid popup
    "#{year_date.to_formatted_s(:month_year)}: #{person.given_name} #{person.last_name} was a #{title} at #{location.address}"
  end

  def resid_popover
    "#{year_date.to_formatted_s(:month_year)}: #{title} at #{location.address}"
  end

  def resto_popup # need to fix date formatting
    if resto_name.blank? # no resto name [not sure he logic is righ]
      "#{year_date.to_formatted_s(:month_year)}: #{person.given_name} #{person.last_name} was a #{title} at #{location.address} at a restaurant named #{resto_name}."
    end #  with resto name
    "#{year_date.to_formatted_s(:month_year)}: #{person.given_name} #{person.last_name} was a #{title} at #{location.address}."
  end

  def resto_popover
    if resto_name.blank? # no resto name [not sure he logic is righ]
      "#{year_date.to_formatted_s(:month_year)}: #{title} at #{location.address} at a restaurant named #{resto_name}."
    end #  with resto name
    "#{year_date.to_formatted_s(:month_year)}: #{title} at #{location.address}."
  end

  # For next and previous in show. Better if sorted by date
  # Not working well
  def next
    Year.where('id > ? AND year_date = ?', id, year_date).first
  end

  def previous
    Year.where('id < ? AND year_date = ?', id, year_date).last
  end

  # https://blog.eq8.eu/til/image-width-and-height-in-rails-activestorage.html but not working for me as I would expect
  def height
    doc.metadata['height']
  end

  def width
    doc.metadata['width']
  end

  private

  # not needed when the next check works
  def resid_xor_resid
    if [resto, resid].compact.count != 1
      errors.add(:base, 'Select Residence or Restaurant, but not both')
    end
  end

  # Calculate value of content_plain_text by stripping html markup from content
  def content_body_to_plain_text
    self.content_plain_text = content.body.to_plain_text
  end

end
