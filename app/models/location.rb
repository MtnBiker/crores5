# models/location.rb
class Location < ApplicationRecord
  has_many :years # , dependent: :destroy # destroy gets rid of the years (connection) link if address is deleted. Do I really want to do this? I really should check or put a note in the Years section, but can't leave a broken link. Later commented out the destroy part.
  has_many :people, through: :years
  belongs_to :doc # works with single doc selection in years
  # has_many :docs, through: :years # was before correction below
  # has_many :docs, {:through=>:years, :source=>"doc"} # from error Ambiguous source reflection for through association. Please specify a :source directive on your declaration like:
  has_many_attached :documents # ActiveStorage FIXME when get docs completely implemented
  has_many :resto_resid_lines
  has_rich_text :content # ActionText

  before_save :content_body_to_plain_text

  # validates_with MyDocumentValidator # which is in models/concerns/my_document_validator. Does this work? FIXME
  # validates :doc_id, presence: true
  validates :city, presence: true
  validates :address, presence: true, length: { minimum: 5, maximum: 50 }, uniqueness: true # option: :uniqueness => {:case_sensitive => false}
  # Above wouldn't work if added other cities, maybe change to full address for uniquenes, but leave length with address

  # https://medium.com/swlh/build-a-dynamic-search-with-stimulus-js-and-rails-6-56b537a44579
  include PgSearch::Model
  pg_search_scope :global_search,
      # A list of fields to be searched against.
      against: [:address, :notes, :current_description, :city, :street_view_note, :geocoded_with],
    using: {
      tsearch: { prefix: true }
  }

  def longlat
    "#{longitude} #{latitude}"
  end

  def notes_plus_geocode # Not sure why the if is needed. Why not go with just the final one. One space extra?
    if notes == ''
      geocoded_with.to_s
    else
      "#{notes} #{geocoded_with}"
    end
  end

  # Not using? And should have an option to save existing first
  # def edit_next_location
  #   link_to 'Edit next', edit_next_location
  # end

  def full_address
    # full_address = "#{address}, #{city}, #{state}" # this and following should be equivalent
    [address, city, state].compact.join(', ')
  end

  # For next and previous in show.
  def next
    Location.where(['id > ?', id]).first
  end

  def previous
    Location.where(['id < ?', id]).last
  end

  # Calculate value of content_plain_text by stripping html markup from content
  def content_body_to_plain_text
    self.content_plain_text = content.body.to_plain_text
  end

  def self.search(search_locations)
    where('address ILIKE ?', "%#{search_locations}%")
    # where("notes ILIKE ?",   "%#{search_locations}%") # doesn't find all with more than one item
    # where("date_of_birth ILIKE ?", "%#{search_locations}%")
    # where("source ILIKE ?", "%#{search_locations}%")
  end

  # def coords_locked
  #   coords_locked = !coords_not_locked
  # end

  geocoded_by :full_address # Tells geocoder which address to use per https://github.com/alexreisner/geocoder
  # after_validation :geocode, :if => :address_changed?
  # after_validation :geocode, :if => !:coords_locked && :address_changed? # controller errors
  # No colon method missing

  #
  # after_validation :geocode # , :if => ( :coords_not_locked? && :address_changed? ) # auto-fetch coordinates (geocoder) # Not working coords_not_looked false has no effect
  after_validation :geocode, if: :coords_not_locked? # 2016-10-19 Seems to be working correctly. 2020.09.19 Giving poor results in general. Prefs set in config/initializers/geocoder.rb
  # after_validation :geocode, :if => :coords_locked == true  # undefined method `after_validation' for false:FalseClass
  # after_validation :geocode, :if => :coords_locked == false && :address_changed? # undefined method `after_validation' for false:FalseClass in controller
  # after_validation :geocode, :if => :coords_locked == 0 && :address_changed? # undefined method `after_validation' for false:FalseClass. Same error if change to 1
  # after_validation do |location|

  # after_validation :geocode,
  #   if: Proc.new { |location| location.coords_not_locked? && location.address_changed? }

  #     if :coords_locked
  #       break
  #     elsif :address_changed
  #       :geocode
  #     end
  #   end

  # gem geocoder. I think it's only this line that causes longitude and latitude to be written on submit new or update
  # Want geocoding if no coordinates and when adding a new address;
  # but not when make minor correction in address (whether coordinates exist or not)
  # but may want geocoding when have change.

  # Address has initial caps, no periods, St, Ct, etcc
  # ADDRESS_REGEX =
  # validates_format_of :location, :with => ADDRESS_REGEX
  # validates_uniqueness_of :id

  # Had to add presence: true to get test to work
end
