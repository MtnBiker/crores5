include ActiveModel::Validations

class MyDocumentValidator < ActiveModel::Validator
  def acceptable_image(record)
    return unless record.attached?

    errors.add(byte_size, 'is too big') unless record.byte_size <= 2.megabyte

    acceptable_types = ['image/jpeg', 'image/png']
    unless acceptable_types.include?(record.content_type)
      errors.add(:document, 'must be a JPEG or PNG')
    end
  end
end

# Another example https://medium.com/better-programming/how-to-implement-custom-activerecord-validations-235543f5dd8c
# Suggests that another syntax
# class DensityValidator < ActiveModel::Validator
#   def validate(record)
#     if record.density > 20
#       record.errors.add(:density, “is too high to safely ship”)
#     end
#   end
# end
