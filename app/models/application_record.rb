class ApplicationRecord < ActiveRecord::Base
  self.abstract_class = true
end

# Created when created table Images. Maybe a Rails 5 thing. Yes and all models should inherit from it. Secion 4.2 https://guides.rubyonrails.org/upgrading_ruby_on_rails.html

# Put it here to see if could pull up the connections table. Besides I'm already using connections in routing
# Probably should have its own model
# class Connections < ActiveRecord::Base
# end
