class AllLocations < ApplicationRecord
  # https://medium.com/swlh/build-a-dynamic-search-with-stimulus-js-and-rails-6-56b537a44579. Stimulus. Didn't get working. Also doesn't search ActionText
  include PgSearch::Model
  pg_search_scope :global_search,
                  against: [:id, :address],
                  using: {
                    tsearch: { prefix: false }
                  }
   
end

# bin/rails zeitwerk:check
# expected file app/models/all_locations_map.rb to define constant AllLocationsMap
# so I did. Don't know what will break
AllLocations = "" # Didn't ask for this but JIC