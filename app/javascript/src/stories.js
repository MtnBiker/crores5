// # Place all the behaviors and hooks related to the matching controller here.
// # All this logic will automatically be available in application.js.

// From MaptimeLA workshop 2017.05.20 https://github.com/MtnBiker/storymap_tutorial/blob/master/leaflet/2_leaflet.html
// This function create the map to load onto your web browser
"use strict";
function storyMap() {
  
  var map = L.map('map').setView([34.0622, -118.2680], 13);

  //This function adds a layer to the map, this layer would be added first, known as a base layer. Additional layers can be added on top.    
  L.tileLayer('http://d.tile.stamen.com/toner-lite/{z}/{x}/{y}.png', {
      attribution: '<a target="_top" href="http://stamen.com">Stamen Design, under CC BY 3.0, data by OSM</a>'
  }).addTo(map);

  //This function adds a tile layer from remote sources like the David Rumsey Map Collection, remove the "/*" at the beginning and "*/" at the end to activate.

  L.tileLayer('http://maps.georeferencer.com/georeferences/236482959682/2017-02-20T14:25:19.132722Z/map/{z}/{x}/{y}.png?key=zfSXuPw8W8w4AymmpUlZ', {
      attribution: '&copy; <a href="http://www.davidrumsey.com/luna/">David Rumsey Map Collection</a> GW Baist Map, 1921'
  }).addTo(map);


  //Here are other tiles you can switch, make sure you change the attribution to the source.
  //Stamen Toner-Lite | http://d.tile.stamen.com/toner-lite/{z}/{x}/{y}.png | <a target="_top" href="http://stamen.com">Stamen Design, under CC BY 3.0, data by OSM</a>
  //Stamen Watercolor | http://d.tile.stamen.com/watercolor/{z}/{x}/{y}.png | <a href="http://stamen.com">Stamen Design under CC BY 3.0, data by OSM</a>

  //This are the locations for your map to move along with your story
  //This is variable created an object called an JSON, you can add and remove chapters to the story. Make sure you reflect the changes in the sections with the text of your story.   
  // id is the id of the sections above
  var chapters = {
      'losangeles': {
          center: [34.0622, -118.2680],
          zoom: 9,
      },
      'boyleheights': {
          center: [34.0323, -118.2120],
          zoom: 13,
      },
      'kennethhahn': {
          center: [34.00408441523888, -118.34574043750764],
          zoom: 17,
      },
      'centralave': {
          center: [34.00672273556911, -118.25617347611393],
          zoom: 18,
      },
      'chavezravine': {
          center: [34.081204647758256, -118.23789479255632],
          zoom: 15
      },
      'watts': {
          center: [33.938957715132304, -118.24182004928838],
          zoom: 17
      },
      'unionstation': {
          center: [34.05573545250901, -118.23536853348772],
          zoom: 16
      }
  };

  //Function to run the story    
  // On every scroll event, check which element is on screen
  // Adapted from MapBox
  window.onscroll = function() {
      var chapterNames = Object.keys(chapters);
      for (var i = 0; i < chapterNames.length; i++) {
          var chapterName = chapterNames[i];
          if (isElementOnScreen(chapterName)) {
              setActiveChapter(chapterName);
              break;
          }
      }
  };

  var activeChapterName = 'losangeles'; //Change this to match the first chapter of your story
  function setActiveChapter(chapterName) {
      if (chapterName === activeChapterName) return;

      map.flyTo(chapters[chapterName].center, chapters[chapterName].zoom);

      document.getElementById(chapterName).setAttribute('class', 'active');
      document.getElementById(activeChapterName).setAttribute('class', '');

      activeChapterName = chapterName;
  }

  function isElementOnScreen(id) {
      var element = document.getElementById(id);
      var bounds = element.getBoundingClientRect();
      return bounds.top < window.innerHeight && bounds.bottom > 0;
  }
  //End of function to run the story    
    

  //This JS Function asks Mapbox to get the bearing/center/zoom/pitch of the mouse position when clicked.
  //To turn off this function, add a "/*" at the beginning of the function and a "*/" at the end. This will turn the function into a comment.
  map.on('click', function () {
      // e.lngLat is the longitude, latitude geographical position of the event
      var latlng = map.getCenter();
      console.log(latlng);
      prompt("Copy to clipboard: Ctrl+C, Enter",
          "center: ["+latlng.lat+", "+latlng.lng+"],\n"+
          "zoom: "+map.getZoom()+",\n"
      );
  });   
}
