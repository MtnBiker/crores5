// window.onload = init; // function called after window loaded. Needed to have the html document defined and available
// function init() {
console.log('demoEventListener:3. Entered script') // ok and with without window.onload 
  // Getting this to work in the simplist way https://medium.com/@ysmiracle/use-addeventlistener-instead-of-onclick-oninput-onchange-especially-when-working-in-teams-50ad40badb8d
  function clickMe() {
      console.log("I'm clicked!");
  }

  function clickMeAlso() {
      console.log("I'm also clicked!");
  }
  function displayDoc() {
    const div_id = 'show-doc' // show-doc is in _form. 'year_doc_id'
    const showDoc = document.getElementById(div_id);
    
    const onClickBtn =     document.createElement("button");
    onClickBtn.innerText = "onClick Button";
    
    const addListenerBtn = document.createElement("button");
    addListenerBtn.innerText = "addEventListner Button";
    
    showDoc.appendChild(onClickBtn);
    showDoc.appendChild(addListenerBtn);
    
    // With this commented out only the lower button works and both console logs print
    onClickBtn.onclick = clickMe;
    onClickBtn.onclick = clickMeAlso;
    
    // Only the `onClick` button works with this commented out. With this active, both lines are printed
    addListenerBtn.addEventListener("click", clickMe);
    addListenerBtn.addEventListener("click", clickMeAlso);
   };
  displayDoc();  // Didnt' see this in the example, but I added it.
  // <div id="show-doc">
 //  <button>onClick Button</button>
 //  <button>addEventListner Button</button></div>
// }