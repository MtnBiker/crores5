"use strict";

function makeMap() {

  L.mapbox.accessToken = 'pk.eyJ1IjoibXRuYmlrZXIiLCJhIjoiNmI5ZmZjMzAyNzJhY2Q0N2ZlN2E1ZTdkZjBiM2I1MTUifQ.6R3ptz9ejWpxcdZetLLRqg';
  // var myMap = L.mapbox.map('map', 'mtnbiker.d7jfhf8u').setView([34.040951, -118.258579], 13);
  // Was working on getting rid of MapBox, but not easy
  // var myMap = L.map('map', { center: [34.040951, -118.258579], zoom: 13 });
  var myMap = L.mapbox.map('map').setView([34.040951, -118.258579],  13);

  L.tileLayer('https://crores.s3.amazonaws.com/tiles/bkm/{z}/{x}/{y}.png').addTo(myMap);

  var pointLayer = L.mapbox.featureLayer().loadURL('map/point_data.geojson');

  var lineLayer  = L.mapbox.featureLayer().loadURL('map/line_data.geojson');

  var sliderLayer = L.layerGroup([pointLayer, lineLayer]);

  pointLayer.on('ready', function(e) {myMap.fitBounds(pointLayer.getBounds());});

  // var mySliderControl = $( "#map" ).slider({  // makeMap is not defined why is this?
 //    animate: "fast"
 //  });
 // var mySliderControl = L.control.sliderControl({position: "topright", layer: sliderLayer, range: true}); // still doesn't work
  var mySliderControl = L.control.sliderControl({
    range: false, // true is the default, Has to be false for follow to work
    follow: 5, // range must be false for follow to work.
    timeStrLength: 10, // doesn't seem to make any difference, works with default 19 also
    // layer: pointLayer // this option is consistently seen. Except
    // layer: lineLayer // OK but popup is date, not info needed
    layer: sliderLayer  // both don't work. Points all show and line is start or finish only
  });
// console.log("22. mySliderControl: ", mySliderControl);
  mySliderControl.addTo(myMap);

  // https://jsfiddle.net/ngeLm8c0/5/ and https://stackoverflow.com/questions/35878087/timeslider-plugin-and-leaflet-markers-not-appearing-in-order/35881206#35881206
// console.log("26. mySliderControl: ", mySliderControl);
  mySliderControl.startSlider();
}

// $.getJSON('data.geo.json', function (data) {
//     // Define the geojson layer and add it to the map
//     L.geoJson(data).addTo(map);
// });
