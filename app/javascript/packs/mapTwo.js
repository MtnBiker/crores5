// javascript/packs/mapTwo.js
// called from map/index.html.erb
// Leaflet

// https://stackoverflow.com/questions/39993676/code-inside-domcontentloaded-event-not-working. So don't appear to need the if-then-else, just the statement in the else will suffice
// if( document.readyState !== 'loading' ) {
//     console.log( 'mapTwo.js:10. document.readyState worked. addEventListener not needed');
//     makeMapTwo();
// } else {
console.log('Hello from /webpacker/packs/mapTwo.js:9');
// This works with 'normal' index.html.erb page set upFIXME turn this on and the one below off if don't go with max's method
// document.addEventListener('DOMContentLoaded', function () {
//   console.log(
//     'mapTwo.js:13. document.addEventListener(DOMContentLoaded worked). document.readyState was false',
//   );
makeMapTwo();
  // }); // document.addEventListener…

// https://stackoverflow.com/questions/39993676/code-inside-domcontentloaded-event-not-working and seems to work with normal page but throws an error
// document.addEventListener('DOMContentLoaded', function () {
//     // you could also use $('.personal-map').each or the less sucky ES6
//     // equivalent
//     console.log("mapTwo.js:20. addEventListener worked")
//     var maps = Array.prototype.map.call(
//       document.getElementsByClassName('map'), makeMapTwo()
//     );
//   });
// } // end if-then-else which isn't needed

function makeMapTwo() {
  console.log('Hello from /javascript/packs/mapTwo.js:31');
  // mapVar is local to this script. map_two is the div on the calling page
  // $(document).ready(function() { // makes no difference at the moment.
  var mapVar = L.map('map', {center: [34.040951, -118.258579], zoom: 13});
  // console.log('mapTwo.js:35. mapVar{} ', mapVar)
  L.tileLayer(
    'https://crores.s3.amazonaws.com/tiles/bkm/{z}/{x}/{y}.png',
  ).addTo(mapVar);

  $.getJSON('map/line_data.geojson', function (data_data) {
    // map/ removed as it again is mislinked. As it should be. But needed to put it back in 2020.10.04
    // $.getJSON("people/show_locations.geojson", function (data_data) { // for demoing show_locations.json.erb. Note below data_data is a placeholder. Trying to get person_id = show_locations
    // $.getJSON("people/locations/locations_data.geojson", function (data_data) { // temp try here

    var timelineData = L.timeline(data_data, {
      style: function (data_data) {
        return {
          stroke: true,
          fillOpacity: 0.5,
        };
      }, // end style: function(data_data)
      waitToUpdateMap: true,
      onEachFeature: function (data_data, layer) {
        layer.bindTooltip(data_data.properties.popup, {direction: 'top'});
        // console.log('36. CONSOLE LOG: data_data ', data_data)
      }, // onEachFeature:
    }); // end let timelineData = L.timeline

    var timelineControl = L.timelineSliderControl({
      enableKeyboardControls: true,
      steps: 100,
      start: 1879,
      end: 1928,
    });
    // console.log("46. CONSOLE LOG: timelineData: ", timelineData);
    timelineData.addTo(mapVar); // without this the line don't show
    // what are the line above and below doing?
    timelineControl.addTo(mapVar);
    timelineControl.addTimelines(timelineData);
    // console.log("51. CONSOLE LOG: timelineControl: ", timelineControl);
    // console.log("52. CONSOLE LOG: last line of $.getJSON. mapVar: ", mapVar);
  }); //  end $.getJSON
  // console.log(
  //   'mapTwo.js:74. Bottom of mapTwo.js (but inside $(document).ready) if not . mapVar: ',
  //   mapVar,
  // );
} //function makeMapTwo()

// }); // end $(document).ready(

//   data_data is a place holder. line_data doesn't matter in calling the geojson. Note that map_controller has a def line_data. But that is called by jbuilder.
