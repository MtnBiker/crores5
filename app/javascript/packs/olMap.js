// url: http://localhost:3000/olMap easier for development 
// webpacker/packs/olMap.js
// FIXME. Line 285. Timeline is not working. No lines show nor does it change with the timeline. But not sure how useful this page is anyway

//  Map and lines show, but slider not implemented
console.log('Hello from /webpacker/packs/olMap.js:11');
import "../stylesheets/olmap"; 
import Map from 'ol/Map';
import View from 'ol/View';
import { transform } from 'ol/proj';
import {transformExtent, fromLonLat} from 'ol/proj';
import LayerGroup from 'ol/layer/Group';
// import ImageLayer from 'ol/layer/Image';
import TileLayer from 'ol/layer/Tile';
import VectorLayer from 'ol/layer/Vector';
import VectorSource from 'ol/source/Vector';
import { OSM, XYZ } from 'ol/source';
import { Fill, Circle as CircleStyle, Stroke, Style, Text, Icon } from 'ol/style';
import layerSwitcher from 'ol-layerswitcher';
import Timeline from 'ol-ext/control/Timeline'; //  http://viglino.github.io/ol-ext/doc/doc-pages/ol.control.Timeline.html#toc0__anchor 
import interactSelect from 'ol/interaction/Select';
import GeoJSON from 'ol/format/GeoJSON'; // only needed for a short test
// import Loupe from 'ol-loupe'
// import 'ol-loupe/src/style.css' //Or your own

var mapExtent = [-118.5, 34.0, -118.2, 33.7];

// all the maps
import {baseLayers} from './modules/baseLayers.js';


// Two steps establishing layer so can reused vectorSource in timeline. 

// Just line date, but try line and point since want more info on rollover
// var vectorLineSource = new VectorSource({
//   url: 'map/line_data.geojson',
//   projection: 'EPSG:3857',
//   format: new GeoJSON(), // format: new ol.format.GeoJSON(),
//   attributions: [ " <a href='./about'>CroRes</a>" ]
// })
// var vectorLineLayer = new VectorLayer({
//   name: 'Where and when',
//   // style: style(), // the style was making circles and blobs for earthquake data. Do I need style? Yes but will do with LinePoint
//   source: vectorLineSource
// });

// Need both lines and point since point is the ends of the line
var vectorLinePointSource = new VectorSource({
  url: 'map/line_point_data.geojson',
  projection: 'EPSG:3857',
  format: new GeoJSON(), // format: new ol.format.GeoJSON(),
  attributions: [ " <a href='./about'>CroRes</a>" ]
})
var vectorLinePointLayer = new VectorLayer({
  name: 'Where and when',
  style: style(),
  source: vectorLinePointSource
});


var map = new Map({
    target: 'map',
    layers: [ baseLayers, vectorLinePointLayer ],
    view: new View({
      center: fromLonLat([-118.258579, 34.040951]),
      zoom: 14,
      maxZoom: 20,
      // extent: transformExtent(mapExtent, 'EPSG:4326', 'EPSG:3857'), // define mapExtent better or understand how this works. Or maybe my data is extending beyond the extent
      constrainOnlyCenter: true
    }) // end view: new View({
}); // end var map

// The following needs base and overlay
var varLayerSwitcher = new layerSwitcher({
    tipLabel: 'Legend', // Optional label for button and isn't seen
    groupSelectStyle: 'children' // Can be 'children' [default], 'group' or 'none'
});
map.addControl(varLayerSwitcher);
// map.addControl(new Loupe()); // problems with 'import'ing

// Create Timeline control from http://viglino.github.io/ol-ext/examples/storymap/map.control.timequake.html  'ol-ext/control/Timeline'
// This just seems to put the Timeline on and has little to do with how the data is displayed
var tline = new Timeline({
  className: 'ol-pointer',
  features: [{ // Features to show in the timeline
    text: 1900, // Showing up between 1899 and 1900 marker? The example seems wrong too on the map.  Was: text: 2015
    date: new Date('1900/01/01'), // date: new Date('2015/01/01'),
    endDate: new Date('1900/12/31') // endDate: new Date('2015/12/31')
  }],
  graduation: 'month', // This is for the scale. 'month' Was 'day' Default is year month and day for my spread out scale come out the same. Helps to have months; confirms that the marker is for the beginning of the year
  minDate: new Date('1895/01/01'), // minDate: new Date('2014/06/01'),
  maxDate: new Date('1904/12/31'), // maxDate: new Date('2016/02/01'),
  getHTML:        function(f){ return 1900; }, // getHTML: function(f){ return 2015; }, // a function that takes a feature and returns the html to display
  getFeatureDate: function(f){ return f.date; },  // a function that takes a feature and returns its date, default the date property
  endFeatureDate: function(f){ return f.endDate; }  // a function that takes a feature and returns its end date, default no end date    
}); // end var tline
// map.addControl (tline); //FIXME adding the timeline causes the lines to disappear or more properly, the timeline isn't working


// Set the date when ready
setTimeout(function(){ tline.setDate('1900'); }); // setTimeout(function(){ tline.setDate('2015'); });
tline.addButton ({
  className: "go",
  title: "GO!",
  handleClick: function() {
    go(); // I think this starts the time from left to right
  }
}); // end tline.addButton ({

// http://viglino.github.io/ol-ext/examples/storymap/map.control.timequake.html
// Style function
// to make circles and blobs which I don't need
//  appears to be called from select, but the points are styled before ever being selected
var cache = {};
// function style(select){
//   // Following for development to get a style in scope
//   var blueLine =  new Style({
//             stroke: new Stroke({
//              color: [0, 153, 10, 1],
//              width: 3
//            })
//   });
//
//   return function(f) {
//     var style = cache[f.get('mid_date')+'-'+select];
//     // console.log('305. style: ', style);
//     if (!style) {
//     var str1 = new Stroke({
//       color: [0, 153, 10, 1],
//       width: 3
//     });
//     var str2 = new Stroke({
//       color: [153, 10, 10, 1],
//       width: 2
//     });
//     style = cache[f.get('mid_date')+'-'+select] =
//       [
//         new Style({ stroke: str1 }),
//         new Style({ stroke: str2 })
//       ];
//     }
//
//     // var style = cache[f.get('mag')+'-'+select];
//  //    if (!style) {
//  //      var img = new CircleStyle({
//  //        radius: f.get('mag')*f.get('mag')/2,
//  //        fill: new Fill({
//  //          color: select ? 'rgba(255,0,0,.5)':'rgba(255,128,0,.3)'
//  //        })
//  //      });
//  //      var img2 = new CircleStyle({
//  //        radius: f.get('mag')*f.get('mag')/4,
//  //        fill: new Fill({
//  //          color: select ? 'rgba(255,0,0,.5)':'rgba(255,128,0,.3)'
//  //        })
//  //      });
//       // style = cache[f.get('mag')+'-'+select] =
//       // [
//       //   new Style({ image: img }),
//       //   new Style({ image: img2 })
//       // ];
// //    }
//       console.log('341. style: ', style);
//       // console.log('341. style: ', blueLine);
//       return style;
//     }
//   };
  // for development
var greenLine =  new Style({
          stroke: new Stroke({
           color: [0, 153, 10, 1],
           width: 3
         })
});
// console.log('333. style: ', style); // seems to read like the definition
// console.log('334. style(true): ', style(true)); // same as above
// console.log('335. style(false): ', style(false)); // same as above
// console.log('334. cache: ', cache); // not of much interest ?
    
//Trying a second style function where color is based on mid_date, 
function style(select){
  return function(f) {
    var style = cache[f.get('mid_date')+'-'+select];
    // console.log('22. style: ', style);
    // console.log('23. f.get(`mag`): ', f.get('mag'));
    if (!style) {
    var str1 = new Stroke({
      color: [0, 153, 10, 1],
      width: 3
    });    
    var str2 = new Stroke({
      color: [153, 10, 10, 1],
      width: 2
    });
    
    if (f.get('mid_date')<-2030198400) {
      style = new Style({ stroke: str1 });
    } else {
      style =  new Style({ stroke: str2 });
    }
   
    // console.log('41. style: ', style);
    return style;
  }
}};
// end development style function for two colors

// Show features on scroll. Only these ~20 lines are needed for manual scroll to work. 
tline.on('scroll', function(e){
  var d = tline.roundDate(e.date, 'day')
  $('.dateStart').text(d.toLocaleDateString(undefined, {year: 'numeric', month: 'short', day: 'numeric'}));
  // Filter features visibility
  // vectorLineSource.getFeatures().forEach(function(f) { // vectorLineSource is line_data.geojson
  vectorLinePointSource.getFeatures().forEach(function(f) {
    var dt = f.get('mid_date') - e.date;  // var dt = f.get('time') - e.date; //  e is the pointer
    // console.log('341. dt: ', dt,  '. e.date: ', e.date); // needed mid_date in unix time and got it. dt can be negative 2196153129600 – ". e.date: " – Tue May 01 1900 00:35:31 GMT-0700 (PDT) // 'Math.abs(dt): ', Math.abs(dt),
    // console.log('342. f: ', f); // is the line item and all it's gory details
    // if (Math.abs(dt) > 1000*3600*12) { // Original. approx 1.37 years, but why the funny aritmetic: He's calculating 1000 half days, i.e, 500 days
    if (Math.abs(dt) > 1000*3600*12) {
    f.setStyle([]); // commented out lines show up in default style
    // f.setStyle(greenLine);
    } else {
      f.setStyle(); // This default style
    }
  }); // vectorLineSource.getFeatures()
}); // tline.on('scroll', function(e){

// The following ~15 lines are for When click on something on the map, other information shows up on the right where the date is shown (see earthquake example). NOT IMPLEMENTED YET
var select = new interactSelect({ hitTolerance: 5, style: style(true) });
map.addInteraction(select);
// console.log('356. Hello from map3.js. map: ', map)

select.on('select', function(e){
  var f = e.selected[0];
  if (f) {
    // Show info
		var info = $("#select").html("");
    // $("<p>").text((new Date(f.get("time"))).toLocaleString()).appendTo(info); // original
    // Some placeholder in case it starts working
		$("<p>").text('Name: '+f.get("mid_date")+' - '+f.get("resid_address")).appendTo(info);
		$("<p>").text('Position: '+f.get("title_resto")+' at '+f.get("resto_address")).appendTo(info);
	} else {
    $("#select").html('');
  }
}); 
  // end select.on('select', function(e){
// end select that shows more info about whatever click on the map

// Run on the timeline. Is this what happens after clicking on the run triangle?
var running = false;  // running is a boolean, but when printed appears to be an integer that increases approx. every second
var start = new Date('1880'); // var start = new Date('2015');
var end   = new Date('1920'); // var end = new Date('2016');
// console.log('olMap.js:396. start: ', start, '. end: ', end, '. tline.getDate()', tline.getDate()) // tline.getDate() is before start, maybe left edge of timeline

function go(next) { // called from 332 and 403
  var date = tline.getDate();
  // console.log('olMap.js:395. running: ', running, ' next: ', next, 'date: ', date, '. start: ', start, '. end', end,  '. a progress indicator')
  if (running) clearTimeout(running);
  if (!next) {
    // stop
    if (date>start && date<end && running) {
      running = false;
      tline.element.classList.remove('running');
      return;
    }
    if (date > end) {
      date = start;
    }
  }
  if (date > end) {
    tline.element.classList.remove('running');
    return;
  }
  if (date < start) {
    date = start;
  }
  // 1 day
  date = new Date(date.getTime() + 24*60*60*1000);
  tline.setDate(date, { anim:false }); // Center timeline on a date anim is animate
  // next
  tline.element.classList.add('running');
  // console.log('376. Hello from map3.js. tline: ', tline)
  running = setTimeout(function() { go(true); }, 100);
} // end function go(next)
