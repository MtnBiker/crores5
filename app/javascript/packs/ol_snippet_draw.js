//  See _original without deletions crores5/zMiscellaneous/ol_snippet_draw_without_deletions.js.
// Moved file because afraid it might be loaded and confuse things. Still not working righ
console.log('ol_snippet_draw.js:1 is the js being used')
import 'ol/ol.css';
import Map from 'ol/Map';
import View from 'ol/View';
import Polygon from 'ol/geom/Polygon';
import Draw, {createRegularPolygon, createBox} from 'ol/interaction/Draw';
import {Tile as TileLayer, Vector as VectorLayer} from 'ol/layer';
import ImageLayer from 'ol/layer/Image';
import {OSM, Vector as VectorSource} from 'ol/source';
import Static from 'ol/source/ImageStatic';
import Projection from 'ol/proj/Projection';
import {getCenter} from 'ol/extent';
//  This may be part of my having to reload problem https://guides.rubyonrails.org/working_with_javascript_in_rails.html#rails-ujs-event-handlers
// document.addEventListener("turbolinks:load", () => {
//   alert("page has loaded!");
// });
// document.addEventListener("turbolinks:load") = init; // swag Didn't work substituting for the following
// window.onload = init(); // function called after window loaded. Needed to have the html document defined and available
// document.addEventListener("turbolinks:load", () => { // swag number 2. Didn't work substituting for the following
function init() {
  var map_div = 'map' // because occurs in two places in this script and also on show.html.erb. BUT ONLY WORKS IF #map is used, not map_div
  var map_div_js = '#' + map_div // for line 40, only have to change the above
  console.log('ol_snippet_draw.js:25 map_div: ', map_div, '. And must agree with the id= on show. map_div_js: ', map_div_js)

  var extent = []
  // vector will be for the draw layer
  var source = new VectorSource(); // used more than once
  var drawLayer = new VectorLayer({
    source: source
  });

  var docURL = $(map_div_js).data('url');
  console.log('ol_snippet_draw.js:53. docURL: ', docURL)
  var docWidth  = $(map_div_js).data('width');
  var docHeight = $(map_div_js).data('height');
  extent = [0, 0, docWidth, docHeight];
  console.log('ol_snippet_draw.js:68. extent (image size): ', extent, '. from map_div_js: ', map_div_js) // , '. snippetExtent: ', snippetExtent
  var projection = new Projection({
    // code: '',
    units: 'pixels',
    extent: extent,
  });

  var docSource = new Static({
    attributions: '',
    url: docURL,
    projection: projection,
    imageExtent: extent
  });

  var docLayer = new ImageLayer({ // var raster in example for draw
    source: docSource,
    extent: extent,
  });

  var staticView = new View({
    projection: projection,
    center: getCenter(extent),
    zoom: 2.5, // zoom needs to be defined or the image doesn't show up. 3 seems to fill the div
    maxZoom: 4, // Can go beyond this with scroll wheel but doesn't get indicated
  });

  var map = new Map({
    // layers: [raster, vector],
    layers: [docLayer, drawLayer],
    target: map_div, // doesn't work with doc-draw or anything other than map. map_div defined at top
    view: staticView,
    extent: extent,
   });

  function addInteraction() {
    var draw = new Draw({
      source: source,
      type: 'Circle',
      geometryFunction: createBox()
    });
    console.log('ol_snippet_draw.js:122. draw: ', draw)
    map.addInteraction(draw);

    // https://stackoverflow.com/questions/30504313/how-to-get-coordinates-of-a-user-drawn-circle-in-openlayers-3 not sure coordinates are correct, but is returning a value
    draw.on('drawend', function(event) {
       // coords https://stackoverflow.com/questions/47351171/how-to-convert-openlayers-polygon-coordinates-into-latitude-and-longitude
       var boxCoords = event.feature.getGeometry().getCoordinates();
       console.log("ol_snippet_draw.js:127. boxCoords: ", boxCoords);
       var boxCoordsStringified = JSON.stringify(boxCoords);
       console.log("ol_snippet_draw.js:129. boxCoordsStringified: ", boxCoordsStringified);
       var boxCoordsStringifiedParsed = JSON.parse(boxCoordsStringified);
       console.log("ol_snippet_draw.js:131. boxCoordsStringified Parsed: ", boxCoordsStringifiedParsed);
       document.getElementById('year_snippet_coords').innerHTML = boxCoordsStringified;
       // document.getElementById('year_snippet_extent').innerHTML = String(boxCoords); // same just a list of coords
       console.log('ol_snippet_draw.js:164. Got to here in draw.on in addInteraction() !')
     });
   } // end function addInteraction
   console.log('ol_snippet_draw.js:167. Got to here, just out of draw.on !')
  addInteraction();

  // Show zoom level. https://gis.stackexchange.com/questions/62926/openlayers-possible-to-show-zoom-level
  map.on("moveend", function() {
      var zoom = map.getView().getZoom(); 
      var zoomInfo = '<strong>Zoom level:</strong> ' + zoom.toPrecision(3); // significant figures which is easy here since always between 1 and ~24
      document.getElementById('zoomlevel').innerHTML = zoomInfo;
  });
 
} // end function init()

// window.onload = init(); // this works better with the () although that's the opposite of what I thought
// document.addEventListener('DOMContentLoaded', init()); // Farkas. This worked better for some things, but trying below again for. 2021.08.23 Doesn't show doc
document.addEventListener('DOMContentLoaded', init); // 2021.08.20 works
