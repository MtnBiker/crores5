/* eslint no-console:0 */

// This file is the Webpacker entry point app/javascript/packs/application.js
// But should it now be at a higher level?. Couldn't make work at app/javascript. E.g., first four imports didn't work

// This file is automatically compiled by Webpack, along with any other files
// present in this directory. You're encouraged to place your actual application logic in
// a relevant structure within app/webpack and only use these pack files to reference
// that code so it'll be compiled.

// To reference this file, add < = javascript_pack_tag 'application' %> to the appropriate
// layout file, like app/views/layouts/application.html.erb

// Uncomment to copy all static images under ../images to the output folder and reference
// them with the image_pack_tag helper in views (e.g < = image_pack_tag 'rails.png' %>)
// or the `imagePath` JavaScript helper below.

// Haven't put (all?) images under Webpacker control and probably shouldn't bother. I think it may be tedious.
// const images = require.context('../images', true, /\.(gif|jpg|png|svg|eot|ttf|woff|woff2)$/i)
// const imagePath = (name) => images(name, true)

import 'core-js/stable';
import 'regenerator-runtime/runtime';
import '../stylesheets/application.scss'; // importing stylesheets listed in application.scss.  https://github.com/rails/webpacker/issues/2071 hard to believe that this is my problem. But it seemed to. Probably don't need the .scss // Doesn't load other stylesheets though, load those on individual/specific .js pages

// If using webpacker for images which I'm not
// import '../images/cat.jpg' // we could do this for each image. And we store our cat.jpg image at app/assets[or webpack]/images/cat.jpg. And we use with `image_pack_tag 'cat.jpg'` 
// require.context("../images", true); // or this to import all the images at once

import Rails from "@rails/ujs"
// import * as Turbo from "@hotwired/turbo" // https://turbo.hotwired.dev/handbook/installing Aug 2021
// import { Turbo, cable } from "@hotwired/turbo-rails" // https://medium.com/pragmatic-programmers/a-brief-hello-to-hotwire-ea38c1258564. I don't need cable, but what about the difference in importing Turbo

// import { Turbo } from '@hotwired/turbo-rails'; //  `yarn add @hotwired/turbo-rails` and `gem 'turbo-rails'` both needed? FIXME
import "@hotwired/turbo-rails"// FIXME two imports of this in part. See ~5 lines above
// import "@hotwired/turbo" // Caution. Many pitfalls https://evilmartians.com/chronicles/hotwire-reactive-rails-with-no-javascript

// Next 6 lines https://stimulus.hotwired.dev/handbook/installing
import { Application } from "stimulus" // @stimulus is in node-modules, and separate from @hotwired
import { definitionsFromContext } from "stimulus/webpack-helpers"

const application = Application.start()
// const context = require.context("./controllers", true, /\.js$/) // default, so in slightly different place, see below
const context = require.context("../controllers", true, /\.js$/)
application.load(definitionsFromContext(context))

import * as ActiveStorage from "@rails/activestorage"
// import "@rails/activestorage" // which is it? This one is copied from newer install

// Turbo.start(); // one demo had this before Rail.start
Rails.start()
ActiveStorage.start()

import 'channels' // don't have any, but fixed my page load problem
import 'controllers'

import 'trix'
import '@rails/actiontext'

// Aliases $ to jQuery https://github.com/webpack/webpack/issues/4258, Guess this is the best file for this since it's the entry point. Needed for some of my Leaflet stuff and maybe Bootstrap. Errors without. GoRails showed putting in environment.js but got errors without it here. Seems to work OK commented out now 2020.01.18
// See bootstrappy. This should be redundant--no I removed it from environment.js, but then I got errors and if I remove it here the current /map page doesn't load
// Needed for OpenLayers map in people>show, but config/webpack/environment.js seems to do the trick. The map loads if used be
// Not needed by AreYouSure, environment.js must be working!!
// import $ from 'jquery'     // commented out to see if similar in environment.js handled it as they are global settings
// window.jQuery = $          // commented out to see if similar in environment.js handled it as they are global settings
// window.$      = $          // commented out to see if similar in environment.js handled it as they are global settings

// import "../src/doc_select_event"; // we import a file from outside `packs` with `..` https://www.ombulabs.com/blog/learning/webpacker/webpack-all-the-assets.html Except this is not longer being used?
// import "../src/demoEventListener"; // not being used
// import "../src/ol_snippet_display"; // called via javascript_pack_tag
// import "../src/ol_snippet_draw"; // called via javascript_pack_tag


// Three lines for leaflet latest. But trying CDN for v0.7.7
import 'leaflet'; // not needed if ProvidePlugin has this in environment.js. This may be redundant with two lines down.
import 'leaflet.timeline'; // put in mapTwo.js since steps on leaflet FIXME breaking ActionText and not working because of error in loading. Asks for `.Timeline.ts` but there is a file named `Timeline.d.js` in
//  https://github.com/PaulLeCam/react-leaflet/issues/255
import L from 'leaflet';

import 'leaflet/dist/leaflet.css'; // why here and not .scss? But it does exist

// Two below were a quick and dirty trying to work around leaflet.timeline via Node broken, but got other errors
// import '../src/Timeline.js' // since leaflet.timeline via Node is broken, but error on loading
// import '../src/TimelineSliderControl.js' // no error, but need Timeline also?
// Trying with .ts, but this error: You may need an appropriate loader to handle this file type, currently no loaders are configured to process. Removed the files, but t
// import '../src/Timeline.ts' // since leaflet.timeline via Node is broken, but error
// import '../src/TimelineSliderControl.ts'

// stupid hack so that leaflet's images work after going through webpack
import marker from 'leaflet/dist/images/marker-icon.png';
import marker2x from 'leaflet/dist/images/marker-icon-2x.png';
import markerShadow from 'leaflet/dist/images/marker-shadow.png';

delete L.Icon.Default.prototype._getIconUrl;

L.Icon.Default.mergeOptions({
  iconRetinaUrl: marker2x,
  iconUrl: marker,
  shadowUrl: markerShadow,
});

import 'jquery';
// include jQuery in global and window scope (so you can access it globally)
// in your web browser, when you type $('.div'), it is actually refering to global.$('.div')
// https://rubyyagi.com/how-to-use-bootstrap-and-jquery-in-rails-6-with-webpacker/
// But was working without this 2020.10.01, so leaving commented out.
// global.$ = global.jQuery = jQuery; // not needed by AreYouSure ?
// window.$ = window.jQuery = jQuery; // Needed by AreYouSure which did get rid of one error

// import('src/plugins') // loads async. May not be needed https://rossta.net/blog/webpacker-with-bootstrap.html
// require("../packs/leafletPersonMap") // url seems circular-out and back in but it works https://stackoverflow.com/questions/60610788/rails-connecting-to-jbuilder see last @max comment

// require("../stylesheets/application.scss") // https://www.vic-l.com/setup-bootstrap-in-rails-6-with-webpacker-for-development-and-production didn't seem to change anything
// For Bootstrap 4 from GoRails. css is handled in /stylesheets/application.scss.
import 'bootstrap' // or require("bootstrap"); use import for yarn?
// import 'bootstrap/dist/js/bootstrap' // not always done and is breaking Bootstrap 5
import 'ol'
// import 'ol-ext'              // not sure this is necessary, didn't help with error above
// import 'ol-layerswitcher'    // not sure this is necessary, didn't help with error above
// import 'ol-loupe'            // not sure this is necessary, didn't help with error above
// import 'ol-popup' // https://github.com/walkermatt/ol-popup Doesn't seem to be necessary. All packs loaded ?

// https://medium.com/swlh/integrate-bootstrap-4-and-font-awesome-5-in-rails-6-fec52ee51753 Also in aplication.scss
import '@fortawesome/fontawesome-free/js/all'
// import '@mapbox/maki'; // SVG fonts has resto. Not using
// import '@datafire/thenounproject' // SVG icons. API keys needed. Getting an error

// Got en error here with jQuery and ol via CDN TODO
// document.addEventListener("turbolinks:load", () => {
//   $('[data-toggle="tooltip"]').tooltip()
//   $('[data-toggle="popover"]').popover()
// })

// console.log('Hello World from Webpacker or more precisely application.js') // Used to work, but 2020.05.11 an error

//For Ransack. Didn't work, but this is a converted CoffeeScript, so maybe not right
// What is this for FIXME remove if not needed
(function () {
  $(function () {
    return $('#q_reset').click(function () {
      return $('.search-field').val('');
    });
  });
}.call(this));

// More complicated than this, because requiring turbolinks
// **import** is for node/npmjs/yarn
// **require** is for my additions, i.e., files in my app
