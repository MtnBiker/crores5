// javascript/packs/olPersonMap.js
// Can load with  = javascript_pack_tag 'olPersonMap' %> in ol.html.erb or example
// This is to be a map showing where a person lived and worked with color coding for years
//  from layerswitcher/examples/layerswitcher (via Walker Matt . Original copy in /Users/gscar/Documents/GitHub/ol-layerswitcher/examples/)
console.log('olPersonMap.js is the JavaScript for the map');
// https://openlayers.org/en/latest/apidoc/module-ol_style_Icon-Icon.html and http://openlayersbook.github.io/ch06-styling-vector-layers/example-03.html

// import 'ol/ol.css'; // Bringing in globally in application.scss
// ol now seems to have established names, these may not all be the same as the latest, but they are not consistent VectorLayer , but LayerGroup
import Map from 'ol/Map';
import View from 'ol/View';
import GeoJSON from 'ol/format/GeoJSON';
import VectorSource from     'ol/source/Vector';
import VectorTileSource from 'ol/source/VectorTile';
import { OSM, XYZ } from     'ol/source';
import { Fill, Stroke, Style, Text, Icon } from 'ol/style';
// https://openlayers.org/en/latest/apidoc/module-ol_style_Icon-Icon.html and http://openlayersbook.github.io/ch06-styling-vector-layers/example-03.html
import { transform } from 'ol/proj';
import LayerGroup from       'ol/layer/Group';
import VectorLayer from      'ol/layer/Vector';
import VectorTileLayer from  'ol/layer/VectorTile';
// // import VectorImageLayer from 'ol/layer/VectorImage';  // not using 2020.05.03
import TileLayer from        'ol/layer/Tile';
import LayerSwitcher from 'ol-layerswitcher'; // https://github.com/walkermatt/ol-layerswitcher
import { OverviewMap, ScaleLine, ZoomSlider, Zoom, ZoomToExtent } from 'ol/control';
import { transformExtent, fromLonLat } from 'ol/proj';
import Overlay from 'ol/Overlay'; // for popup
// import Loupe from 'ol-loupe' // hyphen is per ol-loupe Readme
// import 'ol-loupe/src/style.css' // // added src wasn't in ReadMe. Or your own

// window.onload = init; // don't seem to need this with channels activated
// function init(){
    
  // https://openlayers.org/en/latest/examples/osm-vector-tiles.html next ~48 lines
  var keyNextzen = 'Mc79jq4TTZ6ZZEB0q7A34A'; // Nextzen API key from https://developers.nextzen.org/
  var roadStyleCache = {};
  var roadColor = {
    'major_road': '#776',
    'minor_road': '#ccb',
    'highway': '#f39'
  };
  var buildingStyle = new Style({
    fill: new Fill({
      color: '#666',
      opacity: 0.4
    }),
    stroke: new Stroke({
      color: '#444',
      width: 1
    })
  });
  var waterStyle = new Style({
    fill: new Fill({
      color: '#9db9e8'
    })
  });
  var roadStyle = function(feature) {
    var kind = feature.get('kind');
    var railway = feature.get('railway');
    var sort_key = feature.get('sort_key');
    var styleKey = kind + '/' + railway + '/' + sort_key;
    var style = roadStyleCache[styleKey];
    if (!style) {
      var color, width;
      if (railway) {
        color = '#7de';
        width = 1;
      } else {
        color = roadColor[kind];
        width = kind == 'highway' ? 1.5 : 1;
      }
      style = new Style({
        stroke: new Stroke({
          color: color,
          width: width
        }),
        zIndex: sort_key
      });
      roadStyleCache[styleKey] = style;
    }
    return style;
  };  // roadStyle

  // var restoMakiIcon = new Style({
  //   // Maki is SVG, but so is FontAwesome so try same
  //   text: "maki2-restaurant",
  //   font: "fontmaki2",
  //   code: 59511,
  //   name: "restaurant",
  // })

  // https://stackoverflow.com/questions/27809479/use-semantic-ui-or-font-awesome-icons-as-markers-in-openlayers3/27932615#27932615 
  // Font Awesome icons only work in Chrome, not Safari!!! FIXME
  // This works for Home. Using for development
  var homeIcon = new Style({
      text: new Text({
          text: '\uf015',                         // fa-home, unicode f015
          font: '900 18px "Font Awesome 5 Free"', // font weight must be 900
      })
  });
  // Householder
  var homeHouseholderIcon = new Style({
    text: new Text({
      text: '\uf015',                         // fa-home, unicode f015
      font: '900 18px "Font Awesome 5 Free"', // font weight must be 900
      fill: new Fill({ color: 'darkgreen'}),
      offsetX: 2,
      offsetY: 2,
    })
  });
  // Resident
  var homeResidIcon = new Style({
    text: new Text({
        text: '\uf015',                         // fa-home, unicode f015
        font: '900 15px "Font Awesome 5 Free"', // font weight must be 900
        fill: new Fill({ color: 'green'}),
      })
  });

  // Stand-in for resto
  var coffeeIconFA = new Style({
  // var restoIcon = new Style({
      text: new Text({
          text: '\uf0f4',                         // fa-coffee Unicode: f0f4
          font: '900 18px "Font Awesome 5 Free"', // font weight must be 900
      })
  });
  // fa-university  · Unicode: f19c also fa-institution
  // fa-map-marker  · Unicode: f04
  var restoIcon = new Style({
  // var storeIconFA = new Style({
      text: new Text({
          text: '\uf54f',                         // f54f fa-store-alt f54e store
          font: '900 18px "Font Awesome 5 Free"', // font weight must be 900
      })
  });
  
  // noun project svg FIXME, if try to use fails
  var restoPropIcon = new Icon({
    src: '/images/Restaurant_Icon.png',
    imgSize: [20, 20], // optional, but seems like I neede it
  });
  
  // another swag at thenounproject. Didn't add API keys and not sure of syntax https://www.npmjs.com/package/@datafire/thenounproject and https://github.com/TailorBrands/noun-project-api
  // var restoNounIcon =
 //  thenounproject.getIconByTerm({
 //    "term": "restaurant"
 //    },
 //    context)
 //  console.log('153. restoNounIcon: ', restoNounIcon)
  
  // Proprietor
  var restoPropIcon = new Style({
  // var storeIconFA = new Style({
      text: new Text({
        text: '\uf54f',                         // f54f fa-store-alt // fa-black-tie [&#xf27e;]
        // text: 'fas fa-store-alt', // just puts "fas fa-store-alt" as the icon, i.e., text literally
        // text: <%= icon("shop") %>, // error, but suggests look again at using icon instead of text except this appears to be an SVG from Bootstrap. Treated as an image or text?
        font: '900 18px "Font Awesome 5 Free"', // font weight must be 900 with FA free
        fill: new Fill({ color: 'red'}),
        offsetX: -4, // up and to the left
        offsetY: -4,
      })
  });
  
  // Co-proprietor
  var restoCoPropIcon = new Style({ // was storeIconFA
      text: new Text({
          text: '\uf54f',                         // f54f fa-store-alt
          font: '900 16px "Font Awesome 5 Free"', // font weight must be 900
          fill: new Fill({ color: [174,35,24,80]}),
        // no offsets
      })
  });
  // Cook
  var restoCookIcon = new Style({ // was storeIconFA
      text: new Text({
          text: '\uf54f',                         // f54f fa-store-alt
          font: '900 12px "Font Awesome 5 Free"', // font weight must be 900
          fill: new Fill({ color: 'green'}),
          offsetX: 2, // down and right
          offsetY: 2,
      })
  });
  // Cashier
  var restoCashierIcon = new Style({ // was storeIconFA
      text: new Text({
          text: '\uf54f',                         // f54f fa-store-alt
          font: '900 10px "Font Awesome 5 Free"', // font weight must be 900
          fill: new Fill({ color: 'blue'}),
          offsetX: 4, // further down and right
          offsetY: 4,
      })
  });
  // Unknown Position
  var unknownPositionIcon = new Style({
      text: new Text({
          text: '\uf54f',                         // f54f fa-store-alt // fa-question f128
          font: '900 18px "Font Awesome 5 Free"', // font weight must be 900
          fill: new Fill({ color: 'blue'}),
          offsetX: 7, // further   right
          offsetY: 0,
      })
  }); //unknownPositionIcon

  var iconType
  // map the position title codes to a colour value, grouping them (don't like the naming but need a good differentition between the name of this var, the result returned (singular) and the final result which will be title)
  // json.positionTitle  year.title Not being used, but maybe it could be rather then the if then else
  var iconMap = {
   'Proprietor': restoIcon,
   'Owner Restaurant' : restoIcon, // refine these when got it working.
   'Co-proprietor': restoIcon    , // refine these when got it working.
   'Cook': restoIcon             , // refine these when got it working.
   'Waiter': restoIcon           , // refine these when got it working.
   'Clerk': restoIcon            , // refine these when got it working.
   'Cashier':restoIcon           , // refine these when got it working.
   'Householder': homeIcon       , // refine these when got it working.
   'Owner Resident': homeIcon    , // refine these when got it working.
   'Resident': homeIcon          , // refine these when got it working.
   'Position Unknown' : homeIcon
  };
  // console.log("iconMap: ", iconMap) // how use this?
  // var iconMapString = JSON.stringify(iconMap);
  // window.alert(JSON.parse(iconMapString).Cashier);
  // window.alert(iconMap);
  // window.alert("Hi");
  // const map1 = new Map();
  // map1.set('bar', 'foo');
  //
  // console.log(map1.get('bar'));

  // a default style is good practice! TODO change to a dot or similar. Doesn't work in place of the Style/Text/FontAwesome text icons
  var defaultStyle = new Icon({
   src: './images/home.png',
   opacity: 0.5,
   scale: 1.0,
   size: 30,
  }); // defaultStyle for Icon

  // using above as a guide
  var lineStyle = function(feature) {
    var name = feature.get('name');
    var place = feature.get('place');
    var sort_key = feature.get('sort_key');
    var styleKey = kind + '/' + railway + '/' + sort_key;
    var style = roadStyleCache[styleKey];
    if (!style) {
      var color, width;
      if (railway) {
        color = '#7de';
        width = 1;
      } else {
        color = roadColor[kind];
        width = kind == 'highway' ? 1.5 : 1;
      }
      style = new Style({
        stroke: new Stroke({
          color: color,
          width: width
        }),
        zIndex: sort_key
      });
      roadStyleCache[styleKey] = style;
    }
    return style;
  };  // roadStyle

  var restoLineStyle = new Style({
    stroke: new Stroke({
        color: '#f00',
        width: 2
      }) // Stroke
  }); // Style

  // Generate a rainbow gradient https://openlayers.org/en/latest/examples/canvas-gradient-pattern.html
  var gradient = (function () {
    var grad = context.createLinearGradient(0, 0, 512 * pixelRatio, 0);
    grad.addColorStop(0, 'red');
    grad.addColorStop(1 / 6, 'orange');
    grad.addColorStop(2 / 6, 'yellow');
    grad.addColorStop(3 / 6, 'green');
    grad.addColorStop(4 / 6, 'aqua');
    grad.addColorStop(5 / 6, 'blue');
    grad.addColorStop(1, 'purple');
    return grad;
  });

  var residLineStyle = new Style({ // may need work. Did to get working
    fill: gradient, // gradient ignored. new Fill(gradient ) ignored. setGradient(gradient ) is maybe for an element
    stroke: new Stroke({
        color: 'darkgreen',
        width: 2
      }) // Stroke
  }); // Style

  var unknownLineStyle = new Style({ // may need work. Did to get working
    stroke: new Stroke({
        color: 'gray',
        width: 2
      }) // Stroke
  }); // Style


  // all the maps
  import {baseLayers} from './modules/baseLayers.js';


  // import the json for the locations and the lines connecting them
  // https://stackoverflow.com/questions/60610788/rails-connecting-to-jbuilder
  var urlLocJSON = $('#map').data('locations') + '.json'; // /people/124/locations.json and when called below builds the json I think. Points for the individual locations
  // window.alert('olPersonMap:85. urlLocJSON: ' + urlLocJSON) // olPersonMap:36. urlLocJSON: /people/108/locations.json
  // var positionTitle = getText().setText(feature.get('positionTitle'));
  // window.alert(positionTitle);

  var urlLineLocJSON = $('#map').data('line') + '.json';
  // console.log('olPersonMap:40. urlLineLocJSON: ' + urlLineLocJSON) // /people/127/lines.json

  // var mapExtent = [-118.5, 34.0, -118.2, 33.7]; // need to change numbers if plan to use

  // Set up the layers: base (maps) and dataOverlays (people specific info)
  // One LayerGroup for base layers. Listed in reverse order
  //  See Ikrom Nishanbaev tutorial for a cleaner way to do this FIXME

  // console.log('olPersonMap.js:253. person-details: ', $('#person-details')),
  // a var for all the Overlay layers

  // console.log("iconType: ", iconType);
  // console.log("homeIcon: ", homeIcon);
  var dataOverlays = new LayerGroup({
    // A layer must have a title to appear in the layerswitcher
    title: 'Where lived and worked',
    // Adding a 'fold' property set to either 'open' or 'close' makes the group layer
    // collapsible
    // fold: 'open',
    layers: [
      // new VectorImageLayer({ // VectorImage for faster rendering during interaction and animations, at the cost of less accurate rendering. So either this or the following works. They seem to have the same methods https://openlayers.org/en/latest/apidoc/module-ol_layer_Vector-VectorLayer.html
      // But can I use different icons?

     // Mixing the two
      new VectorLayer({
        title: 'Locations as points',
        imageRatio: 2,
        source: new VectorSource({
          url: urlLocJSON, // from line 28
          format: new GeoJSON() // works with or without the comma
        }), // source
        // style: homeHouseholderIcon, // for development testing
        // style: defaultStyle, // no go
        style: function(feature, resolution) { // should be style, no error if icon FIXME Nishanbaev may have dealt with this situation better
          // maybe could make this smarter
          // console.log("positionTitle: ", feature.get('positionTitle'))
          if(feature.get('positionTitle') === 'Proprietor') {
           return restoPropIcon;
         } else if(feature.get('positionTitle') === 'Co-proprietor') {
           return restoCoPropIcon;
         } else if(feature.get('positionTitle') === 'Cook') {
           return restoCookIcon;
         } else if(feature.get('positionTitle') === 'Cashier') {
           return restoCashierIcon;
         } else if(feature.get('positionTitle') === 'Resident') {
           return homeResidIcon;
         } else if(feature.get('positionTitle') === 'Householder') {
           return homeHouseholderIcon;
         } else if(feature.get('positionTitle') === 'Position Unknown') {
           return unknownPositionIcon;
         } else {
           return unknownPositionIcon;
         }
        }, // end style
      }), // end VectorLayer

      // https://gis.stackexchange.com/questions/126668/writing-style-function-in-openlayers

      new VectorLayer({ // ol/layer/Vector
        title: 'Connecting the locations sequentially',
        imageRatio: 2,
        source: new VectorSource({
          url: urlLineLocJSON, // from line 39/142
          format: new GeoJSON()
        // style: new Style({ //  'ol/style/Style'
        //   // having no effect
        //   stroke: new Stroke({
        //     color: '#f00',
        //     width: 10
        //   }), // VectorSource
        }), // Style
        // style: residLineStyle // quick test
        style: function(feature, resolution) { // should be style, no error if icon
          // maybe could make this smarter
          // console.log("style: ", feature.get('style'))
          if(feature.get('style') === 'resid' ) {
           return residLineStyle;
          } else if(feature.get('style') === 'resto' ) {
            return restoLineStyle;
          } else {
            return unknownLineStyle;
          }
        },
      }), // VectorLayer connecting … sequentially
    ], // layers: 237
  }); // var dataOverlays 246 a LayerGroup with two VectorLayers
  // window.baseLayers = baseLayers
  
  var map = new Map({
    target: 'map', // id
    layers: [baseLayers, dataOverlays], // rendered in order. dataOverlays appear on top of baseLayers
    // Tried creating an overlays: but then layer switcher quit working and dataOverlays didn't show up. Probably some other things that would need to be done. Need import Overlay from 'ol/Overlay';
    // controls: [ overViewMapControl, scaleLineControl,  zoomSliderControl, zoomControl ], //, new ZoomToExtent would need to define the extent, otherwise goes to whole world
    view: new View({
      center: fromLonLat([-118.243, 34.047]), // 11 W. First St. // [-118.243531, 34.038187]
      zoom: 14,
      maxZoom: 20,
      // extent: transformExtent(mapExtent, 'EPSG:4326', 'EPSG:3857'), // need to figure out the limits of the icons for a start
      // constrainOnlyCenter: true // relates to extent
    }), // end view: new View({
  }); // end var map
  
  // various controls. Didn't work if added at map level zoomSliderControl didn't work
    // Definitions
    var layerSwitcher = new LayerSwitcher({
      tipLabel: 'Legend', // Optional label for button and isn't seen
      groupSelectStyle: 'children' // Can be 'children' [default], 'group' or 'none'. Changing made no difference that I could see.
    });
    // defined since has two options
    const overViewMapControl = new OverviewMap({
      collapsed: true,
      layers: [ new TileLayer({ source: new OSM()}) ],
      tipLabel: 'Click for overview map at larger scale',
    })
    const scaleLineControl = new ScaleLine({
      units: 'us', // Supported values are 'degrees', 'imperial', 'nautical', 'metric' (default), 'us'
      bar: false, // bar is great on a normal map, but a bit glaring on these maps
      minWidth: 50, // 120 good for bar option, 50 good for normal
    })
    const zoomSliderControl = new ZoomSlider()

  // Add Controls to map. Have defaults with map, so now adding in.
  map.addControl(scaleLineControl);
  map.addControl(zoomSliderControl);
  // map.addControl(overViewMapControl); // not sure I want
  // Still Can't move map with layerSwitcher commented out
  map.addControl(layerSwitcher); // adds the attribution and layer switcher. How does it gather the information
  // map.addControl(new Loupe()); // TWo imports needed for this. Loupe.js:53 Uncaught TypeError: Cannot read property 'canvas' of undefined. Wipes out part of the zoom controls

  // https://openlayers.org/en/latest/examples/select-hover-features.html
  // fairly clueless what this is doing, but I'm getting coffeeIconFA on rollover.
  var selected = null;
  var status = document.getElementById('status');

  // Feature Hover Logic (Popover for line) - Using Nishanbeav tutorial
  const popoverTextElement = document.getElementById('popover-text'); // an empty div on people>show
  // console.log('document.getElementById(\'popover-text\'): ', popoverTextElement);
  const popoverTextLayer = new Overlay({
    element : popoverTextElement,
    positioning: 'bottom-center', // directions are opposite
  })
  map.addOverlay(popoverTextLayer); // empty at start, but establishes the layer

// Popover. Gathering all "connections/years" for that location from the two jsons
  map.on('pointermove', function(evt){
    let featureInfo = '';
    let isFeatureAtPixel = map.hasFeatureAtPixel(evt.pixel); // OL function. ie, is a feature where we re
    if (isFeatureAtPixel) {
      let featureAtPixel = map.getFeaturesAtPixel(evt.pixel);
      // console.log("featureAtPixel: ", featureAtPixel); // so can see what info is available to feed into next line
      // console.log("featureAtPixel.length: ", featureAtPixel.length);
      for (var i = 0; i <= featureAtPixel.length - 1; i++) {
        featureInfo = featureInfo  + featureAtPixel[i].get('popover_people') + '<br>';
      }
      popoverTextLayer.setPosition(evt.coordinate);
      popoverTextElement.innerHTML = featureInfo; // all
      map.getViewport().style.cursor = 'pointer' // the hand
    } else {
      popoverTextLayer.setPosition(undefined); // otherwise text remains after leaving
      map.getViewport().style.cursor = ''; // change back to arrow which is default
    }
  });

  // Display zoom level. Need to put next to slider FIXME
  map.on('rendercomplete',function(e) {
    var zoomLevel = map.getView().getZoom();
    var zoomRounded=Math.round(zoomLevel*10)/10;
    document.getElementById('zoom-display-number').innerHTML = 'Zoom level ' + zoomRounded;
  });

// } // function init() // currently off
