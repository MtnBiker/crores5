console.log('ol_snippet_display.js:1. For displaying the snippet only. Called from years/show');
import 'ol/ol.css';
import Map from 'ol/Map';
import View from 'ol/View';
import { Vector as VectorLayer } from 'ol/layer';
import ImageLayer from 'ol/layer/Image';
import { Vector as VectorSource } from 'ol/source';
import Static from 'ol/source/ImageStatic';
import Projection from 'ol/proj/Projection';
// import {getCenter} from 'ol/extent'; // if decide to use this
// debugger;
window.onload = init; // function called after window loaded. Needed to have the html document defined and available. 2021.05.27 Seems unnecessary, but still have some issues. Needed to pass data. Now using Turbolinks at bottom of page
console.log('ol_snippet_display.js:13. after window.onload = init');
function init() {
  console.log('ol_snippet_display.js:15. in function init');
  var map_div = 'snippet'; // because occurs in two places and also on show.html.erb. BUT ONLY WORKS IF #map is used, not map_div
  var map_div_js = '#' + map_div; // for js access, only have to change the above
  console.log('ol_snippet_display.js:16. map_div: ', map_div, ' and must agree with the id= on show. map_div_js: ', map_div_js);
  
  // Map views always need a projection.  Here we just want to map image
  // coordinates directly to map coordinates, so we create a projection that uses
  // the image extent in pixels. See new Projection below

  // May be some unnecessary stuff since pared down for just display

  var docURL = $(map_div_js).data('url');
  console.log('ol_snippet_display.js:27. docURL: ', docURL);
  var docWidth = $(map_div_js).data('width');
  var docHeight = $(map_div_js).data('height');
  var extent = [0, 0, docWidth, docHeight];
  // var snippetExtent = $(map_div_js).data('snippet'); // can't use camelCase for data-var
  // console.log('ol_snippet_display.js:30. snippetExtent from web page: ', snippetExtent)
  // var snippetExtent = $(map_div_js).data('snippet'); // can't use camelCase for data-var
  var snippetCoords = $(map_div_js).data('snippet'); // can't use camelCase for data-var

  // snippetExtent =   [34, 562, 440, 688] // testing if quotes messed things up not the problem
  // extent =   [34, 562, 440, 688] // trying to get just this part to show

  // Using calculation on show.html.erb, but could move here, but have to get integer instead of text
  // console.log('ol_snippet_display.js:35. extent (image size): ', extent, '. snippetExtent: ', snippetExtent)
  // var centerX = (snippetExtent[2]+snippetExtent[2])/2;
  console.log('ol_snippet_display.js:40. extent (image size): ', extent, '. snippetCoords: ', snippetCoords);

  //  var centerY = 433;
  //  console.log('ol_snippet_display.js:63. centerX: ', centerX, '. centerY: ', centerY)
  // Easier for me to calculate in Ruby
  var center = $(map_div_js).data('center');
  console.log('ol_snippet_display.js:46. center: ', center);
  // center = [237, 625]; // test
  // console.log('ol_snippet_display.js:67. center: ', center);
  var projection = new Projection({
  	// code: '',
  	units: 'pixels',
  	extent: extent,
  });

  var docSource = new Static({
  	attributions: '',
  	url: docURL,
  	projection: projection,
  	imageExtent: extent,
  });

  var docLayer = new ImageLayer({
  	// var raster in example for draw
  	source: docSource,
  	extent: extent,
  });

  var staticView = new View({
  	projection: projection,
  	center: center, //getCenter(extent), // maybe could use snippet extent?
  	zoom: 2.5, // zoom needs to be defined or the image doesn't show up. 3 seems to fill the div
  	maxZoom: 4, // Can go beyond this with scroll wheel but doesn't get indicated
  });

  var map = new Map({
  	layers: [docLayer], // plural since had more than one, therefore need an array
  	target: map_div, // map_div defined at top and doesn't have to be map
  	view: staticView,
  	extent: extent,
  	controls: [], // don't want zoom buttons to show, but zoom with scroll wheel still works
  });

  // Show zoom level. https://gis.stackexchange.com/questions/62926/openlayers-possible-to-show-zoom-level
  map.on('moveend', function() {
  	var zoom = map.getView().getZoom();
  	var zoomInfo = '<strong>Zoom level:</strong> ' + zoom.toPrecision(3); // significant figures which is easy here since always between 1 and ~24
  	// document.getElementById('zoomlevelsnippet').innerHTML = zoomInfo; // doesn't much matter for this view
  }); // end map.on
} // end function init()
console.log('ol_snippet_display.js:91. Bottom of page. Outside of function init');
// document.addEventListener('DOMContentLoaded', init()); // done this way in ol_snippet_draw.js. 2021.05.27 didn't help pass data

// https://stackoverflow.com/questions/35245735/rails-turbolinks-issues-with-window-onload but loads twice
// $(document).ready(init); // loads twice with this also. Example had var in front of function, but that gave an error
// Is this still the way? The article is 2016 https://github.com/turbolinks/turbolinks, but Turbo is taking over
//
// $(document).on('turbolinks:load', init); // Turbolinks 5. Still 5 in 2021 using yarn
