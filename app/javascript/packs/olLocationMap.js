// <!-- webpacker/packs/olLocationMap.html Used by location > new. show, and edit -->
// Not used for show at the moment, would be nice if it had a period appropriate map. Maybe also for show
// <!-- The initial vars should be off-loaded for use by other maps -->
console.log('olLocationMap.js is the JavaScript being used.');
import Map from 'ol/Map';
import View from 'ol/View';
import VectorLayer from 'ol/layer/Vector';
import VectorSource from 'ol/source/Vector';
import {Icon, Fill, Stroke, Style, Text} from 'ol/style';
import {transform} from 'ol/proj';
// import LayerGroup from 'ol/layer/Group';
// import TileLayer from 'ol/layer/Tile';
import Point from 'ol/geom/Point';
import OSM from 'ol/source/OSM';
import XYZ from 'ol/source/XYZ';
import LayerSwitcher from 'ol-layerswitcher'; // https://github.com/walkermatt/ol-layerswitcher
import Popup from 'ol-popup';                 // https://github.com/walkermatt/ol-popup
import {transformExtent, fromLonLat} from 'ol/proj';
import Feature from 'ol/Feature';
// import MapBrowserEvent from 'ol/MapBrowserEvent'; // Not using
// import Loupe from 'ol-loupe'
// import 'ol-loupe/src/style.css' //Or your own
// function init() {
//   document.removeEventListener('DOMContentLoaded', init); // Farkas
//   console.log('olLocationMap.js:26. Entered function init().');

// all the maps
import {baseLayers} from './modules/baseLayers.js';

// Define Icons for Restaurants and Residences. I think these are icons from .. that I have saved
var homeIcon = new Icon({
  src: './images/home.png',
  opacity: 0.5,
  scale: 1.0,
  size: 30,
}); // homeIcon
var restoIcon = new Icon({
  src: './images/dish.png',
  opacity: 0.5,
  scale: 1.0,
  size: 30,
}); // restoIcon

// Red X
var svg = '<svg width="75" height="75"  xmlns="http://www.w3.org/2000/svg" version="1.1"> + <line x1="2" x2="73" y1="2" y2="73" stroke="red" stroke-width="10"/> + <line x1="2" x2="73" y1="73" y2="2" stroke="red" stroke-width="10"/></svg>';
var styleSvgX = new Style({
  image: new Icon({
    opacity: 1,
    src: 'data:image/svg+xml;utf8,' + svg,
    scale: 0.3,
  }),
});

// var mapExtent = [-118.5, 34.0, -118.2, 33.7]; // need to change numbers if plan to use



// lat lng via the #map Using for marker and centering or set coords for centering map
var lng = $('#map').data('lng');
var lat = $('#map').data('lat');
console.log('olLocationMap.js:61. lng: ', lng, 'lat: ', lat);
// if new lng and lat don't exist, so need to set lng and lat to center the map
if (lng == '' || lat == '') {
  lng = -118.243128;
  lat = 34.052358;
  var newLocation = true; // flag to not place a marker for edit
  // alert("New, setting center values and no marker. newLocation = ", newLocation)
}
console.log('olLocationMap.js:69. lng: ', lng, 'lat: ', lat, '. Should be the same as above.');

// Create the map
var map = new Map({
  target: 'map', // id
  layers: [baseLayers],
  view: new View({
    center: fromLonLat([lng, lat]),
    zoom: 16,
    maxZoom: 20,
  }), // end view: new View({
}); // end var map
// Add marker if not new (but make a marker for edit and show)
if (newLocation != true) {
  // alert("268. Not new location. newLocation = ", newLocation)
  // A marker for the location
  var marker = new Feature({
    geometry: new Point(fromLonLat([lng, lat])), // Cordinates from the calling page via #map
  });
  marker.setStyle(
    new Style({
      image: new Icon({
        opacity: 1,
        src: 'data:image/svg+xml;utf8,' + svg,
        scale: 0.3,
      }),
    }),
  );
  var markerVectorSource = new VectorSource({
    features: [marker],
  });
  // console.log('olLocationMap.js:286. map: ', map);
  // Now make a new layer and add it to the map: Is the var part needed?
  var markerVectorLayer = new VectorLayer({
    source: markerVectorSource,
  });
  map.addLayer(markerVectorLayer);
} // if (newLocation …

// add Layer Switcher control
var layerSwitcher = new LayerSwitcher({
  tipLabel: 'Legend', // Optional label for button and isn't seen
  groupSelectStyle: 'children', // Can be 'children' [default], 'group' or 'none'. Changing made no diffence that I could see.
});
map.addControl(layerSwitcher); // adds the attribution and layer switcher.

// Click to get location to paste into lon lat
// https://stackoverflow.com/questions/26967638/convert-point-to-lat-lon#26968701
// Another way. Similar js, but presentation and location of info different https://openlayers.org/en/latest/examples/popup.html
 var popup = new Popup();
 map.addOverlay(popup);
 map.on('click', function (evt) {
  var lonlat = transform(evt.coordinate, 'EPSG:3857', 'EPSG:4326');
  var lonClick = lonlat[0];
  var latClick = lonlat[1];
  // Used alert until found popup https://github.com/walkermatt/ol-popup. Can delete. Nice to add to clipboard too FIXME
  // alert(
  //    'You clicked near lat lon: ' +
  //      lonClick.toFixed(6) +
  //      '  ' +
  //      latClick.toFixed(6),
  //  );
  popup.show(evt.coordinate, '<b>You clicked at lat lon:</b> <br>' +  lonClick.toFixed(6) + '  ' + latClick.toFixed(6) );
});
// Display zoom level # Was causing a error. And do I want it?
// map.on('rendercomplete',function(e) {
//   var zoomLevel = map.getView().getZoom();
//   var zoomRounded=Math.round(zoomLevel*10)/10;
//   document.getElementById('zoom-display-number').innerHTML = 'Zoom level ' + zoomRounded;
// });


  
// } // end function init()
// document.addEventListener('DOMContentLoaded', init); // Farkas
