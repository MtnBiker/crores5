// <!-- webpack/packs/olAllLocations Used by locations > ol_all_locations.html.erb -->
// based on webpack/packs/olLocationMap

// <!-- The initial vars should be off-loaded for use by other maps -->
console.log('olAllLocations.js is the JavaScript being used.');
import Map from 'ol/Map';
import View from 'ol/View';
import GeoJSON from 'ol/format/GeoJSON';
// import VectorImageLayer from 'ol/layer/VectorImage';  // not using 2020.05.03. I changed how the layers were organized
import VectorLayer from 'ol/layer/Vector';
import VectorSource from 'ol/source/Vector';
import {Icon, Fill, Stroke, Style, Text, Circle} from 'ol/style';
import {transform} from 'ol/proj';
// import LayerGroup from 'ol/layer/Group';
// import LayerImage from 'ol/layer/Image'; // not using 2020.05.03
// import TileLayer from 'ol/layer/Tile';
import Overlay from 'ol/Overlay';
import Point from 'ol/geom/Point';
import OSM from 'ol/source/OSM';
import XYZ from 'ol/source/XYZ';
import LayerSwitcher from 'ol-layerswitcher'; // https://github.com/walkermatt/ol-layerswitcher
import {transformExtent, fromLonLat} from 'ol/proj';
import Select from 'ol/interaction/Select';
import Feature from 'ol/Feature';
// import Loupe from 'ol-loupe'
// import 'ol-loupe/src/style.css' //Or your own

// all the maps
import {baseLayers} from './modules/baseLayers.js';

// Building an overlay layer with all the locations/addresses shown. Bringing in a jBuilder do
// from https://openlayers.org/en/latest/examples/vector-layer.html and will figure it out later
var style = new Style({
  fill: new Fill({
    color: 'rgba(255, 255, 255, 0.6)',
  }),
  stroke: new Stroke({
    color: '#319FD3',
    width: 1,
  }),
  text: new Text({
    font: '12px Calibri,sans-serif',
    fill: new Fill({
      color: '#000',
    }),
    stroke: new Stroke({
      color: '#fff',
      width: 3,
    }),
  }),
});

// FIXME Replace with maybe 2x2 red and white diamond maybe 20px on a side
var svgDiamond = '<svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-diamond-fill" viewBox="0 0 16 16"><path fill-rule="evenodd" d="M6.95.435c.58-.58 1.52-.58 2.1 0l6.515 6.516c.58.58.58 1.519 0 2.098L9.05 15.565c-.58.58-1.519.58-2.098 0L.435 9.05a1.482 1.482 0 0 1 0-2.098L6.95.435z"/></svg>';
var styleDiamond = new Style({
  image: new Icon({
    opacity: 1,
    src: 'data:image/svg+xml;utf8,' + svgDiamond,
    scale: 0.5, // size controlled here at least to a point
  }),
});

var diamondStroke = '<svg xmlns="http://www.w3.org/2000/svg" width="400" height="400"  viewBox="0 0  1430 1200" preserveAspectRatio="xMinYMin meet" ><rect id="svgEditorBackground" x="0" y="0" width="1430" height="1200" style="fill: none; stroke: none;"/><path d="M0,-4l-4,4l4,4l4,-4Z" style="fill:white; stroke:black; vector-effect:non-scaling-stroke;stroke-width:4px;" id="e1_shape" transform="matrix(16.6389 0 0 16.6389 552.961 550.959)"/></svg>'
var styleDiamondStroke = new Style({
  image: new Icon({
    opacity: 1,
    src: 'data:image/svg+xml;utf8,' + diamondStroke,
    scale: 0.25, // size controlled here at least to a point
  }),
});

var redSquare = '<svg xmlns="http://www.w3.org/2000/svg" width="400" height="400" viewBox="0 0 400 400" xml:space="preserve"><desc>Created with Fabric.js 4.2.0</desc><defs></defs><g transform="matrix(1 0 0 1 200 200)" id="c6c83eaf-d4dc-4a66-894f-07c15b5ce45e"  ><rect style="stroke: none; stroke-width: 1; stroke-dasharray: none; stroke-linecap: butt; stroke-dashoffset: 0; stroke-linejoin: miter; stroke-miterlimit: 4; fill: rgb(255,255,255); fill-rule: nonzero; opacity: 1;" vector-effect="non-scaling-stroke"  x="-200" y="-200" rx="0" ry="0" width="400" height="400" /></g><g transform="matrix(5.44 0 0 5.44 199.92 199.92)" id="9aa29af0-aaca-4d18-8be9-a264d86c1e92"  ><rect style="stroke: rgb(0,0,0); stroke-width: 0; stroke-dasharray: none; stroke-linecap: butt; stroke-dashoffset: 0; stroke-linejoin: miter; stroke-miterlimit: 4; fill: rgb(255,0,0); fill-rule: nonzero; opacity: 1;" vector-effect="non-scaling-stroke"  x="-33.0835" y="-33.0835" rx="0" ry="0" width="66.167" height="66.167" /></g></svg>'
var styleRedSquare = new Style({
  image: new Icon({
    opacity: 1,
    src: 'data:image/svg+xml;utf8,' + redSquare,
    scale: 0.03, // size controlled here at least to a point
  }),
});

// Establish a vector source for a vector layer for the location data. The data is a jBuilder geojson, but FIXME should be a Postgres view or similar since jBuilder gets build each time and a View only when the data changes?
var vectorLocations = new VectorSource({
  // read `http://localhost:3000/map/locationmap` as geojson. And it is now there and almost empty
  // url: 'map/allLocations', // Setting this option instructs the source to load features using an XHR loader (see module:ol/featureloader~xhr). Worked when allLocations was in map, not locations
  url: 'allLocations.json', // need the .json
  format: new GeoJSON(), // The feature format used by the XHR feature loader when url is set. Required if url is set, otherwise ignored. ie, the url is to be read as geojson
  projection: 'EPSG:3857',
  attributions: [ " <a href='./about'>CroRes</a>" ]
})
// All the locations put on the map with a small diamond
var locationsLayer = new VectorLayer({
  name: 'All the addresses/locations', // not sure this is being used, but sometimes name is needed
  source: vectorLocations, //  the geojson
  style: styleDiamondStroke
});

// A popup (pointermove roll over) to show information about the locations

// Starting with https://openlayers.org/en/latest/examples/popup.html even though it's not exactly what I want

// Elements that make up the popup. From https://openlayers.org/en/latest/examples/popup.html. Can't move lower since container is needed for overlay
var container = document.getElementById('popup');
var content = document.getElementById('popup-content'); // The example did some things I don't need
var closer = document.getElementById('popup-closer'); // probably don't need but we'll see

// Create an overlay to anchor the popup to the map. Note this is different than just another layer. Is added to map, so needs to be up here
var overlay = new Overlay({
  element: container,
  autoPan: true,
  autoPanAnimation: {
    duration: 250,
  },
});

// var addressInfo = "popup: " + "popup" +" Need to parse map/locationmap.geojson for this location or <b>attach it to the point to begin with</b>"
// Total swag reworking set style. Returns literal. Used with map.on below, but not working at the moment or it never will
// 
// var addressInfo = function (feature) {
//   addressInfo.getText().setText(feature.get('popup'));
//   return addressInfo;
// }

// end Overlay/Popup

// Create the map with the base layers and locations layer and also an "overlay" for the popup info
var map = new Map({
  target: 'map', // id
  layers: [baseLayers, locationsLayer], // locationsLayer is all the addresses and is on top of the baselayers maps
  overlays: [overlay], // for popup
  view: new View({
    center: fromLonLat([-118.243507, 34.052211]),
    zoom: 15,
    maxZoom: 21,
  }), // end view
}); // end var map

// add Layer Switcher control
var layerSwitcher = new LayerSwitcher({
  tipLabel: 'Legend', // Optional label for button and isn't seen
  groupSelectStyle: 'children', // Can be 'children' [default], 'group' or 'none'. Changing made no diffence that I could see.
});
map.addControl(layerSwitcher); // adds the attribution and layer switcher.

// Display zoom level. Would like it to be at the upper right corneer of the map
map.on('rendercomplete',function(e) {
  var zoomLevel = map.getView().getZoom();
  var zoomRounded=Math.round(zoomLevel*10)/10;
  document.getElementById('zoom-display-number').innerHTML = 'Zoom level ' + zoomRounded;
}); // end zoom Level display

// From OL Beginner's Guide 11_8' Next 30 lines and p.370.pdf
  // use the features Collection to detect when a feature is selected,
  // the collection will emit the add event. 'add' event https://openlayers.org/en/latest/apidoc/module-ol_Collection.CollectionEvent.html

// create a Select interaction and add it to the map. IIRC select is to get the click 
var select = new Select({
  layers: [locationsLayer],
  style: styleRedSquare
   //,
});
map.addInteraction(select);

var selectedFeatures = select.getFeatures();

selectedFeatures.on('add', function(event) { // Only 'add' works, with a click for #address-info. 'mouseover', 'focus', 'focusin', 'hover' and 'click' don't work for #address-info. https://api.jquery.com/on/, but doesn't have 'add' nor doesn https://api.jquery.com/category/events/. I want this to work with mouseover or hover
  var feature = event.target.item(0);
  var locAddress = feature.get('address'); // from geojson
  var locID = feature.get('id');
  var locHistory = feature.get('history'); // history not working in allLocationsMap.json.jbuilder
  var addressInfo = locAddress // proxy for adding a link FIXME.  + '<br> locations/' + locID
  var locCoordinates = feature.get('coordinates'); // this is coordinates added via json.properties
  var olCoordinatesJson = fromLonLat(locCoordinates); // for overlay.setPosition
  content.innerHTML = addressInfo; // var content on line 291
  overlay.setPosition(olCoordinatesJson);

  // Left column (sidebar) list of years (connections) for this address
  $('#addressHeading').html('Who worked or lived at: ' + locAddress + ' (Location ID: ' + locID + '). history: ' + locHistory + '. from olAllLocations.js');
  var yearsList = " List of people and when at this address TBD"; // Trying to get the list of all 'years' associated with this address, so won't be addressInfo, but something else
  // Need a json to bring in all the years for the location which is what is done for location/show but id is known ahead. So needs to be updated with the selected loc_id. Know how to do this in Rails, but not jS which is where we are.
  // $('#yearsList').html(yearsList);
  // $('#js-test').html('Test writing to #js-test. `locID`: ' + locID + '. Works fine when not buried in Stimulus like #loc_id');
  // $('#loc_id').html('XXXX' + locID + '   = `locID`  Test writing to #loc_id.'); // locID first so can get with regex
  $('#near-top').html(locAddress   + '. location_id: '  + locID); // replace heading while one column

  $('#query').html(locID); // trying to put in loc_id into search field, Does seem to work although doesn't show up on page

  // want this <%= render Year.where(location_id: @locID) %> to go to element id="years" where @locID is locID
  // $('#years').html('<%= j render Year.where(location_id: ' + locID + ') %>') // puts the code on the page literally as expected because there's nothing to fire render

});

// when a feature is removed, i.e. when click on another item, clear the address-info div
selectedFeatures.on('remove', function(event) {
  $('#address-info').empty();
});
