import { Controller } from "stimulus"

export default class extends Controller {
  // Following three lines are default. The rest as part of other demos
  connect() {
    this.element.textContent = "Hello World!"
  }

  static targets = [ "output", "hello", "name" ]

  // Very simple confirmation that connection is made
  connect() {
      console.log("Hello, Stimulus! via console.log", this.element)
  }

// on allLocations.html.erb
  greet() {
    // Following line is first step
    // console.log("Hello, Stimulus! via greet() and it'a only console.log, not an element on a page", this.element)
    // These three use input, but still console.log
    const element = this.nameTarget
    const name = element.value
    console.log(`Hello, ${name}!`)
  }

  // demo on /map page
  connect() {
    this.outputTarget.textContent = 'Hello, Stimulus!  (Stimulus demo). This comes from webpacker/packs/controllers/hello_controller.js'
  }
}
