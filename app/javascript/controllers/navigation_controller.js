// https://www.mikewilson.dev/posts/using-hotwire-with-rails-for-a-spa-like-experience/

import { Controller } from 'stimulus';
// import { Controller } from 'hotwire'; // ?? But not using anyway at present

export default class extends Controller {
  static targets = ['content', 'loading', 'link'];

  displayLoading(event) {
    this.loadingTarget.classList.remove('hidden');
    this.contentTarget.classList.add('hidden');

    let value = event.detail.url;

    this.updateLinks(value);
  }

  displayContent() {
    this.loadingTarget.classList.add('hidden');
    this.contentTarget.classList.remove('hidden');
  }

  // Iterate all of our links, remove the selected class, but then add
  // if the link href matches the url we're navigating to.
  updateLinks(item) {
    this.linkTargets.forEach((link) => {

      link.classList.remove('selected');
      if (link.href === item) {
        link.classList.add('selected');
      }
    })
  }
}
