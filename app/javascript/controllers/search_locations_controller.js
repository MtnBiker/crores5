// app/javascript/controllers/search_controller.js
import { Controller } from 'stimulus';

// Wired to location/index searchingor something like that
export default class extends Controller {
	static targets = ["query", "locations"]

  connect() {
  }

  submit() {
    const value = this.queryTarget.value
    fetch(`/?query=${value}`, {
      headers: { accept: 'application/json'}
    }).then((response) => response.json())
    .then(data => {
     var locationHTML = "";
     var locationArray = Object.values(data)[0]
     locationArray.forEach(location => {
      locationHTML += this.locationTemplate(location)
    });
     this.locationsTarget.innerHTML = locationHTML;
   });
  }

  // Somehow as I wrote this it's not being used.
  locationTemplate(location) {
    return `<div>
    <h4>${location.address} <small>${location.notes}</small></h4>
    <p>${location.current_description}</p>
    </div> `
  }

}
