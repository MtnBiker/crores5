// app/javascript/controllers/search_controller.js
import { Controller } from 'stimulus';

// Wired to year/index searchingor something like that
export default class extends Controller {
	static targets = ["query", "years"]

  connect() {
  }

  submit() {
    const value = this.queryTarget.value
    fetch(`/?query=${value}`, {
      headers: { accept: 'application/json'}
    }).then((response) => response.json())
    .then(data => {
     var yearHTML = "";
     var yearArray = Object.values(data)[0]
     yearArray.forEach(year => {
      yearHTML += this.yearTemplate(year)
    });
     this.yearsTarget.innerHTML = yearHTML;
   });
  }

}
