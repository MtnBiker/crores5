import { Controller } from "stimulus"

export default class extends Controller {

  static targets = ["input", "output"]

// showID(event)
  showId() {
    const input_element = this.inputTarget
    console.log('doc-selected_controller:10. inputTarget. input_element: ', input_element)
    const output_element = this.outputTarget // <div data-doc-selected-target="output">

    var idSelected = document.getElementById("year_doc_id").value; // year_doc_id is id 
    console.log('14. Selected doc id: ', idSelected)
    output_element.textContent = "15. Selected doc id: " + idSelected // output

    // Maybe useful, but not using directly
    // console.log('doc-selected_controller:18. Input. this: ', this)
    // console.log('doc-selected_controller:19. Input. this.element: ', this.element)
    // console.log('doc-selected_controller:20. outputTarget. output_element: ', output_element)
  }

}
