// app/javascript/controllers/search_controller.js
import { Controller } from 'stimulus';

// Wired to location/index searching
export default class extends Controller {
	static targets = ["query", "people"]

  connect() {
  }

  submit() {
    const value = this.queryTarget.value
    fetch(`/?query=${value}`, {
      headers: { accept: 'application/json'}
    }).then((response) => response.json())
    .then(data => {
     var personHTML = "";
     var personArray = Object.values(data)[0]
     personArray.forEach(person => {
      personHTML += this.personTemplate(person)
    });
     this.peopleTarget.innerHTML = personHTML;
   });
  }

  // Somehow as I wrote this it's not being used.
  personTemplate(person) {
    return `<div>
    <h4>${person.given_name} <small>${person.last_name}</small></h4>
    <p>${person.date_of_birth}</p>
    </div> `
  }

}
