// // Visit The Stimulus Handbook for more details
// // https://stimulusjs.org/handbook/introduction
// //
// // This example controller works with specially annotated HTML like:
// //
// // <div data-controller="hello">
// //   <h1 data-target-hello="output"></h1> // v2 way
// // </div>
//
// import { Controller } from "stimulus"
//
// export default class extends Controller {
//   static targets = [ "output", "loc_id", "name"]
//
//   // Very simple confirmation that connection is made
//   connect() {
//     // Happens even without the data-action on the .html.erb
//     console.log("17. Hello, Stimulus from year_list_controller#connect! via console.log. this.element: ", this.element , "..")
//     // Couldn't quite get the following three lines to work, but connect is the wrong action since not the locID is not there to begin with
//     // var str = "loc_id";
//     // var n = str.search(this.element);
//     // console.log("30. for " + str + " n: " , n)
//   }
//
//   // A dev and test for click. This works, so can click on an element, but clickLocation below doesn't
//   testclick(){
//     console.log("27. Hello, Stimulus from year_list_controller#testclick! via console.log. this.element: " + this.element + "..")
//   }
//
//   // From Drifting Ruby
//   // changed() {
//   //     this.outputTarget.textContent = this.inputTarget.value
//   //   }
//
//   // this works, but off so can simplify
//   // changed() {
//   //   // console.log(this.yearsTarget.value)
//   //   console.log("21. Hello, Stimulus from year_list_controller#changed! via console.log. this.element: " + this.element + "..")
//   //   this.outputTarget.textContent = this.loc_idTarget.value
//   // }
//
// // This doesn't work, but maybe element isn't specific enough
//   clicklocation() {
//     // console.log(this.yearsTarget.value)
//     console.log("39. Hello, Stimulus from year_list_controller#click! via console.log. this.element: " + this.element + "..")
//     this.outputTarget.textContent = this.loc_idTarget.value
//   }
//
// // on allLocations.html.erb
//   years = "Test from list action"
//
//   list() {
//     // Following line is first step
//     console.log("Hello, Stimulus! via greet() and it'a only console.log, not an element on a page", this.element)
//     // These three use input, but still console.log
//     const element = this.yearsTarget
//     console.log('element: ' + element)
//     const name = element.value
//     console.log(`Hello, ${years}!`)
//   }
//
//   // demo on /map page // but not working now although is did earlier
//   connect() {
//     this.outputTarget.textContent = 'Hello, Stimulus!  (Stimulus demo). This comes from webpacker/packs/controllers/hello_controller.js'
//   }
// }
