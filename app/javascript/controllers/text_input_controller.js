import { Controller} from "stimulus"

export default class extends Controller {
  static targets = ["input", "output"]

  connect() {}
  
  changed() {
    console.log('this.inputTarget.value: ' + this.inputTarget.value + '. From text_input_controller for development')
    this.outputTarget.textContent = this.inputTarget.value
  }
}
