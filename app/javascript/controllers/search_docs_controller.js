// app/javascript/controllers/search_controller.js
import { Controller } from 'stimulus';

// Wired to doc/index searchingor something like that
export default class extends Controller {
	static targets = ["query", "docs"]

  connect() {
  }

  submit() {
    const value = this.queryTarget.value
    fetch(`/?query=${value}`, {
      headers: { accept: 'application/json'}
    }).then((response) => response.json())
    .then(data => {
     var docHTML = "";
     var docArray = Object.values(data)[0]
     docArray.forEach(doc => {
      docHTML += this.docTemplate(doc)
    });
     this.docsTarget.innerHTML = docHTML;
   });
  }

}
