// app/javascript/controllers/search_controller.js
import { Controller } from 'stimulus';

// Wired to source/index searchingor something like that
export default class extends Controller {
	static targets = ["query", "sources", 'sources_brief']

  connect() {
  }

  submit() {
    const value = this.queryTarget.value
    fetch(`/?query=${value}`, {
      headers: { accept: 'application/json'}
    }).then((response) => response.json())
    .then(data => {
     var sourceHTML = "";
     var sourceArray = Object.values(data)[0]
     sourceArray.forEach(source => {
      sourceHTML += this.sourceTemplate(source)
    });
     this.sourcesTarget.innerHTML = sourceHTML;
   });
  }

}
