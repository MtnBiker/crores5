# app/controllers/people/lines_controller.rb
# From https://stackoverflow.com/questions/60610788/rails-connecting-to-jbuilder
# Works for both OpenLayers and Leaflet

# From config/routes.rb
# resources :people do
#   resources :locations, only: [:index, :line_loc], module: :people # assuming index is arbitrary and not linked to index page since it's used on show page and line_loc is for another layer
# end

module People
  class LinesController < ApplicationController
    before_action :set_person

    def index
      respond_to do |f|
        f.json
      end
    end

    private

    def set_person
      @person = Person.eager_load(:locations)
                      .find(params[:person_id])
      @locations = @person.locations
    end
  end
end

# https://stackoverflow.com/questions/59548291/rails-passing-variable-to-jbuilder
# For OpenLayers version. Not clear to me why it doesn't work.
# module People
#   class LocationsController
#     # GET /people/1/locations.json
#     def index
#       @person = Person.includes(:locations).find(params[:person_id])
#       @locations = @person.locations
#     end
#   end
# end
