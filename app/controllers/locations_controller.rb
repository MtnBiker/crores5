class LocationsController < ApplicationController
  helper_method :sort_column, :sort_direction
  # before_action :all_locations
  before_action :set_location, only: [:show, :edit, :update, :destroy]

  # GET /locations
  # GET /locations.json
  def index
    # @locations = Location.all # Put back in if remove global_search

    # For sortable columns
    # @locations = Location.order(sort_column + " " + sort_direction) # Redundant as it is below
    @locations = if params[:search_locations]
                   Location.search(params[:search_locations]) # .order("created_at DESC")
                 else
                   Location.order(sort_column + ' ' + sort_direction) # Changing from above fixed sorting
                 end

    # For Stimulus search
    # https://medium.com/swlh/build-a-dynamic-search-with-stimulus-js-and-rails-6-56b537a44579
    # Fields searched are set in location.rb
    if params[:query]
      @locations = Location.global_search(params[:query])
    else
      @locations = Location.all
    end
    respond_to do |format|
      format.html
      format.json { render json: { locations: @locations } }
    end
 end

  # GET /locations/1
  # GET /locations/1.json
  # I can't see that any of this def is being used at least not by map/allLocations or locations/show
  def show
    # Cam Uncomment if want to put pagination here, but so far don't need pagination
    # @years = @location.years.paginate(page: params[:page]) # for showing connection/years in person>show also location>show

    # For all_locations since show gets called because I'm asking for locations with id = "all_locations" FIXME
    # if id = "all_locations"
    #   @locations = Location.all
    # else
    location = Location.find(params[:id]) # added 2020.04.20 working on showing docs in show
    # end
  end

  # GET /locations/new
  def new
    # Probably not necessary to have the following line for simple cases, but if want to define defaults need the instance which can be picked up from the database definitions.
    @location = Location.find_by(id: params[:base_id])&.dup || Location.new # ({:address=> "Use W E etc and St without PERIODS"})
    # @docs = Doc.all # tried for getting the association to work. .order("year_name_page") had no effect
    repopulateResidResto() #  updating resto_resid_lines to whatever may have changed due to this change
  end

  # GET /locations/1/edit
  def edit; end

  # POST /locations
  # POST /locations.json
  def create
    # Instantiate a new object using form parameters (notes here from Lynda>Skoglund)
    @location = Location.new(location_params)

    # Save the object
    respond_to do |format|
      if @location.save
        # If save succeeds, redirect to the index action
        format.html { redirect_to @location, notice: 'Address was successfully created.' }
        format.json { render :show, status: :created, location: @location }
      else
        # If save fails, redisplay the form with information user filled in
        format.html { render :new }
        format.json { render json: @location.errors, status: :unprocessable_entity }

      end
    end
    # repopulateResidResto() # creating a location won't change rrs
  end # end create

  # PATCH/PUT /locations/1
  # PATCH/PUT /locations/1.json
  def update
    respond_to do |format|
      if @location.update(location_params)
        # If update succeeds, redirect to the show page
        format.html { redirect_to @location, notice: 'Address was successfully updated.' }
        format.json { render :show, status: :ok, location: @location }
      else
        # If update fails, redisplay the edit form for fixing
        format.html { render :edit }
        format.json { render json: @location.errors, status: :unprocessable_entity }
      end
    end
    repopulateResidResto() #  # updating resto_resid_lines to whatever may have changed due to this change
  end # End update

  # DELETE /locations/1
  # DELETE /locations/1.json
  def destroy
    # if a connection exists to this address, preventing deleting an address
    if @location.years.any? # true if any connections to this address exist
      respond_to do |format|
        format.html do
          redirect_to @location,
                      notice: "Address cannot be deleted because #{@location.years.count} people is/are connected to this address as listed below"
        end
        format.json { head :no_content }
      end # respond_to and not allowing a deletion
    else
      location = @location.destroy
      respond_to do |format|
        format.html do
          redirect_to locations_url, notice: "Location '#{location}' was successfully destroyed."
        end
        format.json { head :no_content }
      end # respond_to and we are deleting
      repopulateResidResto()
    end
  end

  # Map for all locations. olAllLocations.js. jBuilder needs this, without is the .geojson is essentially empty
  def allLocations
    @locations = Location.all
  end

  # https://boringrails.com/articles/hovercards-stimulus/
  def hovercard
    @shoe = Location.find(params[:id])
    render layout: false
  end

  # For people/show map @max https://stackoverflow.com/questions/59548291/rails-passing-variable-to-javascript/59556488#59556488
  # Don't think this is doing anything here. people/show and /people/1/locations.json work without this
  # module People
  #   class LocationsController
  #     # GET /people/1/locations.json
  #     def index
  #       @person = Person.includes(:locations).find(params[:person_id])
  #       @locations = @person.locations
  #       binding.pry
  #     end
  #   end
  # end

  private

  # Use callbacks to share common setup or constraints between actions.
  def set_location
    @location = Location.find(params[:id])
  end

  # Never trust parameters from the scary internet, only allow the white list through.
  def location_params
    params.require(:location).permit(:address, :city, :state, :longitude, :latitude, :geom,
                                     :coords_not_locked, :geocoded_with, :extant, :pre1890, :current_description, :street_view_url, :street_view_note, :notes, :doc_id, :content, :content_plain_text, :snippet_coords, :doc_ids, :doc)
  end

  def sort_column
    Location.column_names.include?(params[:sort]) ? params[:sort] : 'address'
  end

  def sort_direction
    %w[asc desc].include?(params[:direction]) ? params[:direction] : 'asc'
  end
end
