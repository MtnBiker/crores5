class PeopleController < ApplicationController
  # include LocationsController # wrong argument type Class (expected Module)
  helper_method :sort_column, :sort_direction
  before_action :set_person, only: [:show, :edit, :update, :destroy]

  # GET /people
  # GET /people.json
  def index
    # @people = Person.all
    # @people = Person.order(last_name: :asc) # should sort on index page, but Ransack may be overriding. But isn't hurting anything. Sorting on ID by default.
    # For Sortable columns
    # @people = Person.order(sort_column + " " + sort_direction) # Redundant as it is below

    if params[:search_people]
      @people = Person.search(params[:search_people]).order('given_name ASC') # sorts on first name
    else
      # @people = Person.all
      @people = Person.order(sort_column + ' ' + sort_direction) # Changing from above fixed sorting.Doesn't matter if this is here or not
    end

    # For Stimulus search
    # https://medium.com/swlh/build-a-dynamic-search-with-stimulus-js-and-rails-6-56b537a44579
    if params[:query]
      @people = Person.global_search(params[:query])
    else
      @people = Person.all
    end
    respond_to do |format|
      format.html
      format.json { render json: { people: @people } }
    end
  end

  # GET /people/1
  # GET /people/1.json
  def show; end

  # GET /people/new
  def new
    @person = Person.find_by(id: params[:base_id])&.dup || Person.new # for duplicate
    # repopulateResidResto() # If this is just for the new page, it's not needed. Probably just create and update
  end

  # GET /people/1/edit
  def edit; end

  # POST /people
  # POST /people.json
  def create
    @person = Person.new(person_params)
    puts @person.last_name
    puts '@person.last_name above. Person.new(person_params).last_name below'
    puts Person.new(person_params).last_name
    respond_to do |format|
      if @person.save
        format.html { redirect_to @person, notice: 'Person was successfully created.' }
        format.json { render :show, status: :created, location: @person }
      else
        format.html { render :new }
        format.json { render json: @person.errors, status: :unprocessable_entity }
      end
    end
    # repopulateResidResto() # a new person won't change. Need to go to years
  end

  # PATCH/PUT /people/1
  # PATCH/PUT /people/1.json
  def update
    respond_to do |format|
      if @person.update(person_params)
        format.html { redirect_to @person, notice: 'Person was successfully updated.' }
        format.json { render :show, status: :ok, location: @person }
      else
        format.html { render :edit }
        format.json { render json: @person.errors, status: :unprocessable_entity }
      end
    end
    repopulateResidResto() #  updating resto_resid_lines to whatever may have changed due to this change
  end

  # DELETE /people/1
  # DELETE /people/1.json
  def destroy
    @person.destroy
    respond_to do |format|
      format.html { redirect_to people_url, notice: 'Person was successfully destroyed.' }
      format.json { head :no_content }
    end
    repopulateResidResto()
  end

  # the following two defs used by map_data.json.jbuilder
  # def line_data()
  #   @where_who_when  =  Years.select("location_id, person_id, ")
  # end

  private

  # Use callbacks to share common setup or constraints between actions.
  def set_person
    # byebug
    # binding.pry # works better than byebug?
    @person = Person.find(params[:id])
  end

  # Never trust parameters from the scary internet, only allow the white list through.
  # people_pkey or pkey, only one is correct
  # TODO is :name needed here
  def person_params
    params.require(:person).permit(:last_name, :given_name, :date_of_birth, :place_of_birth,
                                   :date_of_death, :date_of_entry, :doe_source, :date_of_citizenship, :notes, :croatian, :content, :content_plain_text, :snippet_coords, :doc_id)
  end

  def sort_column
    Person.column_names.include?(params[:sort]) ? params[:sort] : 'last_name'  # Not sure what this needs to be. last_name seemed to make it work. Has to be a column name, not something that's been defined
  end

  def sort_direction
    %w[asc desc].include?(params[:direction]) ? params[:direction] : 'asc'
  end
end
