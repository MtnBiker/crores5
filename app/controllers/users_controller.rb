class UsersController < ApplicationController
  before_action :logged_in_user, only: [:index, :edit, :update, :destroy]
  before_action :correct_user,   only: [:edit, :update]
  before_action :admin_user,     only: :destroy

  # The following from https://devcenter.heroku.com/articles/direct-to-s3-image-uploads-in-rails. But is duplicated in Years controller. Should it be in both places? TODO This broke login on Safari localhost. and thee is no set_user action When did htis appear?
  # before_action :set_user, only: [:show, :edit, :update, :destroy]
  before_action :set_s3_direct_post, only: [:new, :edit, :create, :update]
  # def index # had two indexes, the sort one looks more important
  #   @users = User.paginate(page: params[:page])
  # end

  def index
    @users = User.order(sort_column + ' ' + sort_direction)
  end

  def new
    @user = User.new
  end

  def create # Listing 11.36
    @user = User.new(user_params)
    if @user.save
      @user.send_activation_email
      flash[:info] = 'Please check your email to activate your account.'
      redirect_to root_url
    else
      render 'new'
    end
  end

  def edit
    @user = User.find(params[:id])
  end

  def update
    @user = User.find(params[:id])
    if @user.update_attributes(user_params)
      flash[:success] = 'Profile updated'
      redirect_to @user
    else
      render 'edit'
    end
  end

  def show
    @user = User.find(params[:id])
  end

  def destroy
    User.find(params[:id]).destroy
    flash[:success] = 'User deleted'
    redirect_to users_url
  end

  private

  # Added id to user_params to get a test to pass, not sure if it's correct
  def user_params
    params.require(:user).permit(:id, :name, :email, :password, :password_confirmation)
  end

  # https://devcenter.heroku.com/articles/direct-to-s3-image-uploads-in-rails
  def set_s3_direct_post
    @s3_direct_post = S3_BUCKET.presigned_post(
      key: "uploads/#{SecureRandom.uuid}/${filename}", success_action_status: '201', acl: 'public-read'
    )
  end

  def sort_column
    User.column_names.include?(params[:sort]) ? params[:sort] : 'user'
  end

  def sort_direction
    %w[asc desc].include?(params[:direction]) ? params[:direction] : 'asc'
  end

  # Before filters

  # Confirms a logged-in user.
  def logged_in_user
    unless logged_in?
      store_location
      flash[:danger] = 'Please log in.'
      redirect_to login_url
    end
  end

  # Listing 10.31 online
  # Confirms the correct user.
  def correct_user
    @user = User.find(params[:id])
    redirect_to(root_url) unless current_user?(@user)
  end

  # Confirms an admin user.
  def admin_user
    redirect_to(root_url) unless current_user.admin?
  end
end
