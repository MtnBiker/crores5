class DocsController < ApplicationController
  before_action :set_doc, only: [:show, :edit, :update, :destroy]
  helper_method :sort_column, :sort_direction

  # GET /docs
  # GET /docs.json
  def index
    # @docs = Doc.all # default
    # @docs = Doc.order(id: :asc) # initial sort on load.
    @q = Doc.ransack(params[:q])
    @docs = @q.result.includes(:source) # this allows associations in ransack

    # For Stimulus search
    # https://medium.com/swlh/build-a-dynamic-search-with-stimulus-js-and-rails-6-56b537a44579
    # Fields searched are set in doc.rb
    if params[:query]
      @docs = Doc.global_search(params[:query])
    else
      @docs = Doc.all
    end
    respond_to do |format|
      format.html
      format.json { render json: { docs: @docs } }
    end

  end

  # GET /docs/1
  # GET /docs/1.json
  def show
    # @source.doc = Naturalsorter::Sorter.sort(@source.docs) # SWAG error
  end

  # GET /docs/new
  def new
    @doc = Doc.new
  end

  # GET /docs/1/edit
  def edit; end

  # POST /docs
  # POST /docs.json
  def create
    # https://guides.rubyonrails.org/active_storage_overview.html#attaching-files-to-records Next 3 lines to see if fixes metadata. Same with these three lines or the longer respond_to below
    # doc = Doc.create!(doc_params)
    # session[:doc_id] = doc.id
    # redirect_to doc

    @doc = Doc.new(doc_params)
    respond_to do |format|
      if @doc.save
        format.html { redirect_to @doc, notice: 'Doc was successfully created.' }
        format.json { render :show, status: :created, location: @doc }
      else
        format.html { render :new }
        # format.html { render :edit } #Broke refresh to get metadaa
        format.json { render json: @doc.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /docs/1
  # PATCH/PUT /docs/1.json
  def update
    respond_to do |format|
      if @doc.update(doc_params)
        format.html { redirect_to @doc, notice: 'Doc was successfully updated.' }
        format.json { render :show, status: :ok, location: @doc }
      else
        format.html { render :edit }
        format.json { render json: @doc.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /docs/1
  # DELETE /docs/1.json
  def destroy
    @doc.destroy
    respond_to do |format|
      format.html { redirect_to docs_url, notice: 'Doc was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private

  # Use callbacks to share common setup or constraints between actions.
  def set_doc
    @doc = Doc.find(params[:id])
  end

  # Never trust parameters from the scary internet, only allow the white list through.
  def doc_params
    params.require(:doc).permit(:source_id, :page_no, :image, :original_url, :basename, :notes,
                                :content, :content_plain_text, :snippet_coords)
  end

  def sort_column
    Doc.column_names.include?(params[:sort]) ? params[:sort] : 'year_page' # it does matter what this last field is. Where is default sort set?
  end

  def sort_direction
    %w[asc desc].include?(params[:direction]) ? params[:direction] : 'asc'
  end
end
