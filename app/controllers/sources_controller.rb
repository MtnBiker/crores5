class SourcesController < ApplicationController
  before_action :set_source, only: %i[show edit update destroy]
  helper_method :sort_column, :sort_direction

  # GET /sources
  # GET /sources.json
  def index
    @sources = Source.order(sort_column + ' ' + sort_direction) # same with default. Not setting default ordr
    # @sources = Source.all # default

    # For Stimulus search
    # https://medium.com/swlh/build-a-dynamic-search-with-stimulus-js-and-rails-6-56b537a44579
    # Fields searched are set in source.rb
    if params[:query]
      @sources = Source.global_search(params[:query])
    else
      @sources = Source.all
    end
    respond_to do |format|
      format.html
      format.json { render json: { sources: @sources } }
    end
  end

  def index_brief
    @sources = Source.order(sort_column + ' ' + sort_direction)
  end

  # GET /sources/1
  # GET /sources/1.json
  def show
    # sorted_docs # undefined method `gsub!'
  end

  # GET /sources/new
  def new
    @source = Source.new({ effective_date: '1887-01-01' }) # This is default. Helps to change when going through a group.
  end

  # GET /sources/1/edit
  def edit; end

  # POST /sources
  # POST /sources.json
  def create
    @source = Source.new(source_params)

    respond_to do |format|
      if @source.save
        format.html do
          redirect_to @source, notice: 'Source was successfully created.'
        end
        format.json { render :show, status: :created, location: @source }
      else
        format.html { render :new }
        format.json do
          render json: @source.errors, status: :unprocessable_entity
        end
      end
    end
  end

  # PATCH/PUT /sources/1
  # PATCH/PUT /sources/1.json
  def update
    respond_to do |format|
      if @source.update(source_params)
        format.html do
          redirect_to @source, notice: 'Source was successfully updated.'
        end
        format.json { render :show, status: :ok, location: @source }
      else
        format.html { render :edit }
        format.json do
          render json: @source.errors, status: :unprocessable_entity
        end
      end
    end
  end

  # DELETE /sources/1
  # DELETE /sources/1.json
  def destroy
    @source.destroy
    respond_to do |format|
      format.html do
        redirect_to sources_url, notice: 'Source was successfully destroyed.'
      end
      format.json { head :no_content }
    end
  end

  private

  # Use callbacks to share common setup or constraints between actions.
  def set_source
    @source = Source.find(params[:id])
  end

  # Never trust parameters from the scary internet, only allow the white list through.
  def source_params
    params
      .require(:source)
      .permit(
      :effective_date,
      :cover_year,
      :name,
      :publisher,
      :file_basename,
      :from_where,
      :from_where_url,
      :notes,
      :caption,
      documents: []
    )
  end

  def sort_column
    Source.column_names.include?(params[:sort]) ? params[:sort] : 'cover_year' # it does matter what this last field is. Where is default sort set?
  end

  def sort_direction
    %w[asc desc].include?(params[:direction]) ? params[:direction] : 'asc'
  end

  def sorted_docs
     sorted_docs = Naturalsorter::Sorter.sort(@source.docs)
  end

end
