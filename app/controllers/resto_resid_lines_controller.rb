class RestoResidLinesController < ApplicationController
  # helper_method :sort_column, :sort_direction # goes with http://railscasts.com/episodes/228-sortable-table-columns
  def index
    # @RestoResidLines = RestoResidLine.all # works on old pre-SQL
    @ViewRestoResidLines = ViewRestoResidLine.all # seems to sort by date when used with resto_resid_lines/index.html
    # For Sortable columns copied from people
    # @resto_resid_lines = resto_resid_line.order(sort_column + " " + sort_direction)
  end

  # Part of http://railscasts.com/episodes/228-sortable-table-columns
  # def sort_column
  #    ViewRestoResidLine.column_names.include?(params[:sort]) ? params[:sort] : "name" # Does it matter what is in this last quote
  #  end
  #
  #  def sort_direction
  #    %w[asc desc].include?(params[:direction]) ? params[:direction] : "asc"
  #  end
end
