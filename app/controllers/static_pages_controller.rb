class StaticPagesController < ApplicationController
  def home; end

  def help; end

  def acknowledgements; end

  def about; end

  def contact; end

  def admin; end

  def illich; end

  def marietich; end

  def story_intro; end
end
