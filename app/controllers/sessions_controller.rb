# Listing 11.32: Preventing unactivated users from logging in.
# app/controllers/sessions_controller.rb
class SessionsController < ApplicationController
  def new; end

  # Causes an error in testing: integration/users_edit_test ? and test/integration/users_index_test.rb:13:in `block in <class:UsersIndexTest>'
  def create
    user = User.find_by(email: params[:session][:email].downcase)
    if user && user.authenticate(params[:session][:password])
      if user.activated?
        log_in user
        params[:session][:remember_me] == '1' ? remember(user) : forget(user)
        redirect_back_or user
      else
        message  = 'Account not activated. '
        message += 'Check your email for the activation link.'
        flash[:warning] = message
        redirect_to root_url
      end
    else
      flash.now[:danger] = 'Invalid email/password combination'
      render 'new'
    end
  end

  def destroy
    log_out if logged_in?
    redirect_to root_url
  end
end
