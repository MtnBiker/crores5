class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception
  # before_action :authenticate_user! # Copeland, but he's using a different sign-in
  include SessionsHelper # p351 Hartl

  # Done when any edits are made. This repopulates the RestoResidLines table so connections is made for a Person's work and residence. Fails in Rails 5.1. How to test?
  # See locations, years, and people controllers
  # Seems like this should be in resto_resid_lines_controller, but isn't found there

  def repopulateResidResto # SQL version
    # puts "application.controller:13. DEBUGGING, confirming in application_controller__repopulateResidResto\nSHOULD be kicking off an SQL" # Shows up in rails server log in iTerm
    query = <<-SQL
      CREATE OR REPLACE VIEW public.view_resto_resid_lines AS
      SELECT resto.person_id AS person_id, -- "Person ID", -- could as easily be resid.person_id since it's the same#{' '}
      RTRIM(p.given_name) || ' ' || RTRIM(p.last_name) AS Name,
      -- TODO make the titles more PG and Rails like. This is OK for testing but won't work with Rails views very weill
      resto.year_date AS resto_date, -- "Resto Date",#{' '}
      resid.year_date AS resid_date, --  "Resid Date",
      resid.year_date AS mid_date, -- TODO need to calculate this but need it in the talbe for now
      resto.resto_name AS resto_name, -- "Resto Name",
      resto_loc.address AS resto_address, -- "Resto Address",
      resto.title AS title_resto, -- "Title Resto",
      resid_loc.address AS resid_address, -- "Resid Address",#{' '}
      resid.title AS title_resid, -- "Title Resid",#{' '}
      resto_loc.longitude AS long_resto, -- "Resto Longitude",
      resto_loc.latitude AS  lat_resto, -- "Resto Latitude",
      resid_loc.longitude AS long_resid, -- "Resid Longitude",
      resid_loc.latitude AS lat_resid, --  "Resid Latitude",
      RTRIM(resto.notes) AS resto_notes, -- "Resto Notes",
      RTRIM(resid.notes) AS resid_notes, -- "Resid Notes",
      p.date_of_birth AS date_of_birth, -- "Date of Birth",
      resto.id AS resto_connection_id, -- years_resto_id,
      resid.id AS resid_connection_id, -- years_resid_id,
      resto.location_id as resto_loc_id,
      resid.location_id as resid_loc_id
      From years resto JOIN years resid ON  resto.year_date between (resid.year_date-364) and (resid.year_date+364)
      JOIN people p ON resto.person_id = p.id
      JOIN locations resto_loc ON resto.location_id = resto_loc.id
      JOIN locations resid_loc ON resid.location_id = resid_loc.id
      WHERE -- This makes little sense to me why the following works. Note some restrictions set above
          resto.person_id = resid.person_id and
          resto.resto = true           and
          resto.resto != resid.resto   and
          resto.resid != resid.resid   and
          resto.resto = true           and
          resid.resto = false#{'          '}
      Order by resto.year_date, p.last_name, p.given_name
    SQL
  end # def repopulateResidResto

  # A table
  def repopulateConnectingLines
    query = <<-SQL
      CREATE OR REPLACE VIEW public.view_connecting_lines AS

      ORDER BY year_date#{'      '}
    SQL
  end

end # class
