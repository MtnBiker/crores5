class AllLocationsController < ApplicationController
  # A SWAG to get this to work
  def index
    # https://medium.com/swlh/build-a-dynamic-search-with-stimulus-js-and-rails-6-56b537a44579. See models/all_locations.rb for other comments
    # the if then else can be removed, but keep the @locations = Location.all
    @locations = if params[:query].present?
                   Location.global_search(params[:query])
                 else
                   Location.all
                 end

    respond_to do |format|
      format.html
      format.json { render json: { locations: @locations } }
    end
  end
end
