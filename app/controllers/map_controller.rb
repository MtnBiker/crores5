class MapController < ApplicationController
  # layout "map" # not sure I need this. Directs to layout/map.html.erb but seemed to work OK without it or does it But maybe everything is in the application.html.erb?? Probably got this from Clark p158

  def index; end

  def show; end

  # Trying to get a map for all locations. olAllLocations.js. jBuilder needs this, without is the .geojson is essentially empty # Moved to locations
  # def allLocations
  #   @locations = Location.all
  # end

  # map_data superceded by line_data and point_data since I couldn't out how to have @year_who_what_where and @lines in the same def. Leaving in for a while, but can delete eventually 2016-10-15
  # def map_data
  #   # Need to make this more general, i.e., feed in start from web page or look at slider
  #   # start   = '1865-01-01'
  #   # start   = start.to_date
  #   # finish  = (start + 80.year)
  #   # finish  = '1895-12-31', + 1.year takes it to 1 Jan. may need to subtract a day, but depends on how range works

  #   # Used by map_data.json.jbuilder for putting the restaurants and residences on
  #   @year_who_what_where = Year.where(year_date: start..finish).select("location_id, year_date, title, person_id, resto, resid, resto_name")

  #   # For the line connecting the resto to the residence.
  #   @lines =  RestoResidLine.where(resto_date: start..finish).select("id, person_id, resto_loc_id, resid_loc_id, resto_name, resto_date, title_resto, resid_date, title_resid, long_resto, lat_resto, long_resid, lat_resid")
  # end

  # the following two defs used by map_data.json.jbuilder. Although I don't see the link, must be named line_data. Is that because of routes?
  def line_data # put start, finish inside parentheses and it broke jbuilder, even with start and finish being redefined below
    # Need to make this more general, i.e., feed in start from web page or look at slider
    start   = '1865-01-01'
    start   = start.to_date # tried putting start.to_date in place of start below and commenting this line out, and it broke jbuilder
    finish  = start + 80.years

    # For the line connecting the resto to the residence.
    # resto_loc_id, resid_loc_id, person_id, first_name, last_name are probably not needed for what it's being used for
    # So took them out. Put person_id back in to work on data for one person
    # apparently thise calls resto_resid_lines_controller which has
    @lines =  ViewRestoResidLine.where(resto_date: start..finish).select('person_id, full_name, resto_name, resto_date, title_resto, resid_date, title_resid, long_resto, lat_resto, long_resid, lat_resid, resto_address, resid_address, mid_date, resto_loc_id, resid_loc_id') #
  end

  def line_point_data # just a copy of line_data to get line_point_data.jbuilder.erb working, but it builds @lines
    # Need to make this more general, i.e., feed in start from web page or look at slider
    start   = '1865-01-01'
    start   = start.to_date # tried putting start.to_date in place of start below and commenting this line out, and it broke jbuilder
    finish  = start + 80.years

    # For the line connecting the resto to the residence.
    # resto_loc_id, resid_loc_id, person_id, first_name, last_name are probably not needed for what it's being used for
    # So took them out. Put person_id back in to work on data for one person
    @lines =  ViewRestoResidLine.where(resto_date: start..finish).select('person_id, full_name, resto_name, resto_date, title_resto, resid_date, title_resid, long_resto, lat_resto, long_resid, lat_resid, resto_address, resid_address, mid_date')
    # Used by line_point_data.json.builder for putting the restaurants and residences on?  people/person_location.json.builder
    @year_who_what_where = Year.where(year_date: start..finish).select('location_id, year_date, title, person_id, resto, resid, resto_name')
  end

  def point_data
    # Need to make this more general, i.e., feed in start from web page or look at slider
    start   = '1865-01-01'
    start   = start.to_date
    finish  = start + 80.years

    # Used by  point_data.json.builder for putting the restaurants and residences on?  people/person_location.json.builder
    @year_who_what_where = Year.where(year_date: start..finish).select('location_id, year_date, title, person_id, resto, resid, resto_name')
  end

  def dot_data # crazy since same as point data, need to fix
    # Need to make this more general, i.e., feed in start from web page or look at slider
    start   = '1865-01-01'
    start   = start.to_date
    finish  = start + 80.years

    @year_who_what_where = Year.where(year_date: start..finish).select('location_id, year_date, title, person_id, resto, resid, resto_name')
  end
end
