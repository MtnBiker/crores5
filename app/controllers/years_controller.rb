class YearsController < ApplicationController
  # binding.pry
  helper_method :sort_column, :sort_direction
  before_action :set_year, only: %i[show edit update destroy] # 2019.06.20 Can't see that this is being used anywhere. But without it get an error on years/show.html.erb:5 undefined method `person' for nil:NilClass. This is some basic Rails thing
  before_action :set_s3_direct_post, only: %i[new edit create update] # 2019.06.20 Causing an error if commented out on years/312/edit, but OK as long as aws.rb still set See TODO.md. I also have this in Users. Where does it belong.
  before_action :dup_check, only: %i[new edit update] # runs, but not what I think I want
  before_action :resto_or_resid, only: %i[update] # create is handled within def create

  PAGE_SIZE = 10

  # GET /years/new # First since change frequently
  def new
    puts '=+=+=+= years_controller:12'
    @year =
      Year.find_by(id: params[:base_id])&.dup ||
        Year.new({ year_date: '1896-05-01' })

    # https://stackoverflow.com/questions/61722805/rails-to-for-a-duplicate-record. First statement is for duplicate a record
    puts "=+=+=+= years_controller:15. @year: #{@year}. @year.year_date: #{
           @year.year_date
         }"

    # All from Tutorial Points Tutorial. Second statement for initial value.
    # @year = Year.new({:year_date => "1913-01-01"}) # This is default. Helps to change when going through a group.
    @locations = Location.all
    @people = Person.all
    # @source    = Source.all  # FIXME? this or the change in model
    # year       = Year.new # ActiveStorage
    # @positions = Position.all # The table with titles for people, Errors when new is called. Try again after creating model
    # @full_names = Person.all.given_name + Person.all.last_name
    # binding.pry
  end

  def index
    @search = Year.ransack(params[:q]) # sometimes defined as @q
    @years = @search.result.includes(:location, :person) # this allows associations in ransack
    respond_to do |format|
      format.html {}
      format.json { render json: @years }
    end

    # For Stimulus search
    # https://medium.com/swlh/build-a-dynamic-search-with-stimulus-js-and-rails-6-56b537a44579
    # Fields searched are set in location.rb
    if params[:query]
      @years = Year.global_search(params[:query])
    else
      @years = Year.all
    end
    respond_to do |format|
      format.html
      format.json { render json: { years: @years } }
    end

  end

  # Ransack https://github.com/ylankgz/InvoiceApp
  def search
    index
    render :index
  end

  def documents
    @years = Year.all
  end

  def map_one
    # mapping one connection/year
    # need to use the year passed in and then make it for a year
    years = Year.where(year_date: '1883-09-01'..'1995-12-31')
  end

  def summary
    @years = Year.order(:year_date)
    respond_to do |format|
      format.html {}
      format.json { render json: @years }
    end
  end

  def show
    @year = Year.find(params[:id])
    @doc = Doc.new
  end

  # GET /years/1/edit
  def edit
    # @positions = Position.all # Needed for Positions/ Titles list to work, However not added to database. 2021.07.01 Seems to work without this? But commenting it out doesn't solve the double GET
  end

  # POST /years
  # POST /years.json
  def create
    @year = Year.new(year_params)

    # Below sets resto or resid true/false as needed determined by title (position)
    resto_or_resid # I think the above sets up @year and then this call can work. It doesn't as a before_action
    respond_to do |format|
      if @year.save
        format.html do
          redirect_to @year, notice: 'Connection was successfully created.'
        end # default

        # format.html do
        #   render :edit, notice: 'Connection was partially created. Add snippet.'
        # end # Want to draw snippet. Crude and causing issues
        format.json { render :show, status: :created, location: @year }
      else
        format.html { render :new }
        format.json { render json: @year.errors, status: :unprocessable_entity }
      end
    end
    repopulateResidResto
    # redirect_to year # https://edgeguides.rubyonrails.org/action_text_overview.html but didn't have it before. Already done by something else
  end

  # PATCH/PUT /years/1
  # PATCH/PUT /years/1.json
  def update
    respond_to do |format|
      if @year.update(year_params)
        format.html do
          redirect_to @year, notice: 'Connection was successfully updated.'
        end
        format.json { render :show, status: :ok, location: @year }
      else
        format.html { render :edit }
        format.json { render json: @year.errors, status: :unprocessable_entity }
      end
    end
    repopulateResidResto # nice to have some error handling here
  end

  # DELETE /years/1
  # DELETE /years/1.json
  def destroy
    @year.destroy
    respond_to do |format|
      format.html do
        redirect_to years_url, notice: 'Connection was successfully destroyed.'
      end
      format.json { head :no_content }
    end
    repopulateResidResto
  end

  # https://gorails.com/forum/how-do-i-create-a-delete-button-for-images-uploaded-with-active-storage
  def delete_image_attachment
    @document = ActiveStorage::Attachment.find(params[:id])
    @document.purge
    redirect_back(fallback_location: request.referer)
  end

  private

  # Use callbacks to share common setup or constraints between actions.
  def set_year
    # See notes near top for why this is here
    # puts "years_controller:141. :id #{@year.id}"
    @year = Year.find(params[:id])
  end

  # Never trust parameters from the scary internet, only allow the white list through.
  # Note that I copied all of these to create
  def year_params
    params
      .require(:year)
      .permit(
      :year_date,
      :person_id,
      :name_shown,
      :title,
      :location_id,
      :resid,
      :resto,
      :resto_name,
      :resto_event,
      :notes,
      :content,
      :content_plain_text,
      :doc_id,
      :snippet_extent,
      :snippet_coords
    ) # doc_ids:[] trying for multiples. :file_basename, :notes, :aws_url, :from_where_url, :from_where, :caption, added 2021 although already in db, but not adding anymore? took back out since didn't help. :notes is now in :content, I guess leaving it in since use a a clipboard
  end

  # https://devcenter.heroku.com/articles/direct-to-s3-image-uploads-in-rails or was it Paperclip since this doesn't look like the heroku article. 2019.06.20 Commented out since gave an error. But i may want direct post at some point FIXME deprecated with docs
  def set_s3_direct_post
    @s3_direct_post =
      S3_BUCKET.presigned_post(
        key: "uploads/#{SecureRandom.uuid}/${filename}",
        success_action_status: '201',
        acl: 'public-read'
      )
    puts "=+=+=+= years_controller:141. set_s3_direct_post: #{@s3_direct_post}"
  end

  def dup_check
    # 2020.02.08 This is info only for development. Had to give up as all seems to be handled in controller>create and I won't be able to parse that. Can delete if also remove related before_action from from near top
    # 1. Need to calc checksum of document selected
    # 2. Check if that checksum exists in blobs.
    # 3. If it doesn't continue
    # 4. If it does exist, need to change or write to new active_storage_attachments with the exising blob_id

    # Would be nice to see what instance variable are available, but Black-Leo creates an instance
    # puts "years_controller:70. model.year_date: #{model.year_date}" # trying some Ruby-Copeland p345, but not getting it
    # puts self.caption.downcase # undefined method caption. See previous line
    # puts "years_controller:150. in def dup_check. @year: #{@year}.  @year.id: #{@year.id}" # error on new
    if @year == ''
      # nil didn't work
      puts "=+=+=+= years_controller:156. @year.year_date: #{@year.year_date}" # trying some Ruby-Copeland p345, but not getting it

      io =
        '/Users/gscar/Documents/Genealogy/1900-09 Los Angeles City Directories/1900 NOT Los Angeles City Directory. p902. Restaurants. Restovich, 505 S Main.jpg'

      # io = @tempfile # @tempfile shows up in iTerm, but gives an error, obviously doesn't exist when compile this page
      new_file_checksum = Digest::MD5.file(io).base64digest
      aws_checksum = ActiveStorage::Blob.find_by(checksum: new_file_checksum)
      if aws_checksum
        aws_blob_id =
          ActiveStorage::Blob.find_by(checksum: new_file_checksum).id # the id of the blob that already exists

        # The following is just for dev
        # aws_file_key =  ActiveStorage::Blob.find_by(checksum: new_file_checksum).key
        aws_file_filename =
          ActiveStorage::Blob.find_by(checksum: new_file_checksum).filename

        # aws_file_content_type = ActiveStorage::Blob.find_by(checksum: new_file_checksum).file_content_type
        # aws_file_metadata = ActiveStorage::Blob.find_by(checksum: new_file_checksum).file_metadata
        # aws_file_byte_size = ActiveStorage::Blob.find_by(checksum: new_file_checksum).file_byte_size
        # aws_file_created_at = ActiveStorage::Blob.find_by(checksum: new_file_checksum).file_created_at
        puts "years_controller:190. searched and found that a blob for checksum: #{
               new_file_checksum
             } and need to do other stuff" # works with link to a file as in the first test
        puts "id of already existing file at AWS, named aws_blob_id: #{
               aws_blob_id
             }. Will become blob_id in active_storage_attachments for new entry"
        puts "years_controller:192. Now need to find how to write to active_storage_attachments for year.id #{
               @year.id
             } changing the blob_id to #{
               aws_blob_id
             } since it already exist. And not create the blob!!! Maybe it's for me to do the save or update from scratch?"
        puts "years_controller:193. @year: #{@year}. @year.year_date: #{
               @year.year_date
             }. So can access all the fields. So will try to change it"
        @year.year_date = '1920-11-22' # This one shows up on the edit page and is saved
        puts "years_controller:195. The date above is different than the one displayed on the page if a new year_date is set in dup_check. \nBut need to create a new active_storage_attachments line not change the existing one "
        puts "The new active_record attachment will be: id is that auto created? name: 'documents', record_type: 'Year', record_id (which is @year.id): {@year.id}, blog_id: #{
               aws_blob_id
             }, the latter two determined dynamically. Can I just `create`? Do I make a year_params? p349 Buby-Copeland the field is set by self.field_name, then save or do I create something that looks like what shows up in iTerm except what is it called, is is ActiveStorage::Attachment… "
        puts "aws_file_filename: #{aws_file_filename}"
        # See item 4 above
      else
        puts "No file exists with same checksum: #{
               new_file_checksum
             } as the file being uploaded, so move on"
      end # don't need to do anything if new_file isn't already uploaded
    else
      puts '=+=+=+= years_controller:180. dup_check. nothing to see since an update, but new'
    end # @year.year_date?  because doesn't exist for new

    #  TODO early dev stuff that can be deleted # if active_storage_blobs(:checksum ) == an existing blob
    #  checksumFor49 = ActiveStorage::Blob.find(49).checksum
    #  if ActiveStorage::Blob.find_by(checksum: checksumFor49).image?
    #     puts "searched and found that blob exists or checksum: #{checksumFor49} and its blob_id is:"
    #   else
    #      puts "searched and found that a blob DOES NOT exist for checksum: #{checksumFor49}"
    #    end
    #  # let's first run a test seeing if a know checksum exists. That works. Now get the existing checksums
    #  test_checksum = '0M4nc4nuUaVuqo3+sJw+Lg==' # will get for selected image at some point
    #
    #  test_checksum == '0M4nc4nuUaVuqo3+sJw+Lg==' ? (puts "years_controller:165. test_checksum #{test_checksum} does exist") : (puts "years_controller:158. test_checksum #{test_checksum} does not exist") # Need the ()
    #   puts "years_controller:160. dup_check entered via before_action. before_save results in an errro"
    # end
  end # dup_check

  def person_params
    params
      .require(:person)
      .permit(:last_name, :given_name, :full_name, :full_name_id)
  end

  # Sets resto or resid to true or false based on title. Get called in two places. Once by new and once by create.
  def resto_or_resid
    puts "=+=+=+= years_controller.rb:207. resto_or_redid. @year.title:: #{
           @year.title
         }"
    if @year.title === 'Resident' || @year.title === 'Householder' ||
         @year.title === 'Owner Resident'
      # Should have been Owner Resident but it wasn't? Not used much || @year.title === "Owner Residence"  Removed so less to check
      @year.resid = true
      @year.resto = false # unsetting if had been mis-set before
      # puts "years_controller.rb:205: #{@year.title}. resid: #{@year.resid}. resto: #{@year.resto}. year_date: #{@year.year_date}. person_id: #{@year.person_id}"
    else
      @year.resto = true
      @year.resid = false # unsetting if had been mis-set before
      # puts "years_controller.rb:209: #{@year.title}. resid: #{@year.resid}. year_date: #{@year.year_date}. person_id: #{@year.person_id}"
    end
  end
end
