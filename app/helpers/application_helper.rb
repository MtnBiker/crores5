module ApplicationHelper
  # For column sorting from http://railscasts.com/episodes/228-sortable-table-columns
  # http://railscasts.com/episodes/240-search-sort-paginate-with-ajax adding search

  def sortable(column, title = nil)
    title ||= column.titleize
    css_class = column == sort_column ? "current #{sort_direction}" : nil
    direction = column == sort_column && sort_direction == 'asc' ? 'desc' : 'asc'
    link_to title, { sort: column, direction: direction }, { class: css_class }
  end

  # Returns the full title on a per-page basis.
  # The result is that the full_title method is automagically available in all our views.
  def full_title(page_title = '')
    base_title = 'Croatian Operated Restaurants in Early Los Angeles'
    if page_title.empty?
      base_title
    else
      page_title + ' | ' + base_title
    end
  end

  # https://stackoverflow.com/questions/60727460/using-bootstrap-icons-in-rails-6-with-webpacker/64438269#64438269
  # Commented this out 2020.10.20 to see if really needed. It is needed on people>show
  # To use Bootstrap Icons. < = icon("alarm-fill", class: "text-success") %>
  def icon(icon, options = {})
    file = File.read("node_modules/bootstrap-icons/icons/#{icon}.svg")
    doc = Nokogiri::HTML::DocumentFragment.parse file
    svg = doc.at_css 'svg'
    svg['class'] += ' ' + options[:class] if options[:class].present?
    doc.to_html.html_safe
  end
end
