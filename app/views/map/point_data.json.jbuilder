# Puts in points for restaurants and residences. Maybe should be split in two to simplify the logic.

json.type 'FeatureCollection'
locIDlast = ''
i = 0
json.features @year_who_what_where do |year| # lives in map_controller.rb
  json.type 'Feature'
  json.properties do
    repeatLoc = locIDlast == year.location_id.to_s
    i = locIDlast == year.location_id.to_s ? i + 1 : 0
    if year.resto
      if repeatLoc
        json.set! 'marker-color', '#C978F3'
        json.set! 'marker-size', 'medium'
        json.set! 'marker-symbol', 'restaurant'
        if i == 2
          json.set! 'marker-size', 'small'
          json.set! 'marker-color', '#DAABF3'
        end
      else
        json.set! 'marker-color', '#7E0FB7'
        json.set! 'marker-size', 'large'
        json.set! 'marker-symbol', 'restaurant'
        i = 0
      end
    else
      json.set! 'marker-color', '#5cb85c'
      json.set! 'marker-size', 'small'
      json.set! 'marker-symbol', 'lodging'
      case i
      when 0
        json.set! 'marker-color', '#65E661'
        json.set! 'marker-size', 'small'
      when 1
        json.set! 'marker-color', '#5cb85c'
        json.set! 'marker-size', 'medium'
      when 2
        json.set! 'marker-color', '#3F8B3D'
        json.set! 'marker-size', 'large'
      end
    end

    json.time  year.year_date # naming .time since that's what sliderControl originally used. .year_date would be nicer, but need to change it for line_data.json.builder and in map.js
    json.title year.map_popup
  end
  if year.location.longitude
    json.geometry do
      json.type 'Point'
      json.coordinates [year.location.longitude, year.location.latitude]
    end
  end
  locIDlast = year.location_id.to_s
end
