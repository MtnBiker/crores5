# Draws lines connecting the restaurant location and residence of a person
# Need to add information about the person PROBABLY HAVE TO CHANGE LINES TO HAVE INFORMATION AVAILABLE
# This shows getting called when go to /map page. At least it did when I was looking for a problem
# Added to if for development testing  && line.person_id == 140
# @lines is defined in the map_controller based on view_resto_resid_line
json.type 'FeatureCollection'
json.features @lines do |line|
  if line.long_resto && line.long_resid
    json.type 'Feature'

    json.properties do
      # Next four lines trying doing the calculation here and not in mapTwo.js
      # This works, subtracting and added a year to the date arbitrarily. Super kluge
      startDate = line.mid_date.to_s[0..3].to_i - 1 # - 8000000
      endDate =   line.mid_date.to_s[0..3].to_i + 1 # + 8000000
      json.start startDate
      json.end   endDate
      json.mid_date line.mid_date.to_time.to_i # .to_time.to_i converts to Unix time and it works in olMap.js

      json.popup line.line_popup # a method in view_resto_resid_line.rb.

      #     json.title line.line_popup # a method in resto_resid_line.rb TODO get this working again
    end # json.properties

    json.geometry do
      # Only want an entry if coordinates exist for location. Should log missing results
      json.type 'LineString'
      # Added .to_f to see if data would fix problem, gets rid of quotes, but may not solve problem, but didn't. Seems better without quotes, but I think not needed
      json.coordinates [[line.long_resto.to_f, line.lat_resto.to_f],
                        [line.long_resid.to_f, line.lat_resid.to_f]]
    end # json.geometry for lines

  end # if
end # features lines
