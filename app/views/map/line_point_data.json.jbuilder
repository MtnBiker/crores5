# Test combining line and point data in one. Only get the Feature Collection line and no features. Maybe can only have one json.features

# @lines =  ViewRestoResidLine.where(resto_date: start..finish).select("person_id, full_name, resto_name, resto_date, title_resto, resid_date, title_resid, long_resto, lat_resto, long_resid, lat_resid, resto_address, resid_address, mid_date, resto_loc_id, resid_loc_id") # I don't really think this is being used

json.type 'FeatureCollection'
# one person at one time live and work, but that will ignore where we only have one or the other FIXME?
json.features @lines do |line| # lives in map_controller.rb
  json.type 'Feature'
  json.properties do
    # put them all in, may not need them all
    json.mid_date        line.mid_date.to_time.to_i
    json.full_name       line.full_name
    json.resto_name      line.resto_name
    json.resto_date      line.resto_date
    json.title_resto     line.title_resto
    json.resid_date      line.resid_date
    json.title_resid     line.title_resid
    json.resto_address   line.resto_address
    json.resid_address   line.resid_address
    # json.resto_loc_id       line.resto_loc_id  # don't understand why these don't work
    # json.resid_loc_id       line.resid_loc_id  # don't understand why these don't work
    json.long_resto      line.long_resto
    json.lat_resto       line.lat_resto
    json.long_resid      line.long_resid
    json.lat_resid       line.lat_resid
  end # json.properites do
  json.geometry do
    json.type 'LineString'
    json.coordinates [[line.long_resto.to_f, line.lat_resto.to_f],
                      [line.long_resid.to_f, line.lat_resid.to_f]]
  end
end

#  only can have one feature collection AFAIK and the last one is the one that shows up
# line_data.jbuilder.json below
# need to have this line_point_data defined in map_controller.rb
# json.type "FeatureCollection"
# json.features @lines do |line|
#   if (line.long_resto && line.long_resid)
#     json.type "Feature"
#
#     json.properties do
#       # Next four lines trying doing the calculation here and not in mapTwo.js
#       # This works, subtracting and added a year to the date arbitrarily. Super kluge
# startDate = line.mid_date.to_s[0..3].to_i - 1 # - 8000000
# endDate =   line.mid_date.to_s[0..3].to_i + 1 # + 8000000
# json.start startDate
# json.end   endDate
# json.mid_date line.mid_date.to_time.to_i # .to_time.to_i converts to Unix time and it works in olMap.js
#
#       json.popup line.line_popup # a method in view_resto_resid_line.rb.
#
# #     json.title line.line_popup # a method in resto_resid_line.rb TODO get this working again
#     end #json.properties
#
#     json.geometry do
#      # Only want an entry if coordinates exist for location. Should log missing results
#        json.type "LineString"
#        # Added .to_f to see if data would fix problem, gets rid of quotes, but may not solve problem, but didn't. Seems better without quotes, but I think not needed
#        json.coordinates [[line.long_resto.to_f, line.lat_resto.to_f], [line.long_resid.to_f, line.lat_resid.to_f]]
#     end # json.geometry for lines
#
#   end  # if
# end # features lines
