# Puts in points for restaurants and residences. These small points are intended to be on the map no matter what the slider (or year selected) is.
# Needs to be in routes to be found. Also needs its own method in map_controller. Could it reuse point_data?
# putting only "features"
# @year_who_what_where = Year.where(year_date: start..finish).select("location_id, year_date, title, person_id, resto, resid, resto_name")

json.type 'FeatureCollection'
json.features @year_who_what_where do |year| # lives in map_controller.rb
  # @year_who_what_where = Year.where(year_date: start..finish).select("location_id, year_date, title, person_id, resto, resid, resto_name")

  json.type 'Feature'
  json.properties do
    if year.location.longitude # don't try if no coordinates

      # different color for resto or resid. Later maybe shade by decade
      if year.resto
        json.set! 'marker-color', '#C978F3'
        json.set! 'marker-symbol', 'circle'
        json.set! 'marker-size', 'small'
      else # resid
        json.set! 'marker-color', '#7E0FB7'
        json.set! 'marker-symbol', 'square'
        json.set! 'marker-size', 'small'
      end # if

      json.geometry do
        json.type 'Point'
        json.coordinates [year.location.longitude, year.location.latitude]
      end # json.geom
    end
  end
end
