json.extract! source, :id, :year, :name, :publisher, :source, :from_where, :from_where_url,
              :caption, :pub_date, :notes, :created_at, :updated_at
json.url aws_url(source, format: :json)
