json.extract! doc, :id, :source, :page_no, :image, :original_url, :basename, :created_at,
              :updated_at
json.url doc_url(doc, format: :json)
