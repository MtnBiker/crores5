# views/people/lines/index.json.jbuilder
# Creating a line for resto and a line for resid
# What causes this to be built? See similar people/lines/index.html.erb. Creates the rollover for the lines on people/show map

json.type 'FeatureCollection'
# Unlike some of my other jBuilders, this accesses the database and creates an array to work with. I used sql in development and don't know offhand how to make it Ruby.
# http://localhost:3000/people/52/lines.json is where this file ends up
# puts "index.json.jbuilder:10. @person.id #{@person.id}" # passed through params
# Since one set of features, the way I have it now, have to work with this array.
array = Year.find_by_sql("Select year_date as date,  people.given_name || ' ' || people.last_name as full_name, years.resto, years.resid, locations.longitude as longitude, locations.latitude as latitude, title, locations.address " + 'From years  ' + 'inner join people  on years.person_id = people.id ' + 'inner join locations  on years.location_id = locations.id ' + "where  person_id = #{@person.id} and locations.city = 'Los Angeles' " + 'order by year_date')
# Some tests to make sure I'm getting what I want
# puts "index.json.jbuilder:12. array: #{array}"
# puts "index.json.jbuilder:13. array[0].longitude.to_f: #{array[0].longitude.to_f}"
# puts "index.json.jbuilder:14. array[0].date.class: #{array[0].date.class}"
# puts "index.json.jbuilder:15. array[0].date.to_s: #{array[0].date.to_s}"
# As usual need to declare variable so they are available in the loops
count = 0
countResto = 0
countResid = 0

longitude_resto_start = ''
latitude_resto_start = ''
date_resto_start = ''

longitude_resid_start = ''
latitude_resid_start = ''
date_resid_start = ''

person_id = @person.id # not sure I need this as I put it in above
json.features array.each do |array|
  if array.resto
    if countResto != 0 # all but the first time do this (make a negative so main action was on top).
      json.type 'Feature'
      # These need to be in the json.features and not above or don't calculate correcctly
      # puts "index.json.jbuilder:37. Iteration no. #{countResto} of resto of total count #{count} at the top of feature"
      json.properties do
        # puts "index.json.jbuilder:38. Iteration no. #{countResto} of resto of total count #{count}."
        json.name array.full_name # redundant in that this is just for one person, but is good for debugging and if I use it for all the people
        json.place 'resto'
        json.style 'resto'
        # date_start will have to come from a previous iteration
        # puts "index.json.jbuilder:51. date_resto_start: #{date_resto_start}."
        json.date_start date_resto_start # no implicit conversion of Year into Integer
        date_resto_end = array.date.to_s
        # puts "index.json.jbuilder:54. date_resto_end: #{date_resto_end}."
        json.date date_resto_end
        date_range = "#{date_resto_start} to #{date_resto_end}"
        json.popup_loc  date_range # defined early and not sure all what using for
        json.popover_people date_range # defined specifically for show>people
        date_resto_start = date_resto_end # for next round
      end # json.properties
      json.geometry do
        json.type 'LineString'
        json.coordinates [[longitude_resto_start, latitude_resto_start],
                          [array.longitude.to_f, array.latitude.to_f]]
      end # json.geometry
    else # first time through
      # puts "index.json.jbuilder:61. Iteration no. #{countResto} resto of total count #{count}.. Setting start variables for the first iteration of resto"
    end # if count = 0 skip the first run through
    countResto += 1
    longitude_resto_start = array.longitude
    latitude_resto_start = array.latitude
    date_resto_start = array.date
    # puts "index.json.jbuilder:53. Iteration no. #{countResto} resto of total count #{count}.. Setting start variable"
  else # resid
    if countResid != 0 # all but the first time do this (make a negative so main action was on top).
      json.type 'Feature'
      # These need to be in the json.features and not above or don't calculate correcctly
      # puts "index.json.jbuilder:76. Iteration no. #{countResid} resid of total count #{count}. at the top of feature"
      json.properties do
        # puts "index.json.jbuilder:81. Iteration no. #{countResid} resid of total count #{count}."
        json.name array.full_name
        json.place 'resid'
        json.style 'resid'
        # date_start will have to come from a previous iteration
        # puts "index.json.jbuilder:71. date_resid_start: #{date_resid_start}."
        json.date_start date_resid_start # no implicit conversion of Year into Integer
        date_resid_end = array.date.to_s
        # puts "index.json.jbuilder:82. date_resid_end: #{date_resid_end}."
        json.date date_resid_end
        date_range = "#{date_resid_start} to #{date_resid_end}" # don't need variable, but easier for me to comprehend by having it named
        json.popup_loc  date_range
        json.popover_people date_range # defined specifically for show>people
        date_resid_start = date_resid_end # for next round
      end # json.properties
      json.geometry do
        json.type 'LineString'
        json.coordinates [[longitude_resid_start, latitude_resid_start],
                          [array.longitude.to_f, array.latitude.to_f]]
      end # json.geometry
    else # first time through
      # puts "index.json.jbuilder:91. Iteration no. #{countResid}. Setting start variables for the first iteration of resid"
      # longitude_start = array.longitude
      #  latitude_start = array.latitude
      #  date_start = array.date.to_s
    end # if count = 0 skip the first run through
    countResid += 1
    longitude_resid_start = array.longitude
    latitude_resid_start = array.latitude
    date_resid_start = array.date
    # puts "index.json.jbuilder:100. Iteration no. #{countResto}. Setting start variable"
  end #  if resto/resid
  count += 1
end # json.features
