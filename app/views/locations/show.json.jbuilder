json.extract! @location, :id, :address, :city, :state, :longitude, :latitude, :extant,
              :current_description, :notes, :created_at, :updated_at
