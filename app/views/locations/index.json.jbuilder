# # http://localhost:3000/locations.json
# What was this for? Not sure but until commented out the next to last line was broken, so probably not used 2021.08.09
json.array!(@locations) do |location|
  json.extract! location, :id, :address, :city, :state, :longitude, :latitude, :extant,
                :current_description, :notes
  # json.url street_view_url(location, format: :json)
end
