# All Locations on a map
# Used by locations/ol_all_locations.html.erb
# Would like a pop up that shows lists the connections to this place

json.type 'FeatureCollection'
# @locations defined in map_controller.rb.
json.features @locations do |location|
  json.type 'Feature'

  json.properties do
    json.id      location.id
    json.address location.address
    json.city    location.city
    # json.popup location.full_address # a method in location.rb. # TMI Maybe unless far far away
    # Want to add history at address
    json.coordinates [location.longitude, location.latitude] # as a test and it works. Should be able to read from below FIXME
    #  json.person year.person_id # NameError - undefined local variable or method `year
    # Add history
    json.history @location do |year|
      # json.year location.year
      json.person year.person_id
      # @location nil. location.year nil. location.years nil
    end
    # json.array! @location, :id
  end # json.properties

  json.geometry do
    json.type 'Point'
    # json.coordinates [location.longitude.to_f, location.latitude.to_f] # this works to, but not needed. Removes quotes from around coordinates
    json.coordinates [location.longitude, location.latitude] # has quotes around numbers, but seems to work
  end # json.geometry
end # features
