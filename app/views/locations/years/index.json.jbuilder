# app/views/locations/years/index.json.jbuilder # Copied from app/views/people/locations/index.json.jbuilder
# This is a trial FIXME, not being used

json.type 'FeatureCollection'

json.features @location.years do |year|
  json.type 'Feature'
  # Can have properties do loop twice and they end up together in the output
  json.properties do
    json.year_id        year.id # not using but is handy for debugging and maybe I'll use it
    json.name           year.person.full_name
    json.person_id      year.person.id
  end
  # json.positionTitle  year.title

  if year.resto # maybe use different way to get later but keeps it separate
    json.properties do
      json.date           year.year_date
      json.address        year.location.address
      json.resto_name     year.resto_name
      json.positionTitle  year.title
      json.place          'restaurant'
      json.popup_loc      year.resto_popup # defined in year.rb. loc being locations.json
      json.popover_people year.resto_popover # defined specifically for show>people
      json.icon 'restoIcon' # may not be using but leave it in
    end # json.properties resto
  else # resid
    json.properties do
      json.date           year.year_date
      json.address        year.location.address
      json.positionTitle  year.title
      json.place 'residence'
      json.popup_loc      year.resid_popup # defined in year.rb. loc being locations.json
      json.popover_people year.resid_popover # defined specifically for show>people
      json.icon 'residIcon' # may not be using but leave it in
      # json.set! "marker-color", "#7E0FB7" # will make this smarter if it works, different for resto and resid and maybe colored by decade
    end # json.properties resid
  end # if # resto (or resid)

  json.geometry do
    json.type 'Point'
    json.coordinates [year.location.longitude.to_f, year.location.latitude.to_f]
    json.popup year.year_date # 2020.09.16 trying. Not working, no errors. Is in lines.json
  end # json.geometry
end # json.features
